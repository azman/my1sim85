/**
*
* wxswitch.hpp
*
* - header for wx-based switching control
*
**/

#ifndef __MY1SWITCH_HPP__
#define __MY1SWITCH_HPP__

#include <wx/wx.h>
#include "wxbit.hpp"

#define SWI_SIZE_DEFAULT 21
#define SWI_SIZE_OFFSET 2
#define SWI_SIZE_SLIDER 4
#define SWI_SIZE_KNOB 6
#define KEY_SIZE_PANEL 19

class my1SWICtrl : public my1BITCtrl
{
protected:
	int mSize;
	bool mSwitched;
	wxBitmap *mImageDefHI, *mImageDefLO;
	wxBitmap *mImageHI, *mImageLO;
	virtual void DrawSWITCH(wxBitmap*,bool);
public:
	my1SWICtrl(wxWindow*,wxWindowID,bool do_draw=true,
		int aWidth=SWI_SIZE_DEFAULT,int aHeight=SWI_SIZE_DEFAULT);
	~my1SWICtrl();
	virtual void LinkThis(my1dpin_t*);
	bool GetState(void);
	bool Toggle(void);
	void Switch(bool aFlag=true);
	void OnPaint(wxPaintEvent&);
	void OnPopupClick(wxCommandEvent &event);
	void OnMouseClick(wxMouseEvent &event);
	void OnMouseOver(wxMouseEvent &event);
	// target for function pointer need to be static!
	static void DoDetect(void*);
};

class my1BUTCtrl : public my1SWICtrl
{
protected:
	wxColor mLiteUp, mLiteDn;
public:
	my1BUTCtrl(wxWindow*, wxWindowID,
		int aWidth=KEY_SIZE_PANEL,int aHeight=KEY_SIZE_PANEL);
	virtual void DrawSWITCH(wxBitmap*,bool);
	void OnMouseClick(wxMouseEvent& event);
};

class my1ENCkPad : public my1SWICtrl
{
public:
	my1ENCkPad(wxWindow*, wxWindowID, bool do_dummy=false,
		int aWidth=KEY_SIZE_PANEL,int aHeight=KEY_SIZE_PANEL);
	virtual void DrawSWITCH(wxBitmap*,bool);
};

class my1KEYCtrl : public wxPanel
{
protected:
	bool mPushed;
	int mKeyID;
	wxStaticText *mText;
public:
	my1KEYCtrl(wxWindow*,wxWindowID,int,int,int,const wxString&);
	~my1KEYCtrl();
	int KeyID(void);
	wxWindow* GetNextCtrl(wxWindowList::Node**);
	void OnPaint(wxPaintEvent& event);
	void OnResize(wxSizeEvent& event);
	void OnMouseClick(wxMouseEvent& event);
};

class my1INTCtrl : public my1SWICtrl
{
protected:
	wxStaticText *mText;
public:
	my1INTCtrl(wxWindow*,wxWindowID,int,int,const wxString&);
	~my1INTCtrl();
	void OnPaint(wxPaintEvent& event);
	void OnResize(wxSizeEvent& event);
	void OnMouseClick(wxMouseEvent& event);
};

#endif
