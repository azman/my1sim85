//------------------------------------------------------------------------------
// wxform_handlers.cpp
// - method implementations for event handlers in wxform
// - to be included directly in wxform
//------------------------------------------------------------------------------
void my1Form::OnFormClose(wxCloseEvent& event) {
	// browse open notebook page
	int cCount = mNoteBook->GetPageCount();
	for (int cLoop=0;cLoop<cCount;cLoop++) {
		wxWindow *cTarget = mNoteBook->GetPage(cLoop);
		if (cTarget->IsKindOf(wxCLASSINFO(my1CodeEdit))) {
			my1CodeEdit *cEditor = (my1CodeEdit*) cTarget;
			if (cEditor->GetModify()) {
				wxString cTitle = wxS("Changes in '") + cEditor->GetFileName();
				cTitle += wxS("' NOT Saved!");
				wxString cMessage = wxS("Save Before Closing?");
				cMessage += wxS(" [Cancel] will ignore all remaining!");
				int cGoSave = wxMessageBox(cMessage,cTitle,
					wxYES_NO|wxCANCEL|wxCANCEL_DEFAULT|wxICON_QUESTION,this);
				if (cGoSave==wxYES) this->SaveEdit(cTarget);
				else if (cGoSave==wxCANCEL) break;
			}
		}
	}
	if (event.CanVeto()) event.Skip();
	else this->Destroy();
}
//------------------------------------------------------------------------------
void my1Form::OnQuit(wxCommandEvent& event) {
	Close(true);
}
//------------------------------------------------------------------------------
void my1Form::OnNew(wxCommandEvent& event) {
	wxString cFileName = wxS("");
	this->OpenEdit(cFileName);
}
//------------------------------------------------------------------------------
void my1Form::OnLoad(wxCommandEvent& event) {
	wxFileName cThisPath(mThisPath,"");
	cThisPath.AppendDir(wxS("asm"));
	wxFileDialog *cSelect = new wxFileDialog(this,wxS("Select code file"),
		wxS(""),wxS(""),wxS("Any file (*.*)|*.*"),
		wxFD_OPEN|wxFD_FILE_MUST_EXIST);
	cSelect->SetWildcard("ASM files (*.asm)|*.asm|Any file (*.*)|*.*");
	cSelect->SetDirectory(cThisPath.GetPath());
	if (cSelect->ShowModal()!=wxID_OK) return;
	wxString cFileName = cSelect->GetPath();
	this->OpenEdit(cFileName);
}
//------------------------------------------------------------------------------
void my1Form::OnSave(wxCommandEvent &event) {
	int cSelect = mNoteBook->GetSelection();
	if (cSelect<0) return;
	wxWindow *cTarget = mNoteBook->GetPage(cSelect);
	if (!cTarget->IsKindOf(wxCLASSINFO(my1CodeEdit))) return;
	bool cSaveAs = false;
	if (event.GetId()==MY1ID_SAVEAS) cSaveAs = true;
	this->SaveEdit(cTarget,cSaveAs);
}
//------------------------------------------------------------------------------
void my1Form::OnAbout(wxCommandEvent& event) {
	wxAboutDialogInfo cAboutInfo;
	cAboutInfo.SetName(MY1APP_PROGNAME);
	cAboutInfo.SetVersion(MY1APP_PROGVERS);
	cAboutInfo.SetDescription(wxS(ABOUT_TITLE));
	cAboutInfo.SetCopyright(ABOUT_COPYRIGHT);
	cAboutInfo.SetWebSite(ABOUT_WEBSITE);
	cAboutInfo.AddDeveloper(ABOUT_AUTHOR);
	wxAboutBox(cAboutInfo,this);
}
//------------------------------------------------------------------------------
void my1Form::OnWhatsNew(wxCommandEvent& event) {
	wxFileName cFileName(mThisPath,wxS("CHANGELOG"));
	if (!cFileName.IsOk()||!cFileName.FileExists()) {
		wxMessageBox(wxS("Cannot find file 'CHANGELOG'!"),wxS("[INFO]"),
			wxOK|wxICON_INFORMATION);
		return;
	}
	/** wxTE_AUTO_SCROLL no longer defined in wxWidgets-3.1.5 - default? */
	wxTextCtrl *cChangeLog = new wxTextCtrl(mNoteBook, wxID_ANY,
		wxS(MY1APP_TITLE" CHANGELOG\n\n"), wxDefaultPosition, wxDefaultSize,
		wxTE_MULTILINE|wxTE_READONLY, wxDefaultValidator);
	wxFont cFont(CONS_FONT_SIZE,wxFONTFAMILY_TELETYPE,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL,
		false,wxEmptyString,wxFONTENCODING_ISO8859_1);
	cChangeLog->SetFont(cFont);
	cChangeLog->LoadFile(cFileName.GetFullPath()); // already checked?
	mNoteBook->AddPage(cChangeLog, wxS("CHANGELOG"),true);
}
//------------------------------------------------------------------------------
void my1Form::OnReadMe(wxCommandEvent& event) {
	wxFileName cFileName(mThisPath,wxS("README"));
	if (!cFileName.IsOk()||!cFileName.FileExists()) {
		wxMessageBox(wxS("Cannot find file 'README'!"),wxS("[INFO]"),
			wxOK|wxICON_INFORMATION);
		return;
	}
	/** wxTE_AUTO_SCROLL no longer defined in wxWidgets-3.1.5 - default? */
	wxTextCtrl *cReadMe = new wxTextCtrl(mNoteBook, wxID_ANY,
		wxS(MY1APP_TITLE" README\n\n"), wxDefaultPosition, wxDefaultSize,
		wxTE_MULTILINE|wxTE_READONLY, wxDefaultValidator);
	wxFont cFont(CONS_FONT_SIZE,wxFONTFAMILY_TELETYPE,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL,
		false,wxEmptyString,wxFONTENCODING_ISO8859_1);
	cReadMe->SetFont(cFont);
	cReadMe->LoadFile(cFileName.GetFullPath()); // already checked?
	mNoteBook->AddPage(cReadMe, wxS("README"),true);
}
//------------------------------------------------------------------------------
void my1Form::OnMenuHighlight(wxMenuEvent& event) {
	event.Skip(false);
}
//------------------------------------------------------------------------------
void my1Form::OnAssemble(wxCommandEvent &event) {
	my1CodeEdit *cEditor = (my1CodeEdit*) m8085.GetCodeLink();
	wxString cFileLST;
	char *cDoList = 0x0;
	if (!cEditor) {
		int cSelect = mNoteBook->GetSelection();
		wxWindow *cTarget = mNoteBook->GetPage(cSelect);
		if (!cTarget->IsKindOf(wxCLASSINFO(my1CodeEdit))) return;
		cEditor = (my1CodeEdit*) cTarget;
	}
	if (cEditor->GetModify()) {
		int cGoSave = wxMessageBox(wxS("Save & Continue?"),
			wxS("File modified!"),wxOK|wxCANCEL,this);
		if (cGoSave==wxCANCEL) return;
		this->SaveEdit((wxWindow*)cEditor);
	}
	if (this->mOptions.mComp_DoList) {
		cFileLST = cEditor->GetPathName() +
			cEditor->GetFileNoXT() + wxS(".lst");
		wxCStrData tbuf = cFileLST.c_str();
		cDoList = (char*) tbuf.AsChar();
	}
	if (m8085.Assemble(cEditor->GetFullName().ToAscii(),cDoList)) {
		wxString cStatus;
		if (m8085.ZeroCode()) {
			cStatus = wxS("No code assembled!\n");
			this->PrintDebugMessage(cStatus,(char*)"NO-CODE");
		}
		else {
			cStatus = wxS("Code in ") +
				cEditor->GetFileName() + wxS(" assembled!\n");
			this->PrintInfoMessage(cStatus,(char*)"ASM");
		}
		this->ShowStatus(cStatus);
		m8085.SetCodeLink((void*)cEditor);
		cEditor->Assembled();
	}
	else this->PrintErrorMessage(wxS("Assembler failed?!\n"));
}
//------------------------------------------------------------------------------
void my1Form::OnSimulate(wxCommandEvent &event) {
	word32_t stat;
	wxString cStatus;
	my1CodeEdit *cEditor;
	if (!m8085.Built()) {
		this->PrintErrorMessage(wxS("No system built for simulation!\n"));
		return;
	}
	cEditor = (my1CodeEdit*) m8085.GetCodeLink();
	if (!cEditor||cEditor->GetModify()||!cEditor->IsAssembled()) {
		this->OnAssemble(event);
		if (!m8085.Ready()||m8085.ZeroCode()) return;
	}
	// just in case did not get it the first time
	cEditor = (my1CodeEdit*) m8085.GetCodeLink();
	if (!cEditor) return;
	if (!m8085.Ready()||m8085.ZeroCode()) {
		this->PrintErrorMessage(wxS("No code ready for simulation!\n"));
		return;
	}
	m8085.SetStartAddress(mOptions.mSims_StartADDR);
	if (m8085.Simulate(0)) { // force a reset!
		cStatus = wxS("Ready for Simulation!\n");
		this->ShowStatus(cStatus);
		this->PrintInfoMessage(cStatus,(char*)"SIM");
		this->SimulationMode();
		cEditor->SetReadOnly(mSimulationMode);
		cEditor->ExecLine(m8085.GetNextLine()-1);
	}
	else {
		if (m8085.Flag(SIM85_FLAG_ECODEFIND)) {
			cStatus = wxS("No code @ address 0x") +
				wxString::Format(wxS("%04X"),m8085.GetStartAddress());
			this->PrintInfoMessage(cStatus);
			return;
		}
		if (m8085.Flag(SIM85_FLAG_ECODE2MEM)) {
			stat = m8085.C2mem();
			cStatus = wxS("Cannot load code to memory! {Error(s):") +
				wxString::Format(wxS("%d"),stat&0xffff) +
				wxS(" , Byte(s) Loaded:") +
				wxString::Format(wxS("%d"),stat>>16) + wxS("/") +
				wxString::Format(wxS("%d"),m8085.CodeSize()) + wxS("}");
			this->PrintErrorMessage(cStatus);
			return;
		}
		cStatus = wxS("Unknown error starting simulation!\n");
		this->ShowStatus(cStatus);
		this->PrintErrorMessage(cStatus);
	}
}
//------------------------------------------------------------------------------
void my1Form::OnGenerate(wxCommandEvent &event) {
	wxString cStatus;
	my1CodeEdit *cEditor = (my1CodeEdit*) m8085.GetCodeLink();
	if (!cEditor||cEditor->GetModify()||!cEditor->IsAssembled()) {
		this->OnAssemble(event);
		if (!m8085.Ready()||m8085.ZeroCode()) return;
	}
	cEditor = (my1CodeEdit*) m8085.GetCodeLink();
	if (!cEditor) return;
	wxString cFileHEX = cEditor->GetPathName() +
		cEditor->GetFileNoXT() + wxS(".hex");
	if (m8085.Generate(cFileHEX.ToAscii())) {
		cStatus = wxS("HEX file generated from ") +
			cEditor->GetFileName() + wxS(" [") + cFileHEX + wxS("].\n");
		this->PrintInfoMessage(cStatus,(char*)"HEX");
	}
	else {
		cStatus = wxS("Cannot generate HEX file!\n");
		this->PrintErrorMessage(cStatus);
	}
	this->ShowStatus(cStatus);
}
//------------------------------------------------------------------------------
void my1Form::OnSysLoad(wxCommandEvent &event) {
	wxFileName cThisPath(mThisPath,"");
	cThisPath.AppendDir(wxS("sys"));
	wxFileDialog *cSelect = new wxFileDialog(this,wxS("Select config file"),
		wxS(""),wxS(""),wxS("Any file (*.*)|*.*"),
		wxFD_OPEN|wxFD_FILE_MUST_EXIST);
	cSelect->SetWildcard("8085-System files (*.8085)|*.8085|"
		"Any file (*.*)|*.*");
	cSelect->SetDirectory(cThisPath.GetPath());
	if (cSelect->ShowModal()!=wxID_OK) return;
	wxString cFilename = cSelect->GetPath();
	if (!this->LoadSystem(cFilename)) {
		wxString cMessage = wxString::Format(
			wxS("Cannot load system from '%s'!"),cFilename.ToAscii());
		wxMessageBox(cMessage,wxS("[System Load Error]"),
			wxOK|wxICON_ERROR);
	}
}
//------------------------------------------------------------------------------
void my1Form::OnSysSave(wxCommandEvent &event) {
	wxFileName cThisPath(mThisPath,"");
	cThisPath.AppendDir(wxS("sys"));
	wxFileDialog *cSelect = new wxFileDialog(this,wxS("Assign File Name"),
		wxS(""),wxS(""),wxS("Any file (*.*)|*.*"),
		wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
	cSelect->SetWildcard("8085-System files (*.8085)|*.8085|"
		"Any file (*.*)|*.*");
	cSelect->SetDirectory(cThisPath.GetPath());
	if (cSelect->ShowModal()!=wxID_OK) return;
	wxString cFilename = cSelect->GetPath();
	if (cSelect->GetFilterIndex()==0) {
		if (cFilename.Right(5)!=wxS(".8085"))
			cFilename += wxS(".8085");
	}
	if (!this->SaveSystem(cFilename)) {
		wxString cMessage = wxString::Format(
			wxS("Cannot save system to '%s'!"),cFilename.ToAscii());
		wxMessageBox(cMessage,wxS("[System Save Error]"),
			wxOK|wxICON_ERROR);
	}
}
//------------------------------------------------------------------------------
void my1Form::OnCheckFont(wxKeyEvent &event) {
	if (!event.ControlDown()) event.Skip();
	int cSelect = mNoteBook->GetSelection();
	wxWindow *cTarget = mNoteBook->GetPage(cSelect);
	if (!cTarget->IsKindOf(wxCLASSINFO(my1CodeEdit))) return;
	my1CodeEdit *cEditor = (my1CodeEdit*) cTarget;
	int cKeyCode = event.GetKeyCode();
	switch (cKeyCode) {
		case WXK_NUMPAD_ADD: cEditor->LargerFont(); break;
		case WXK_NUMPAD_SUBTRACT: cEditor->SmallerFont(); break;
		default: event.Skip();
	}
}
//------------------------------------------------------------------------------
void my1Form::OnCheckConsole(wxKeyEvent &event) {
	if (!mShowSystem) return;
	//if(event.GetUnicodeKey() != WXK_NONE) // check if printable?
	int cKeyCode = event.GetKeyCode();
	switch (cKeyCode) {
		case WXK_UP:
			if (mCmdHistIndex>0) {
				mCmdHistIndex--;
				mCommand->Clear();
				mCommand->AppendText(mCmdHistory[mCmdHistIndex]);
			}
			break;
		case WXK_DOWN:
			if (mCmdHistIndex<(int)mCmdHistory.GetCount()) {
				mCmdHistIndex++;
				mCommand->Clear();
				if (mCmdHistIndex<(int)mCmdHistory.GetCount())
					mCommand->AppendText(mCmdHistory[mCmdHistIndex]);
			}
			break;
		case WXK_RETURN:
			this->OnExecuteConsole((wxCommandEvent&)event);
			break;
		default: event.Skip();
	}
}
//------------------------------------------------------------------------------
void my1Form::OnExecuteConsole(wxCommandEvent &event) {
	bool cValidCommand = false;
	wxString cCommandLine = mCommand->GetLineText(0);
	mCommand->SelectAll(); mCommand->Cut();
	if (!cCommandLine.Length()) {
		mConsole->AppendText("\n");
		return;
	}
	wxString cCommandWord = cCommandLine.BeforeFirst(' ');
	wxString cParameters = cCommandLine.AfterFirst(' ');
	if (!cCommandWord.Cmp(wxS("show"))) {
		wxString cParam = cParameters.BeforeFirst(' ');
		int cEqual = cParam.Find('=');
		if (cEqual==wxNOT_FOUND) {
			if (!cParam.Cmp(wxS("system"))) {
				this->PrintPeripheralInfo();
				cValidCommand = true;
			}
			else if (!cParam.Cmp(wxS("info"))) {
				if (!mSimulationMode) {
					this->PrintMessage("Only available during simulation!");
					return;
				}
				this->PrintCodexInfo(m8085.Next());
			}
			else if (!cParam.Cmp(wxS("prev"))) {
				if (!mSimulationMode) {
					this->PrintMessage("Only available during simulation!");
					return;
				}
				this->PrintCodexInfo();
			}
			else this->PrintUnknownParameter(cParameters,cCommandWord);
		}
		else {
			wxString cKey = cParam.BeforeFirst('=');
			wxString cValue = cParam.AfterFirst('=');
			wxString cMesg;
			if (!cKey.Cmp(wxS("mem"))) {
				unsigned long cStart;
				if (cValue.ToULong(&cStart,16)&&cStart<=0xFFFF) {
					if (m8085.Memory(cStart)) {
						this->PrintMemoryContent(cStart);
						cValidCommand = true;
					}
					else {
						cMesg = wxS("No memory at location 0x")+cValue+"!";
						this->PrintErrorMessage(cMesg);
					}
				}
				else {
					cMesg = wxS("Invalid memory location '")+cValue+"'!";
					this->PrintErrorMessage(cMesg);
				}
			}
			else if (!cKey.Cmp(wxS("minimv"))) {
				unsigned long cStart;
				if (cValue.ToULong(&cStart,16)&&cStart<=0xFFFF) {
					if (m8085.Memory(cStart)) {
						this->CreateMemoryMiniPanel(cStart);
						cValidCommand = true;
					}
					else {
						cMesg = wxS("No memory at location 0x")+cValue+"!";
						this->PrintErrorMessage(cMesg);
					}
				}
				else {
					cMesg = wxS("Invalid memory location '")+cValue+"'!";
					this->PrintErrorMessage(cMesg);
				}
			}
			else this->PrintUnknownParameter(cParameters,cCommandWord);
		}
	}
	else if (!cCommandWord.Cmp(wxS("build"))) {
		if (mSimulationMode) {
			this->PrintMessage("Build mode disabled during simulation!");
			return;
		}
		wxString cParam = cParameters.BeforeFirst(' ');
		int cEqual = cParam.Find('=');
		if (cEqual==wxNOT_FOUND) {
			if (!cParam.Cmp(wxS("default"))) {
				this->ConnectROM();
				this->ConnectRAM();
				this->ConnectPPI();
				cValidCommand = true;
			}
			else if (!cParam.Cmp(wxS("reset"))) {
				this->SystemDisconnect();
				cValidCommand = true;
			}
			else this->PrintUnknownParameter(cParameters,cCommandWord);
		}
		else {
			wxString cKey = cParam.BeforeFirst('=');
			wxString cValue = cParam.AfterFirst('=');
			if (!cKey.Cmp(wxS("rom"))) {
				unsigned long cStart;
				if (cValue.ToULong(&cStart,16)&&cStart<=0xFFFF)
					this->ConnectROM(cStart);
				else this->PrintUnknownParameter(cValue,cKey);
				cValidCommand = true;
			}
			else if (!cKey.Cmp(wxS("ram"))) {
				unsigned long cStart;
				if (cValue.ToULong(&cStart,16)&&cStart<=0xFFFF)
					this->ConnectRAM(cStart);
				else this->PrintUnknownParameter(cValue,cKey);
				cValidCommand = true;
			}
			else if (!cKey.Cmp(wxS("ppi"))) {
				unsigned long cStart;
				if (cValue.ToULong(&cStart,16)&&cStart<=0xFF)
					this->ConnectPPI(cStart);
				else this->PrintUnknownParameter(cValue,cKey);
				cValidCommand = true;
			}
			else this->PrintUnknownParameter(cParameters,cCommandWord);
		}
	}
	else if (!cCommandWord.Cmp(wxS("clear"))) {
		mConsole->Clear();
		mConsole->AppendText(wxString::Format(wxS("Welcome to %s\n\n"),
			MY1APP_TITLE));
		cValidCommand = true;
	}
	else if (!cCommandWord.Cmp(wxS("help"))) {
		this->PrintHelp();
		cValidCommand = true;
	}
	else if (!cCommandWord.Cmp(wxS("data"))) {
		wxString cParam = cParameters.BeforeFirst(' ');
		wxString cValue;
		wxString cMesg;
		unsigned long cAddr, cData;
		int cEqual = cParam.Find('=');
		if (cEqual!=wxNOT_FOUND) {
			cValue = cParam.AfterFirst('=');
			cParam = cParam.BeforeFirst('=');
		}
		if (cParam[0]!='@') {
			cMesg = wxS("Invalid data address format '")+cParam+"'!";
			this->PrintErrorMessage(cMesg);
		}
		else {
			cParam = cParam.Mid(1);
			if (!cParam.ToULong(&cAddr,16)||cAddr>0xFFFF) {
				cMesg = wxS("Invalid memory location '")+cParam+"'!";
				this->PrintErrorMessage(cMesg);
			}
			else if (!m8085.Memory((word32_t)cAddr)) {
				cMesg = wxS("No memory at location 0x")+cParam+"!";
				this->PrintErrorMessage(cMesg);
			}
			else {
				byte08_t temp;
				if (cValue.Length()) {
					// writing to memory - get data
					temp = 10;
					if (cValue[0]=='0'&&(cValue[1]=='x'||cValue[1]=='X')) {
						cValue = cValue.Mid(2);
						temp = 16;
					}
					if (!cValue.ToULong(&cData,temp)||cData>0xFF) {
						cMesg = wxS("Invalid data value '")+cValue+"'!";
						this->PrintErrorMessage(cMesg);
					}
					else if (!m8085.MemoryWrite(cAddr,cData)) {
						cMesg = wxS("Failed to write memory location 0x")+
							cParam+"!";
						this->PrintErrorMessage(cMesg);
					}
					else {
						this->UpdateMemoryPanel();
						m8085.MemoryRead(cAddr,temp);
						cMesg = wxS("[0x")+cParam+"] = "+
							wxString::Format(wxS("0x%02X"),temp)+" ("+
							wxString::Format(wxS("%d"),(int)temp)+")!";
						this->PrintInfoMessage(cMesg);
						cValidCommand = true;
					}
				}
				else {
					// show memory at location
					if (!m8085.MemoryRead(cAddr,temp)) {
						cMesg = wxS("Failed to read memory location 0x")+
							cParam+"!";
						this->PrintErrorMessage(cMesg);
					}
					else {
						cMesg = wxS("[0x")+cParam+"] = "+
							wxString::Format(wxS("0x%02X"),temp)+" ("+
							wxString::Format(wxS("%d"),(int)temp)+")!";
						this->PrintInfoMessage(cMesg);
						cValidCommand = true;
					}
				}
			}
		}
	}
	else this->PrintUnknownCommand(cCommandWord);
	if (cValidCommand) {
		if (!mCmdHistory.GetCount()||mCmdHistory.Last()!=cCommandLine) {
			mCmdHistory.Add(cCommandLine);
			if (mCmdHistory.GetCount()>CMD_HISTORY_COUNT)
				mCmdHistory.RemoveAt(0);
		}
	}
	// reset command history index
	mCmdHistIndex = mCmdHistory.GetCount();
}
//------------------------------------------------------------------------------
void my1Form::OnSimulationPick(wxCommandEvent &event) {
	if (!mSimulationMode) return;
	switch (event.GetId()) {
		case MY1ID_SIMSEXEC:
			if (mSimulationStepping) mSimulationRunning = true;
			else mSimulationRunning = !mSimulationRunning;
			mSimulationStepping = false;
			break;
		case MY1ID_SIMSSTEP:
			mSimulationRunning = true;
			mSimulationStepping = true;
			break;
		default:
			mSimulationRunning = false;
			mSimulationStepping = false;
	}
	if (mSimulationRunning) {
		my1CodeEdit *cEditor = (my1CodeEdit*) m8085.GetCodeLink();
		if (cEditor) cEditor->ShowLine(mSimulationStepping);
		if (mSimulationStepping)
			this->SetStatusText(MSG_SYSTEM_SSIM,STATUS_SYS_INDEX);
		else
			this->SetStatusText(MSG_SYSTEM_RSIM,STATUS_SYS_INDEX);
		if (!mSimExecTimer->IsRunning())
			mSimExecTimer->Start(SIM_EXEC_PERIOD,wxTIMER_ONE_SHOT);
	}
	else this->SetStatusText(MSG_SYSTEM_MSIM,STATUS_SYS_INDEX);
}
//------------------------------------------------------------------------------
void my1Form::OnSimulationInfo(wxCommandEvent &event) {
	if (!mSimulationMode) return;
	if (event.GetId()==MY1ID_SIMSINFO)
		this->PrintCodexInfo(m8085.Next());
	else if (event.GetId()==MY1ID_SIMSPREV)
		this->PrintCodexInfo();
	else if (event.GetId()==MY1ID_SIMSMIMV)
		this->CreateMemoryMiniPanel();
	else if (event.GetId()==MY1ID_SIMRESET) {
		if (mSimExecTimer->IsRunning())
			mSimExecTimer->Stop();
		this->SetStatusText(MSG_SYSTEM_MSIM,STATUS_SYS_INDEX);
		m8085.Simulate(0);
		my1CodeEdit *cEditor = (my1CodeEdit*) m8085.GetCodeLink();
		if (cEditor) cEditor->ExecLine(m8085.GetCodexLine()-1);
	}
	else if (event.GetId()==MY1ID_SIMSBRKP) {
		my1CodeEdit *cEditor = (my1CodeEdit*) m8085.GetCodeLink();
		if(cEditor) cEditor->ToggleBreak(cEditor->GetCurrentLine());
	}
	else if(event.GetId()==MY1ID_SIMSEXIT) {
		if (mSimExecTimer->IsRunning())
			mSimExecTimer->Stop();
		my1CodeEdit *cEditor = (my1CodeEdit*) m8085.GetCodeLink();
		cEditor->ExecDone();
		m8085.SetCodeLink((void*)0x0);
		mSimulationRunning = false;
		mSimulationStepping = false;
		this->SimulationMode(false);
		cEditor->SetReadOnly(mSimulationMode);
	}
}
//------------------------------------------------------------------------------
void my1Form::OnBuildSelect(wxCommandEvent &event) {
	if (mSimulationMode) {
		this->PrintMessage("Build mode disabled during simulation!\n");
		return;
	}
	int cAddress;
	switch (event.GetId()) {
		case MY1ID_BUILDRST: this->SystemDisconnect(); break;
		case MY1ID_BUILDDEF: this->SystemDefault(); break;
		case MY1ID_BUILDNFO: this->PrintPeripheralInfo(); break;
		case MY1ID_BUILDROM:
			cAddress = this->GetBuildAddress(wxS("[BUILD] Adding 2764 ROM"));
			if (cAddress<0) return;
			this->ConnectROM(cAddress);
			break;
		case MY1ID_BUILDRAM:
			cAddress = this->GetBuildAddress(wxS("[BUILD] Adding 6264 RAM"));
			if (cAddress<0) return;
			this->ConnectRAM(cAddress);
			break;
		case MY1ID_BUILDPPI:
			cAddress = this->GetBuildAddress(wxS("[BUILD] Adding 8255 PPI"));
			if (cAddress<0) return;
			this->ConnectPPI(cAddress);
			break;
	}
}
//------------------------------------------------------------------------------
void my1Form::OnClosePane(wxAuiManagerEvent &event) {
	wxAuiPaneInfo *cPane = event.GetPane();
	// rearrange if a toolbar
	if (cPane->IsToolbar()) {
		wxAuiPaneInfo& cPaneDevC = mMainUI.GetPane(wxS("devcTool"));
		if (cPaneDevC.IsOk()&&cPaneDevC.IsDocked()&&cPaneDevC.IsShown())
			cPaneDevC.Position(TOOL_DEVC_POS);
		wxAuiPaneInfo& cPaneProc = mMainUI.GetPane(wxS("procTool"));
		if (cPaneProc.IsOk()&&cPaneProc.IsDocked()&&cPaneProc.IsShown())
			cPaneProc.Position(TOOL_PROC_POS);
	}
	// browse for mini mem viewer!
	my1MiniViewer *pViewer = mFirstViewer, *pPrev = 0x0;
	while (pViewer) {
		wxString cPanelName = wxS("miniMV")
			+ wxString::Format(wxS("%04X"),pViewer->mStart);
		wxAuiPaneInfo &tPane = mMainUI.GetPane(cPanelName);
		if (cPane==&tPane) {
			if (!pPrev) mFirstViewer = pViewer->mNext;
			else pPrev->mNext = pViewer->mNext;
			delete pViewer;
			break;
		}
		pPrev = pViewer;
		pViewer = pViewer->mNext;
	}
}
//------------------------------------------------------------------------------
void my1Form::OnShowSystem(wxCommandEvent &event) {
	mShowSystem = event.IsChecked();
	wxWindow *cTarget = mNoteBook->GetCurrentPage();
	bool cEditMode = false;
	if (cTarget)
		cEditMode = cTarget->IsKindOf(wxCLASSINFO(my1CodeEdit));
	UpdateMenuState(cEditMode);
	UpdateProcTool(cEditMode);
	wxAuiPaneInfo& cPaneSyst = mMainUI.GetPane(wxS("systPanel"));
	if (cPaneSyst.IsOk()) cPaneSyst.Show(mShowSystem);
	wxAuiPaneInfo& cPaneRegs = mMainUI.GetPane(wxS("regsPanel"));
	if (cPaneRegs.IsOk()) cPaneRegs.Show(mShowSystem);
	wxAuiPaneInfo& cPaneIntr = mMainUI.GetPane(wxS("intrPanel"));
	if (cPaneIntr.IsOk()) cPaneIntr.Show(mShowSystem);
	wxAuiPaneInfo& cPaneDevC = mMainUI.GetPane(wxS("devcTool"));
	if (cPaneDevC.IsOk()) cPaneDevC.Show(mShowSystem);
	if (!mShowSystem) {
		wxAuiPaneInfo& cPaneMems = mMainUI.GetPane(wxS("memsPanel"));
		if (cPaneMems.IsOk()) cPaneMems.Show(mShowSystem);
		// delete created devices/controls?
		this->RemoveControls();
	}
	if (mComsPanel) {
		if (mShowSystem) mComsPanel->Show();
		else mComsPanel->Hide();
	}
	mMainUI.Update();
}
//------------------------------------------------------------------------------
void my1Form::OnShowPanel(wxCommandEvent &event) {
	wxString cToolName = wxS("");
	switch(event.GetId()) {
		case MY1ID_VIEW_SYSTPANE:
			if (mShowSystem) { cToolName = wxS("systPanel"); } break;
		case MY1ID_VIEW_REGSPANE:
			if (mShowSystem) { cToolName = wxS("regsPanel"); } break;
		case MY1ID_VIEW_MEMSPANE:
			if (mShowSystem) { cToolName = wxS("memsPanel"); } break;
		case MY1ID_VIEW_INTRPANE:
			if (mShowSystem) { cToolName = wxS("intrPanel"); } break;
		case MY1ID_VIEW_CONSPANE: cToolName = wxS("consPanel"); break;
		case MY1ID_VIEW_TERMPANE: cToolName = wxS("termPanel"); break;
		case MY1ID_CREATE_MINIMV: this->CreateMemoryMiniPanel(); break;
		case MY1ID_CREATE_DV7SEG: this->CreateDevice7SegPanel(); break;
		case MY1ID_CREATE_DVKPAD: this->CreateDeviceKPadPanel(); break;
		case MY1ID_CREATE_DEVLED: this->CreateDeviceLEDPanel(); break;
		case MY1ID_CREATE_DEVSWI: this->CreateDeviceSWIPanel(); break;
		case MY1ID_CREATE_DEVBUT: this->CreateDeviceBUTPanel(); break;
		case MY1ID_CREATE_DEVLVD:
			this->CreateDeviceLEDPanel(wxEmptyString,true);
			break;
	}
	if (cToolName.Length()>0) {
		wxAuiPaneInfo& cPane = mMainUI.GetPane(cToolName);
		if (cPane.IsOk()) {
			cPane.Show();
			mMainUI.Update();
		}
	}
	return;
}
//------------------------------------------------------------------------------
void my1Form::OnCheckOptions(wxCommandEvent &event) {
	my1OptionDialog *prefDialog = new my1OptionDialog(this,
		wxS("Options"),this->mOptions);
	prefDialog->ShowModal();
	prefDialog->Destroy();
	if (this->mOptions.mChanged) {
		this->mOptions.mChanged = false;
		int cCount = mNoteBook->GetPageCount();
		for (int cLoop=0;cLoop<cCount;cLoop++) {
			// set for all opened editor?
			wxWindow *cTarget = mNoteBook->GetPage(cLoop);
			if (cTarget->IsKindOf(wxCLASSINFO(my1CodeEdit))) {
				my1CodeEdit *cEditor = (my1CodeEdit*) cTarget;
				cEditor->SetViewEOL(this->mOptions.mEdit_ViewEOL);
				cEditor->SetViewWhiteSpace(this->mOptions.mEdit_ViewWS?1:0);
				cEditor->Refresh();
			}
		}
	}
}
//------------------------------------------------------------------------------
void my1Form::OnStatusTimer(wxTimerEvent& event) {
	this->SetStatusText(wxS(""),STATUS_MSG_INDEX);
}
//------------------------------------------------------------------------------
void my1Form::OnSimExeTimer(wxTimerEvent& event) {
	bool cWasHalted = m8085.Halted();
	if (m8085.Simulate()) {
		int line = m8085.GetNextLine()-1;
		my1CodeEdit *cEditor = (my1CodeEdit*) m8085.GetCodeLink();
		cEditor->ExecLine(line,mSimulationStepping);
		if (mOptions.mSims_ShowRunInfo)
			this->PrintCodexInfo();
		if (cEditor->IsBreakLine(line))
			mSimulationStepping = true;
		if (!m8085.Curr()) {
			this->PrintInfoMessage("No code @ address!");
			mSimulationStepping = true;
		}
		else if (m8085.Halted()) {
			if (!cWasHalted) this->PrintInfoMessage("System HALTED!");
			mSimulationStepping = mOptions.mSims_PauseOnHALT;
		}
		else if (m8085.Interrupted()) {
			this->PrintInfoMessage("System Interrupt!");
			mSimulationStepping = mOptions.mSims_PauseOnINTR;
		}
	}
	else if (!cWasHalted) {
		my1CodeEdit *cEditor = (my1CodeEdit*) m8085.GetCodeLink();
		wxMessageBox(wxS("Simulation Terminated!"),wxS("[SIM Error]"),
			wxOK|wxICON_EXCLAMATION);
		mSimulationRunning = false;
		this->SimulationMode(false);
		cEditor->SetReadOnly(mSimulationMode);
	}
	if (mSimulationRunning&&!mSimulationStepping)
		mSimExecTimer->Start(SIM_EXEC_PERIOD,wxTIMER_ONE_SHOT);
}
//------------------------------------------------------------------------------
void my1Form::OnPageChanging(wxAuiNotebookEvent &event) {
	if (mSimulationMode) event.Veto();
}
//------------------------------------------------------------------------------
void my1Form::OnPageChanged(wxAuiNotebookEvent &event) {
	wxWindow *cTarget = mNoteBook->GetPage(event.GetSelection());
	if (!cTarget) return;
	bool cEditMode = cTarget->IsKindOf(wxCLASSINFO(my1CodeEdit));
	m8085.SetCodeLink((void*)0x0);
	wxMenuBar *cMenuBar = this->GetMenuBar();
	cMenuBar->EnableTop(cMenuBar->FindMenu(wxS("Tool")),cEditMode);
	UpdateMenuState(cEditMode);
	UpdateProcTool(cEditMode);
	mMainUI.Update();
}
//------------------------------------------------------------------------------
void my1Form::OnPageClosing(wxAuiNotebookEvent &event) {
	wxWindow *cTarget = mNoteBook->GetPage(event.GetSelection());
	if (cTarget->IsKindOf(wxCLASSINFO(my1CodeEdit))) {
		my1CodeEdit *cEditor = (my1CodeEdit*) cTarget;
		if (cEditor->GetModify()) {
			int cGoSave = wxMessageBox(wxS("Save Before Closing?"),
				wxS("Code Modified!"),wxYES_NO|wxCANCEL,this);
			if (cGoSave==wxYES) this->SaveEdit(cTarget);
			else if(cGoSave==wxCANCEL) event.Veto();
		}
	}
	else if (!cTarget->IsKindOf(wxCLASSINFO(wxTextCtrl))) {
		event.Veto(); // welcome page is always visible!
	}
}
//------------------------------------------------------------------------------
void my1Form::OnBITPanelClick(wxMouseEvent &event) {
	mPortPanel = 0x0;
	if (event.RightDown()) {
		wxMenu *cMenuPop = this->GetDevicePortMenu();
		if (!cMenuPop) { event.Skip(); return; }
		mPortPanel = (wxPanel*) FindWindowById(event.GetId(),this);
		if (!mPortPanel) { event.Skip(); return; }
		mPortPanel->Connect(wxID_ANY,wxEVT_COMMAND_MENU_SELECTED,
			WX_CEH(my1Form::OnBITPortClick),NULL,this);
		mPortPanel->PopupMenu(cMenuPop);
	}
	else if (event.LeftDClick()) {
		wxWindow* pTarget = FindWindowById(event.GetId(),this);
		wxAuiPaneInfo& cPane = mMainUI.GetPane(pTarget);
		if (cPane.IsOk()) {
			wxString cLabel = mMainUI.SavePaneInfo(cPane);
			cLabel = cLabel.Mid(cLabel.First(wxS("caption=")));
			cLabel = cLabel.BeforeFirst(';');
			cLabel = cLabel.AfterFirst('=');
			wxTextEntryDialog* cDialog = new wxTextEntryDialog(this,
				wxS("Enter new caption"), wxS("Changing Caption - ")+cLabel);
			if (cDialog->ShowModal()!=wxID_OK)
				return;
			wxString cCaption = cDialog->GetValue();
			if (cCaption.Length()) {
				cPane.Caption(cCaption);
				cPane.CaptionVisible();
				mMainUI.Update();
			}
			else {
				cPane.CaptionVisible(false);
				mMainUI.Update();
			}
		}
	}
	else event.Skip();
}
//------------------------------------------------------------------------------
void my1Form::OnBITPortClick(wxCommandEvent &event) {
	int cCheck = event.GetId() - MY1ID_CPOT_OFFSET;
	if (cCheck<0||cCheck>=MY1ID_CBIT_OFFSET) return;
	my1BitSelect cLink;
	cLink.mDevice = cCheck/PPI8255_PCNT;
	cLink.mDev = m8085.DevicePick(cLink.mDevice);
	if (!cLink.mDev) return;
	cLink.pick = cCheck%PPI8255_PCNT; // port index
	cLink.mDevicePort = cLink.pick;
	cLink.port = devd_port(cLink.mDev,cLink.pick);
	if (!cLink.port) return;
	cLink.addr = cLink.pick+cLink.mDev->addr.offs; // port address
	cLink.mDeviceAddr = cLink.mDev->addr.offs; // do  we still need this?
	wxWindowList& cList = mPortPanel->GetChildren();
	if ((int)cList.GetCount()<=0)  return;
	wxWindowList::Node *pNode = cList.GetFirst();
	for (int cLoop=DPORT_PCNT-1;cLoop>=0;cLoop--) {
		wxWindow *pTarget = 0x0;
		while (pNode) {
			pTarget = (wxWindow*) pNode->GetData();
			pNode = pNode->GetNext();
			if (pTarget->IsKindOf(wxCLASSINFO(my1BITCtrl)))
				break;
			pTarget = 0x0;
		}
		if (!pTarget) {
			this->PrintErrorMessage("Cannot Assign Port!");
			mPortPanel = 0x0;
			break;
		}
		my1BITCtrl *pCtrl = (my1BITCtrl*) pTarget;
		if (pCtrl->IsDummy()) continue;
		pCtrl->LinkBreak();
		cLink.pidx = cLoop; // bit index
		cLink.pbit = dport_pin(cLink.port,cLink.pidx);
		cLink.mDeviceBit = cLink.pidx;
		cLink.mPointer = (void*) cLink.pbit;
		// assign new link
		pCtrl->LinkCheck(cLink);
	}
}
//------------------------------------------------------------------------------
void my1Form::OnMiniMVChanging(wxGridEvent& event) {
	int irow, icol, cols;
	unsigned long conv, addr;
	wxString temp, init;
	wxGrid *pGrid = (wxGrid*) event.GetEventObject();
	irow = event.GetRow();
	icol = event.GetCol();
	temp = event.GetString();
#if 0
	printf("@@ %s->%s\n",pGrid->GetCellValue(irow,icol).ToAscii().data(),
		temp.ToAscii().data());
#endif
	if (temp.Length()==2&&temp.ToULong(&conv,16)&&conv<=0xFF) {
		init = pGrid->GetRowLabelValue(0);
		init.ToULong(&addr,16);
		cols = pGrid->GetNumberCols();
		addr += irow*cols+icol;
		if (!m8085.MemoryWrite(addr,conv))
			event.Veto();
#if 0
		else {
			printf("@@ YAY! {%d,%d}(%04x:%04x)\n",irow,icol,
				(unsigned int)addr,(unsigned int)conv);
		}
#endif
	}
	else event.Veto();
}
//------------------------------------------------------------------------------
void my1Form::AssignHandlers(void) {
	// actions & events! - (int, wxEventType, wxObjectEventFunction)
	this->Connect(wxEVT_CLOSE_WINDOW,wxCloseEventHandler(my1Form::OnFormClose));
	wxEventType cEventType = wxEVT_COMMAND_TOOL_CLICKED;
	this->Connect(MY1ID_EXIT,cEventType,WX_CEH(my1Form::OnQuit));
	this->Connect(MY1ID_LOAD,cEventType,WX_CEH(my1Form::OnLoad));
	this->Connect(MY1ID_SAVE,cEventType,WX_CEH(my1Form::OnSave));
	this->Connect(MY1ID_SAVEAS,cEventType,WX_CEH(my1Form::OnSave));
	this->Connect(MY1ID_NEW,cEventType,WX_CEH(my1Form::OnNew));
	this->Connect(MY1ID_ABOUT,cEventType,WX_CEH(my1Form::OnAbout));
	this->Connect(MY1ID_WHATSNEW,cEventType,WX_CEH(my1Form::OnWhatsNew));
	this->Connect(MY1ID_README,cEventType,WX_CEH(my1Form::OnReadMe));
	this->Connect(MY1ID_SYSTEM,cEventType,WX_CEH(my1Form::OnShowSystem));
	this->Connect(MY1ID_VIEW_SYSTPANE,cEventType,WX_CEH(my1Form::OnShowPanel));
	this->Connect(MY1ID_VIEW_REGSPANE,cEventType,WX_CEH(my1Form::OnShowPanel));
	this->Connect(MY1ID_VIEW_MEMSPANE,cEventType,WX_CEH(my1Form::OnShowPanel));
	this->Connect(MY1ID_VIEW_INTRPANE,cEventType,WX_CEH(my1Form::OnShowPanel));
	this->Connect(MY1ID_VIEW_CONSPANE,cEventType,WX_CEH(my1Form::OnShowPanel));
	this->Connect(MY1ID_VIEW_TERMPANE,cEventType,WX_CEH(my1Form::OnShowPanel));
	this->Connect(MY1ID_OPTIONS,cEventType,WX_CEH(my1Form::OnCheckOptions));
	this->Connect(MY1ID_ASSEMBLE,cEventType,WX_CEH(my1Form::OnAssemble));
	this->Connect(MY1ID_SIMULATE,cEventType,WX_CEH(my1Form::OnSimulate));
	this->Connect(MY1ID_GENERATE,cEventType,WX_CEH(my1Form::OnGenerate));
	this->Connect(MY1ID_CREATE_MINIMV,cEventType,WX_CEH(my1Form::OnShowPanel));
	this->Connect(MY1ID_CREATE_DV7SEG,cEventType,WX_CEH(my1Form::OnShowPanel));
	this->Connect(MY1ID_CREATE_DVKPAD,cEventType,WX_CEH(my1Form::OnShowPanel));
	this->Connect(MY1ID_CREATE_DEVLED,cEventType,WX_CEH(my1Form::OnShowPanel));
	this->Connect(MY1ID_CREATE_DEVSWI,cEventType,WX_CEH(my1Form::OnShowPanel));
	this->Connect(MY1ID_CREATE_DEVBUT,cEventType,WX_CEH(my1Form::OnShowPanel));
	this->Connect(MY1ID_CREATE_DEVLVD,cEventType,WX_CEH(my1Form::OnShowPanel));
	this->Connect(MY1ID_BUILDLOD,cEventType,WX_CEH(my1Form::OnSysLoad));
	this->Connect(MY1ID_BUILDSAV,cEventType,WX_CEH(my1Form::OnSysSave));
	cEventType = wxEVT_COMMAND_BUTTON_CLICKED;
	this->Connect(MY1ID_CONSEXEC,cEventType,WX_CEH(my1Form::OnExecuteConsole));
	this->Connect(MY1ID_SIMSEXEC,cEventType,WX_CEH(my1Form::OnSimulationPick));
	this->Connect(MY1ID_SIMSSTEP,cEventType,WX_CEH(my1Form::OnSimulationPick));
	this->Connect(MY1ID_SIMSINFO,cEventType,WX_CEH(my1Form::OnSimulationInfo));
	this->Connect(MY1ID_SIMSPREV,cEventType,WX_CEH(my1Form::OnSimulationInfo));
	this->Connect(MY1ID_SIMRESET,cEventType,WX_CEH(my1Form::OnSimulationInfo));
	this->Connect(MY1ID_SIMSMIMV,cEventType,WX_CEH(my1Form::OnSimulationInfo));
	this->Connect(MY1ID_SIMSBRKP,cEventType,WX_CEH(my1Form::OnSimulationInfo));
	this->Connect(MY1ID_SIMSEXIT,cEventType,WX_CEH(my1Form::OnSimulationInfo));
	this->Connect(MY1ID_BUILDRST,cEventType,WX_CEH(my1Form::OnBuildSelect));
	this->Connect(MY1ID_BUILDDEF,cEventType,WX_CEH(my1Form::OnBuildSelect));
	this->Connect(MY1ID_BUILDNFO,cEventType,WX_CEH(my1Form::OnBuildSelect));
	this->Connect(MY1ID_BUILDROM,cEventType,WX_CEH(my1Form::OnBuildSelect));
	this->Connect(MY1ID_BUILDRAM,cEventType,WX_CEH(my1Form::OnBuildSelect));
	this->Connect(MY1ID_BUILDPPI,cEventType,WX_CEH(my1Form::OnBuildSelect));
	cEventType = wxEVT_TIMER;
	this->Connect(MY1ID_STAT_TIMER,cEventType,WX_TEH(my1Form::OnStatusTimer));
	this->Connect(MY1ID_SIMX_TIMER,cEventType,WX_TEH(my1Form::OnSimExeTimer));
	// disable status bar showing helpstring
	this->Connect(wxID_ANY,wxEVT_MENU_HIGHLIGHT,
		wxMenuEventHandler(my1Form::OnMenuHighlight));
	// AUI-related events
	this->Connect(wxID_ANY,wxEVT_AUI_PANE_CLOSE,
		wxAuiManagerEventHandler(my1Form::OnClosePane));
	this->Connect(wxID_ANY,wxEVT_COMMAND_AUINOTEBOOK_PAGE_CHANGING,
		wxAuiNotebookEventHandler(my1Form::OnPageChanging));
	this->Connect(wxID_ANY,wxEVT_COMMAND_AUINOTEBOOK_PAGE_CHANGED,
		wxAuiNotebookEventHandler(my1Form::OnPageChanged));
	this->Connect(wxID_ANY,wxEVT_COMMAND_AUINOTEBOOK_PAGE_CLOSE,
		wxAuiNotebookEventHandler(my1Form::OnPageClosing));
	// setup hotkeys?
	wxAcceleratorEntry hotKeys[7];
	hotKeys[0].Set(wxACCEL_NORMAL, WXK_F8, MY1ID_SIMSEXEC);
	hotKeys[1].Set(wxACCEL_NORMAL, WXK_F7, MY1ID_SIMSSTEP);
	hotKeys[2].Set(wxACCEL_NORMAL, WXK_F6, MY1ID_SIMRESET);
	hotKeys[3].Set(wxACCEL_NORMAL, WXK_F5, MY1ID_SIMSEXIT);
	hotKeys[4].Set(wxACCEL_CTRL, WXK_F7, MY1ID_SIMULATE);
	hotKeys[5].Set(wxACCEL_CTRL, WXK_F6, MY1ID_GENERATE);
	hotKeys[6].Set(wxACCEL_CTRL, WXK_F5, MY1ID_ASSEMBLE);
	wxAcceleratorTable hkTable(7,hotKeys);
	this->SetAcceleratorTable(hkTable);
}
//------------------------------------------------------------------------------
