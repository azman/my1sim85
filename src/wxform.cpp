//------------------------------------------------------------------------------
// wxform.cpp
// - implementation for main wx-based form
//------------------------------------------------------------------------------
#define ABOUT_TITLE "8085 Microprocessor System Simulator"
#define ABOUT_COPYRIGHT "(C) 2011-2024 Azman M. Yusof"
#define ABOUT_WEBSITE "http://codeberg.org/azman/my1sim85/wiki"
#define ABOUT_AUTHOR MY1APP_AUTHOR
//------------------------------------------------------------------------------
#include "wxform.hpp"
//#include "wxpanel.hpp"
#include "wxpaneldev.hpp"
#include "wxcode.hpp"
#include "wxled.hpp"
#include "wxswitch.hpp"
#include "wx/gbsizer.h"
#include "wx/aboutdlg.h"
#include "wx/textfile.h"
#include "wx/wfstream.h"
#include "wx/fileconf.h"
#include "wx/stdpaths.h"
#include "wx/filefn.h"
#include "wx/dir.h"
#include "wx/display.h"
//------------------------------------------------------------------------------
#include "../res/apps.xpm"
#include "../res/exit.xpm"
#include "../res/newd.xpm"
#include "../res/open.xpm"
#include "../res/save.xpm"
#include "../res/cons.xpm"
#include "../res/term.xpm"
#include "../res/binary.xpm"
#include "../res/option.xpm"
#include "../res/build.xpm"
#include "../res/hexgen.xpm"
#include "../res/simx.xpm"
#include "../res/target.xpm"
#include "../res/devled.xpm"
#include "../res/devswi.xpm"
#include "../res/devbut.xpm"
#include "../res/dv7seg.xpm"
#include "../res/dvkpad.xpm"
//------------------------------------------------------------------------------
// handy alias
#define WX_CEH wxCommandEventHandler
#define WX_KEH wxKeyEventHandler
#define WX_MEH wxMouseEventHandler
#define WX_TEH wxTimerEventHandler
//------------------------------------------------------------------------------
#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define REGS_PANEL_WIDTH 180
#define REGS_HEADER_HEIGHT 30
#define CONS_PANEL_HEIGHT 150
#define INFO_REG_SPACER 5
#define SEG7_NUM_SPACER 5
#define DEVC_POP_SPACER 5
#define STATUS_COUNT 3
#define STATUS_FIX_WIDTH (REGS_PANEL_WIDTH+3)
#define STATUS_F2X_WIDTH 70
#define STATUS_SYS_INDEX 1
#define STATUS_MSG_INDEX 2
#define STATUS_MSG_PERIOD 3000
#define SIM_START_ADDR 0x0000
#define SIM_EXEC_PERIOD 1
#define TOOL_FILE_POS 0
#define TOOL_EDIT_POS 10
#define TOOL_VIEW_POS 20
#define TOOL_PROC_POS 30
#define TOOL_DEVC_POS 40
#define TITLE_FONT_SIZE 24
#define EMAIL_FONT_SIZE 8
#define PANEL_FONT_SIZE 10
#define INFO_FONT_SIZE 8
#define LOGS_FONT_SIZE 8
#define SIMS_FONT_SIZE 8
#define GRID_FONT_SIZE 8
#define CONS_FONT_SIZE 10
#define KPAD_FONT_SIZE 10
#define FLOAT_INIT_X 40
#define FLOAT_INIT_Y 40
#define MEM_VIEW_WIDTH 8
#define MEM_VIEW_HEIGHT (I8085_MAX_MSIZE/MEM_VIEW_WIDTH)
#define MEM_MINIVIEW_WIDTH 8
#define MEM_MINIVIEW_HEIGHT 4
#define DOT_SIZE 11
#define AUI_EXTER_LAYER 3
#define AUI_OUTER_LAYER 2
#define AUI_INNER_LAYER 1
#ifdef DO_MINGW
#define DEV_INIT_POS -1
#else
#define DEV_INIT_POS 0
#endif
#define BOT_CONS_POS 0
#define BOT_TERM_POS 1
//------------------------------------------------------------------------------
#define MSG_SYSTEM_IDLE wxS("Inactive")
#define MSG_SYSTEM_MSIM wxS("Idle")
#define MSG_SYSTEM_SSIM wxS("Stepping")
#define MSG_SYSTEM_RSIM wxS("Running")
//------------------------------------------------------------------------------
my1Form::my1Form(const wxString &title, const my1App* p_app)
		: wxFrame( NULL, MY1ID_MAIN, title, wxDefaultPosition,
		wxDefaultSize, wxDEFAULT_FRAME_STYLE), myApp((my1App*)p_app) {
	mShowSystem = false;
	// simulation stuffs
	mSimulationMode = false;
	mSimulationRunning = false;
	mSimulationStepping = true; // stepping by default
	m8085.SystemObject((void*)this);
	m8085.UpdateHandler((regstask_t)SimUpdateREG);
	// default option?
	mOptions.mChanged = false;
	mOptions.mEdit_ViewWS = false;
	mOptions.mEdit_ViewEOL = false;
	mOptions.mConv_UnixEOL = false;
	mOptions.mSims_ShowRunInfo = false;
	mOptions.mSims_PauseOnINTR = false;
	mOptions.mSims_PauseOnHALT = false;
	mOptions.mSims_StartADDR = SIM_START_ADDR;
	mOptions.mComp_DoList = false;
	// reset mini-viewers (link-list)
	mFirstViewer = 0x0;
	// minimum window size... duh!
	this->SetMinSize(wxSize(WIN_WIDTH,WIN_HEIGHT));
	// status bar
	this->CreateStatusBar(STATUS_COUNT);
	this->SetStatusText(wxS("Simulation System: "));
	this->SetStatusText(MSG_SYSTEM_IDLE,STATUS_SYS_INDEX);
	const int cWidths[STATUS_COUNT] = { STATUS_FIX_WIDTH,STATUS_F2X_WIDTH,-1 };
	wxStatusBar* cStatusBar = this->GetStatusBar();
	cStatusBar->SetStatusWidths(STATUS_COUNT,cWidths);
	// create timers
	mDisplayTimer = new wxTimer(this, MY1ID_STAT_TIMER);
	mSimExecTimer = new wxTimer(this, MY1ID_SIMX_TIMER);
	// console command history
	mCmdHistory.Clear();
	mCmdHistory.Alloc(CMD_HISTORY_COUNT+1);
	mCmdHistIndex = 0;
	// some handy pointers
	mConsole = 0x0;
	mCommand = 0x0;
	mComsPanel = 0x0;
	mTermCon = new my1Term(this,MY1ID_MAIN_TERM); // assume ok?
	mFileTool = 0x0;
	mEditTool = 0x0;
	mProcTool = 0x0;
	mDevicePopupMenu = 0x0;
	mDevicePortMenu = 0x0;
	mMemoryGrid = 0x0;
	mPortPanel = 0x0;
	mMainPanel = 0x0;
	// keeps pointers to dev panels
	mDevPanels.Clear();
	// setup image
	//wxInitAllImageHandlers();
	wxIcon mIconApps = MACRO_WXICO(apps);
	this->SetIcon(mIconApps);
	// main menu
	BuildMenu();
	// using AUI manager...
	mMainUI.SetManagedWindow(this);
	// create notebook for main/editor panel
	mNoteBook = new wxAuiNotebook(this, wxID_ANY,
		wxDefaultPosition, wxDefaultSize, wxAUI_NB_DEFAULT_STYLE);
	mNoteBook->AddPage(CreateInitPanel(mNoteBook), wxS("Welcome"), true);
	// create initial pane for main view
	mMainUI.AddPane(mNoteBook, wxAuiPaneInfo().Name(wxS("codeBook")).
		CenterPane().MaximizeButton(true).PaneBorder(false));
	// tool bars
	BuildToolBars();
	// main panels
	BuildMainPanels();
	UpdateMemoryPanel();
	// update build tool
	UpdateProcTool(false);
	// commit changes!
	mMainUI.Update();
	// need this here coz setting Show(false) screws Position
	wxAuiPaneInfo& cPaneDevC = mMainUI.GetPane(wxS("devcTool"));
	cPaneDevC.Show(mShowSystem);
	mMainUI.Update();
	// handlers
	AssignHandlers();
	// working directory
	SetupWorkPathConfig();
	// try to redirect standard console to gui console
	mRedirector = new wxStreamToTextRedirector(mConsole);
	// scroll console to last line
	while(mConsole->ScrollPages(1));
	// let command prompt has focus - now not visible by default
	//mCommand->SetFocus();
}
//------------------------------------------------------------------------------
my1Form::~my1Form() {
	// cleanup system
	this->SystemDisconnect();
	// cleanup aui
	mMainUI.UnInit();
	// cleanup mini-viewers (dual-link-list?)
	while (mFirstViewer) {
		my1MiniViewer *pViewer = mFirstViewer;
		mFirstViewer = pViewer->mNext;
		delete pViewer;
	}
	// cleanup redirector if neccessary
	if (mRedirector) { delete mRedirector; mRedirector = 0x0; }
	// just in case... it's just a link!
	if (mPortPanel) mPortPanel = 0x0;
	// save size & position?
	wxFileName cFullConf = wxFileName(mConfPath,wxS(MY1APP_PROGCONF));
	if (cFullConf.DirExists())
		this->SaveConfig(cFullConf.GetFullPath());
}
//------------------------------------------------------------------------------
void my1Form::SimulationMode(bool aGo) {
	this->GetMenuBar()->Enable(!aGo);
	mFileTool->Enable(!aGo);
	mProcTool->Enable(!aGo);
	mMainPanel->Enable(!aGo);
	wxAuiPaneInfo& cPaneSims = mMainUI.GetPane(wxS("simsPanel"));
	if (cPaneSims.IsOk()) cPaneSims.Show(aGo);
	if (aGo) this->SetStatusText(MSG_SYSTEM_MSIM,STATUS_SYS_INDEX);
	else this->SetStatusText(MSG_SYSTEM_IDLE,STATUS_SYS_INDEX);
	mMainUI.Update();
	mSimulationMode = aGo;
}
//------------------------------------------------------------------------------
bool my1Form::GetUniqueName(wxString& aName) {
	wxString cName;
	int cIndex = 0;
	while (cIndex<0x100) { // 256 max
		cName = aName + wxString::Format(wxS("%02X"),cIndex++);
		wxAuiPaneInfo& rPane = mMainUI.GetPane(cName);
		if (!rPane.IsOk()) {
			aName = cName;
			return true;
		}
	}
	return false;
}
//------------------------------------------------------------------------------
bool my1Form::LinkPanelToPort(wxPanel* aPanel,int anIndex) {
	// use existing method!
	wxCommandEvent cEvent(wxEVT_COMMAND_MENU_SELECTED,
		MY1ID_CPOT_OFFSET+anIndex);
	mPortPanel = aPanel;
	this->OnBITPortClick(cEvent);
	return mPortPanel ? true : false;
}
//------------------------------------------------------------------------------
void my1Form::OpenEdit(wxString& cFileName) {
	my1CodeEdit *cCodeEdit = new my1CodeEdit(mNoteBook,
		wxID_ANY, cFileName, this->mOptions);
	wxString cTempFile = cCodeEdit->GetFileName();
	if (!cTempFile.Length())
		cTempFile = wxS("unnamed");
	cCodeEdit->Connect(cCodeEdit->GetId(),wxEVT_KEY_DOWN,
		WX_KEH(my1Form::OnCheckFont),NULL,this);
	mNoteBook->AddPage(cCodeEdit, cTempFile,true);
	if (mOptions.mConv_UnixEOL)
		cCodeEdit->ConvertEOLs(2);
	wxString cStatus = wxS("File ") +
		cCodeEdit->GetFileName() + wxS(" loaded!");
	this->ShowStatus(cStatus);
}
//------------------------------------------------------------------------------
void my1Form::SaveEdit(wxWindow* cEditPane, bool aSaveAs) {
	wxString cFileName;
	my1CodeEdit *cEditor = (my1CodeEdit*) cEditPane;
	if (aSaveAs||!cEditor->GetFileName().Length()) {
		wxFileName cThisPath(mThisPath,"");
		cThisPath.AppendDir(wxS("asm"));
		wxFileDialog *cSelect = new wxFileDialog(this,wxS("Assign File Name"),
			wxS(""),wxS(""),wxS("Any file (*.*)|*.*"),
			wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
		cSelect->SetWildcard("ASM files (*.asm)|*.asm|Any file (*.*)|*.*");
		cSelect->SetDirectory(cThisPath.GetPath());
		if (cSelect->ShowModal()!=wxID_OK) return;
		cFileName = cSelect->GetPath();
		if (cSelect->GetFilterIndex()==0) {
			if(cFileName.Right(4)!=wxS(".asm"))
				cFileName += wxS(".asm");
		}
	}
	cEditor->SaveEdit(cFileName);
	wxString cStatus = wxS("File ") + cEditor->GetFileName() + wxS(" saved!");
	this->ShowStatus(cStatus);
}
//------------------------------------------------------------------------------
void my1Form::ShowStatus(wxString& aString) {
	this->SetStatusText(aString,STATUS_MSG_INDEX);
	mDisplayTimer->Start(STATUS_MSG_PERIOD,wxTIMER_ONE_SHOT);
}
//------------------------------------------------------------------------------
void my1Form::SetupWorkPathConfig(void) {
	// get program path/name
	wxStandardPaths& cPaths = wxStandardPaths::Get();
	wxFileName cFullName(cPaths.GetExecutablePath());
	// do this also to get config path
	wxFileName cFullConf(cPaths.GetUserLocalDataDir());
	wxString cName, cPath;
	do {
		cName = cFullConf.GetName();
		if (cName[0]=='.') {
			cPath = cFullConf.GetPathWithSep();
			// unix system - add in .local in path and remove 'dot'
			cFullConf = wxFileName(cPath,".local");
			cPath = cFullConf.GetFullPath();
			cName = cName.Mid(1);
			cFullConf = wxFileName(cPath,cName);
		}
		cFullConf = wxFileName(cFullConf.GetFullPath(),".");
		cPath = cFullConf.GetPath();
		// create if not existing
		if (!cFullConf.DirExists()) {
			if (!cFullConf.Mkdir(wxS_DIR_DEFAULT,wxPATH_MKDIR_FULL))
				std::cout << "** CreateFailed:{" << cPath << "}\n";
		}
		mConfPath = cPath;
		//std::cout << "@@ ConfPath:{" << mConfPath << "}\n";
		// find a suitable work path
		wxString cTempPath;
		// check current binary path
		cTempPath = cFullName.GetPathWithSep();
		wxDir cLook(cTempPath);
		if (cLook.IsOpened()) {
			if (cLook.HasFiles(wxString("README"))&&
					cLook.HasFiles(wxString("CHANGELOG"))) {
				// we are in 'build' path, use this!
				mThisPath = cTempPath;
			}
			cLook.Close();
		}
		// we are running in build/install path!
		if (mThisPath.Len()) break;
		// find other suitable path
		if (!cFullConf.DirExists()) {
			//std::cout << "** Missing path:{" << mConfPath << "}\n";
			cFullName.AssignHomeDir();
			mThisPath = cFullName.GetFullPath();
		}
		else mThisPath = mConfPath;
		// command line override
		for (int loop=0;loop<this->myApp->argc;loop++) {
			wxString ptest = wxString(this->myApp->argv[loop]);
			if (ptest==wxS("--thispath")) {
				mThisPath = wxGetCwd();
				break;
			}
		}
		// default is still path to this binary
		if (!mThisPath.Len()) mThisPath = cTempPath;
	} while (0);
	//std::cout << "@@ DEBUGX:{" << mThisPath << "}\n";
	wxSetWorkingDirectory(mThisPath);
	// position this!
	cFullConf = wxFileName(mConfPath,wxS(MY1APP_PROGCONF));
	cName = cFullConf.GetFullPath();
	if (!this->LoadConfig(cName)) {
		wxDisplay curr(this);
		wxRect temp = curr.GetClientArea();
		this->SetSize(temp.width*3/5,temp.height*9/10);
		this->Centre();
	}
	mTermCon->ConfigFile(cName);
	mTermCon->Editable(false);
	mTermCon->LoadConfig();
}
//------------------------------------------------------------------------------
void my1Form::BuildMenu(void) {
	// menu bar
	wxMenu *fileMenu = new wxMenu;
	fileMenu->Append(MY1ID_NEW, wxS("&New\tCTRL+N"));
	fileMenu->Append(MY1ID_LOAD, wxS("&Open\tCTRL+O"));
	fileMenu->Append(MY1ID_SAVE, wxS("&Save\tCTRL+S"));
	fileMenu->Append(MY1ID_SAVEAS, wxS("Save &As..."));
	fileMenu->AppendSeparator();
	fileMenu->Append(MY1ID_EXIT, wxS("E&xit\tCTRL+Q"), wxS("Quit program"));
	wxMenu *editMenu = new wxMenu;
	editMenu->Append(MY1ID_SYSTEM, wxS("&Build System"),
		wxEmptyString, wxITEM_CHECK);
	editMenu->AppendSeparator();
	editMenu->Append(MY1ID_VIEW_CONSPANE, wxS("Show Console Panel"));
	editMenu->Append(MY1ID_VIEW_TERMPANE, wxS("Show Terminal Panel"));
	editMenu->AppendSeparator();
	editMenu->Append(MY1ID_OPTIONS, wxS("&Preferences..."));
	wxMenu *systMenu = new wxMenu;
	systMenu->Append(MY1ID_BUILDLOD, wxS("&Load System..."));
	systMenu->Append(MY1ID_BUILDSAV, wxS("&Save System..."));
	systMenu->AppendSeparator();
	systMenu->Append(MY1ID_CREATE_MINIMV, wxS("Create miniMV Panel"));
	systMenu->AppendSeparator();
	systMenu->Append(MY1ID_CREATE_DV7SEG, wxS("Create dv7SEG Panel"));
	systMenu->Append(MY1ID_CREATE_DVKPAD, wxS("Create dvKPAD Panel"));
	systMenu->Append(MY1ID_CREATE_DEVLED, wxS("Create devLED Panel"));
	systMenu->Append(MY1ID_CREATE_DEVSWI, wxS("Create devSWI Panel"));
	systMenu->Append(MY1ID_CREATE_DEVBUT, wxS("Create devBUT Panel"));
	systMenu->Append(MY1ID_CREATE_DEVLVD, wxS("Create devLED Panel (V)"));
	systMenu->AppendSeparator();
	systMenu->Append(MY1ID_VIEW_SYSTPANE, wxS("Show System Panel"));
	systMenu->Append(MY1ID_VIEW_REGSPANE, wxS("Show Register Panel"));
	systMenu->Append(MY1ID_VIEW_MEMSPANE, wxS("Show Memory Panel"));
	systMenu->Append(MY1ID_VIEW_INTRPANE, wxS("Show Interrupt Panel"));
	wxMenu *procMenu = new wxMenu;
	procMenu->Append(MY1ID_ASSEMBLE, wxS("&Assemble"));
	procMenu->Append(MY1ID_GENERATE, wxS("&Generate"));
	procMenu->Append(MY1ID_SIMULATE, wxS("&Simulate"));
	wxMenu *helpMenu = new wxMenu;
	helpMenu->Append(MY1ID_README, wxS("&ReadMe"), wxS("Some Information"));
	helpMenu->Append(MY1ID_WHATSNEW, wxS("&ChangeLog"), wxS("What's New?"));
	helpMenu->AppendSeparator();
	helpMenu->Append(MY1ID_ABOUT, wxS("&About"), wxS("About This Program"));
	wxMenuBar *mainMenu = new wxMenuBar;
	mainMenu->Append(fileMenu, wxS("&File"));
	mainMenu->Append(editMenu, wxS("&Edit"));
	mainMenu->Append(procMenu, wxS("&Tool"));
	mainMenu->Append(systMenu, wxS("&System"));
	mainMenu->Append(helpMenu, wxS("&Help"));
	this->SetMenuBar(mainMenu);
	UpdateMenuState(false);
}
//------------------------------------------------------------------------------
void my1Form::UpdateMenuState(bool aEdit) {
	wxMenuBar *mainMenu;
	wxMenuItem *menuItem;
	mainMenu = this->GetMenuBar();
	mainMenu->EnableTop(mainMenu->FindMenu(wxS("Tool")),aEdit);
	mainMenu->EnableTop(mainMenu->FindMenu(wxS("System")),mShowSystem);
	menuItem = mainMenu->FindItem(MY1ID_VIEW_SYSTPANE,0x0);
	if (menuItem) menuItem->Enable(mShowSystem);
	menuItem = mainMenu->FindItem(MY1ID_VIEW_REGSPANE,0x0);
	if (menuItem) menuItem->Enable(mShowSystem);
	menuItem = mainMenu->FindItem(MY1ID_VIEW_MEMSPANE,0x0);
	if (menuItem) menuItem->Enable(mShowSystem);
	menuItem = mainMenu->FindItem(MY1ID_VIEW_INTRPANE,0x0);
	if (menuItem) menuItem->Enable(mShowSystem);
	menuItem = mainMenu->FindItem(MY1ID_SIMULATE,0x0);
	if (menuItem) menuItem->Enable(mShowSystem);
	menuItem = mainMenu->FindItem(MY1ID_SYSTEM,0x0);
	if (menuItem) menuItem->Check(mShowSystem);
}
//------------------------------------------------------------------------------
int my1Form::GetBuildAddress(const wxString& aString) {
	wxTextEntryDialog* cDialog = new wxTextEntryDialog(this,
		wxS("Enter Address in HEX"), aString);
	if (cDialog->ShowModal()!=wxID_OK) return -1;
	unsigned long cStart = 0x0;
	cDialog->GetValue().ToULong(&cStart,16);
	return cStart;
}
//------------------------------------------------------------------------------
my1dpin_t* my1Form::GetDeviceBit(my1BitSelect& aSelect, bool useAddress) {
	my1dpin_t *pBit = 0x0;
	my1list_t* list;
	// check if interrupt pin
	if (aSelect.mDevice<0) {
		pBit = m8085.Pin(aSelect.mDeviceBit);
		if (pBit) { dpin_unlink(pBit); }
		aSelect.mPointer = (void*) pBit;
		return pBit;
	}
	int cExtra;
	my1device_t *pDev = 0x0;
	if (useAddress)
		pDev = m8085.Device(aSelect.mDeviceAddr,&cExtra);
	else {
		list = m8085.DeviceList();
		list_scan_prep(list);
		while (list_scan_item(list)) {
			if (list->step==aSelect.mDevice) {
				pDev = (my1device_t*)list->curr->data;
				break;
			}
		}
	}
	if (pDev) {
		my1dport_t *pPort = devd_port(pDev,aSelect.mDevicePort);
		if (pPort) pBit = dport_pin(pPort,aSelect.mDeviceBit);
		if (pBit) {
			aSelect.mPointer = (void*) pBit;
			if (useAddress) aSelect.mDevice = cExtra;
			else aSelect.mDeviceAddr = pDev->addr.offs;
		}
	}
	else wxMessageBox(wxString::Format("No device? (%02x)",cExtra),
			wxS("[DEBUG]"),wxOK|wxICON_INFORMATION);
	return pBit;
}
//------------------------------------------------------------------------------
void my1Form::UpdateDeviceBit(bool unLink) {
	my1list_t* list;
	my1device_t *pDev, *tDev;
	list = m8085.DeviceList();
	list_scan_prep(list);
	while (list_scan_item(list)) {
		pDev = (my1device_t*)list->curr->data;
		for (int cPort=0;cPort<PPI8255_PCNT;cPort++) {
			my1dport_t *pPort = devd_port(pDev,cPort);
			for (int cLoop=0;cLoop<DPORT_PCNT;cLoop++) {
				my1dpin_t *pBit = dport_pin(pPort,cLoop);
				if (!pBit) continue;
				my1BITCtrl *pCtrl = (my1BITCtrl*) pBit->pobj;
				if (pCtrl) {
					int cIndex;
					my1BitSelect& cLink = pCtrl->Link();
					word32_t cAddress = cLink.mDeviceAddr;
					tDev = m8085.Device(cAddress,&cIndex);
					if (!unLink&&tDev==pDev) {
						cLink.mDevice = cIndex;
					}
					else pCtrl->LinkBreak();
				}
				else if (unLink) { dpin_unlink(pBit); }
			}
		}
	}
}
//------------------------------------------------------------------------------
wxMenu* my1Form::GetDevicePopupMenu(void) {
	my1device_t* pdev;
	my1list_t* list;
	list = m8085.DeviceList();
	if (!list->size) {
		wxMessageBox(wxS("Build a system with PPI!"),
				wxS("System Incomplete!"),wxOK|wxICON_EXCLAMATION,this);
		return 0x0;
	}
	if (!mDevicePopupMenu) {
		mDevicePopupMenu = new wxMenu;
		int cDevID = MY1ID_DSEL_OFFSET;
		int cBitID = MY1ID_CBIT_OFFSET;
		list_scan_prep(list);
		while (list_scan_item(list)) {
			pdev = (my1device_t*)list->curr->data;
			wxMenu *cMenuBit = new wxMenu;
			for (int cPort=0;cPort<PPI8255_PCNT;cPort++) {
				wxString cPortText = wxS("P") +
					wxString::Format(wxS("%c"),(char)(cPort+(int)'A'));
				for (int cLoop=0;cLoop<DPORT_PCNT;cLoop++) {
					wxString cText = cPortText +
						wxString::Format(wxS("%01X"),cLoop);
					cMenuBit->Append(cBitID++,cText,
						wxEmptyString,wxITEM_CHECK);
				}
			}
			wxString cText = wxS("Device @") +
				wxString::Format(wxS("%02X"),pdev->addr.offs);
			mDevicePopupMenu->Append(cDevID++, cText, cMenuBit);
		}
		// add 8085 interrupt pins!
		{
			int cIntID = MY1ID_8085_OFFSET;
			wxMenu *cMenuBit = new wxMenu;
			wxString cText = wxS("INT: TRAP");
			cMenuBit->Append(cIntID++,cText,wxEmptyString,wxITEM_CHECK);
			cText = wxS("INT: I7.5");
			cMenuBit->Append(cIntID++,cText,wxEmptyString,wxITEM_CHECK);
			cText = wxS("INT: I6.5");
			cMenuBit->Append(cIntID++,cText,wxEmptyString,wxITEM_CHECK);
			cText = wxS("INT: I5.5");
			cMenuBit->Append(cIntID++,cText,wxEmptyString,wxITEM_CHECK);
			cText = wxS("Interrupt Pins");
			mDevicePopupMenu->AppendSeparator();
			mDevicePopupMenu->Append(cDevID++, cText, cMenuBit);
		}
		// add other options
		{
			mDevicePopupMenu->AppendSeparator();
			mDevicePopupMenu->Append(MY1ID_CHANGE_LABEL,
				wxS("Change Label"));
			mDevicePopupMenu->AppendSeparator();
			mDevicePopupMenu->AppendCheckItem(MY1ID_TOGGLE_ACTLVL,
				wxS("Active Low"));
		}
	}
	// make sure all items are unchecked? minus separator and interrupt!
	int cCountD = mDevicePopupMenu->GetMenuItemCount();
	for (int cLoopD=0;cLoopD<cCountD;cLoopD++) {
		wxMenuItem *cItemD = mDevicePopupMenu->FindItemByPosition(cLoopD);
		if (cItemD->IsSeparator()) break;
		wxMenu *cMenuD = cItemD->GetSubMenu();
		int cCountB = cMenuD->GetMenuItemCount();
		for (int cLoopB=0;cLoopB<cCountB;cLoopB++) {
			wxMenuItem *cItem = cMenuD->FindItemByPosition(cLoopB);
			cItem->Check(false);
			int cCheck = cItem->GetId() - MY1ID_CBIT_OFFSET;
			my1BitSelect cSelect;
			cSelect.UseIndex(cCheck);
			my1dpin_t* pBit = this->GetDeviceBit(cSelect);
			if (dpin_linked(pBit)) cItem->Enable(false);
		}
	}
	// interrupt as well?
	{
		int cIntID = MY1ID_8085_OFFSET;
		for (int cLoop=0;cLoop<4;cLoop++,cIntID++) {
			wxMenuItem *cItem = mDevicePopupMenu->FindItem(cIntID);
			cItem->Check(false);
			int cCheck = cIntID - MY1ID_8085_OFFSET;
			my1dpin_t* pBit = m8085.Pin(cCheck);
			if (dpin_linked(pBit)) cItem->Enable(false);
		}
	}
	return mDevicePopupMenu;
}
//------------------------------------------------------------------------------
void my1Form::ResetDevicePopupMenu(bool unLink) {
	this->UpdateDeviceBit(unLink);
	if (mDevicePopupMenu) {
		delete mDevicePopupMenu;
		mDevicePopupMenu = 0x0;
	}
}
//------------------------------------------------------------------------------
wxMenu* my1Form::GetDevicePortMenu(void) {
	my1device_t *pDev;
	my1dpin_t *pbit;
	if (mDevicePortMenu) {
		delete mDevicePortMenu;
		mDevicePortMenu = 0x0;
	}
	my1list_t* list = m8085.DeviceList();
	if (!list->size) {
		wxMessageBox(wxS("Build a system with PPI!"),
				wxS("System Incomplete!"),wxOK|wxICON_EXCLAMATION,this);
		return 0x0;
	}
	int cPortID = MY1ID_CPOT_OFFSET;
	mDevicePortMenu = new wxMenu;
	list_scan_prep(list);
	while (list_scan_item(list)) {
		pDev = (my1device_t*) list->curr->data;
		for (int cPort=0;cPort<PPI8255_PCNT;cPort++) {
			wxString cPortText = wxS("P") +
				wxString::Format(wxS("%c"),(char)(cPort+(int)'A')) +
				wxString::Format(wxS(" @%02X"),pDev->addr.offs+cPort);
			wxMenuItem* cItem = mDevicePortMenu->Append(cPortID++,cPortText);
			for (int cLoop=0;cLoop<DPORT_PCNT;cLoop++) {
				pbit = devd_dpin(pDev,cPort,cLoop);
				if (dpin_linked(pbit)) {
					cItem->Enable(false);
					break;
				}
			}
		}
	}
	return mDevicePortMenu;
}
//------------------------------------------------------------------------------
void my1Form::UpdateMemoryPanel(void) {
	if (!mMemoryGrid) return;
	wxGrid *pGrid = mMemoryGrid;
	my1list_t* list;
	my1memory_t *pMem;
	list = m8085.MemoryList();
	list_scan_prep(list);
	while (list_scan_item(list)) {
		pMem = (my1memory_t*)list->curr->data;
		if (pMem->core!=(void*)pGrid) {
			pMem->core = (void*)pGrid;
			pMem->doupdate = &this->SimUpdateMEM;
		}
		int cAddress = pMem->addr.offs;
		int vWidth = pGrid->GetNumberCols();
		for (int cLoop=0;cLoop<(int)pMem->addr.size;cLoop++) {
			int cCol = cAddress%vWidth;
			int cRow = cAddress/vWidth;
			pGrid->SetCellValue(cRow,cCol,
				wxString::Format(wxS("%02X"),pMem->data[cLoop]));
			cAddress++;
		}
	}
}
//------------------------------------------------------------------------------
void my1Form::SimUpdateFLAG(void) {
	// update flag if necessary?
	my1reg85_t *preg = m8085.Regs();
	int flag = reg85_flag(preg);
	wxString cFlag = wxString::Format(wxS("%01X"),flag&I8085_FLAG_C?1:0);
	mFlagLink[MY1SIMUI_FLAG_C].Panel()->SetText(cFlag);
	cFlag = wxString::Format(wxS("%01X"),flag&I8085_FLAG_P?1:0);
	mFlagLink[MY1SIMUI_FLAG_P].Panel()->SetText(cFlag);
	cFlag = wxString::Format(wxS("%01X"),flag&I8085_FLAG_A?1:0);
	mFlagLink[MY1SIMUI_FLAG_A].Panel()->SetText(cFlag);
	cFlag = wxString::Format(wxS("%01X"),flag&I8085_FLAG_Z?1:0);
	mFlagLink[MY1SIMUI_FLAG_Z].Panel()->SetText(cFlag);
	cFlag = wxString::Format(wxS("%01X"),flag&I8085_FLAG_S?1:0);
	mFlagLink[MY1SIMUI_FLAG_S].Panel()->SetText(cFlag);
}
//------------------------------------------------------------------------------
bool my1Form::RemoveControls(void) {
	bool cFlag = true;
	wxWindowList::Node *pNode = mDevPanels.GetFirst();
	while (pNode) {
		wxWindow *pTarget = (wxWindow*) pNode->GetData();
		pNode = pNode->GetNext();
		if (mMainUI.DetachPane(pTarget)) {
			this->PrintInfoMessage("Deleted a Panel!");
			mDevPanels.DeleteContents(true);
			mDevPanels.DeleteObject(pTarget);
			mDevPanels.DeleteContents(false);
			mMainUI.Update();
		}
		//else  // shouldn't happen
		//{
		//	cFlag = false;
		//	wxMessageBox(wxString::Format("WinID: '%d'",pTarget->GetId()),
		//		wxS("[CANNOT DETACH PANE!]"),wxOK|wxICON_WARNING);
		//}
	}
	return cFlag;
}
//------------------------------------------------------------------------------
bool my1Form::SystemDefault(void) {
	bool cFlag = true;
	cFlag &= this->SystemDisconnect();
	cFlag &= this->ConnectROM();
	cFlag &= this->ConnectRAM();
	cFlag &= this->ConnectPPI();
	if (cFlag) {
		this->PrintInfoMessage("Default system built!");
//printf("@@ DEBUG1\n"); fflush(stdout);
		// default switch panel
		my1DEVPanel*  pSWIPanel = this->CreateDeviceSWIPanel();
		wxAuiPaneInfo& cPaneSWI = mMainUI.GetPane(pSWIPanel);
		cPaneSWI.Caption("PortB @80");
		mMainUI.Update();
		if (!this->LinkPanelToPort(pSWIPanel,1)) // PB
			this->PrintErrorMessage("Cannot link switch panel!");
		// default led panel
		my1DEVPanel* pLEDPanel = this->CreateDeviceLEDPanel();
		wxAuiPaneInfo& cPaneLED = mMainUI.GetPane(pLEDPanel);
		cPaneLED.Caption("PortA @80");
		mMainUI.Update();
		if (!this->LinkPanelToPort(pLEDPanel,0)) // PA
			this->PrintErrorMessage("Cannot link LED panel!");
		// update main memory display
		this->UpdateMemoryPanel();
	}
	else this->PrintErrorMessage("Default system build FAILED!");
	return cFlag;
}
//------------------------------------------------------------------------------
bool my1Form::SystemDisconnect(void) {
	this->ResetDevicePopupMenu(true);
	bool cFlag = this->RemoveControls();
	cFlag &= m8085.DisconnectALL();
	// clear memory panel
	if (mMemoryGrid) {
		for (int cRow=0;cRow<mMemoryGrid->GetNumberRows();cRow++) {
			for (int cCol=0;cCol<mMemoryGrid->GetNumberCols();cCol++) {
				mMemoryGrid->SetCellValue(cRow,cCol,wxS("--"));
			}
		}
	}
	if (cFlag) this->PrintInfoMessage("System build reset!");
	else this->PrintErrorMessage("System build reset FAILED!");
	//mNoteBook->SetSelection(0);
	return cFlag;
}
//------------------------------------------------------------------------------
bool my1Form::ConnectROM(int aStart) {
	bool cFlag = false;
	if (aStart<0) return cFlag;
	if (aStart%ROM2764_SIZE!=0) {
		wxString cTest = wxS("2764 ROM start address should be");
		cTest += wxString::Format(wxS("multiple of 0x%04X!"),ROM2764_SIZE);
		wxMessageBox(cTest,wxS("Anomaly Detected!"),
			wxOK|wxICON_EXCLAMATION,this);
		return false;
	}
	wxString cTag = wxString::Format(wxS("@[%04X]!"),aStart);
	if ((cFlag=m8085.ConnectROM(aStart))) {
		this->PrintInfoMessage(wxS("2764 ROM added ")+cTag);
		this->UpdateMemoryPanel();
	}
	else this->PrintErrorMessage(wxS("Cannot add 2764 ROM ")+cTag);
	return cFlag;
}
//------------------------------------------------------------------------------
bool my1Form::ConnectRAM(int aStart) {
	bool cFlag = false;
	if (aStart<0) return cFlag;
	if (aStart%RAM6264_SIZE!=0) {
		wxString cTest = wxS("6264 RAM start address should be");
		cTest += wxString::Format(wxS("multiple of 0x%04X!"),RAM6264_SIZE);
		wxMessageBox(cTest,wxS("Anomaly Detected!"),
			wxOK|wxICON_EXCLAMATION,this);
		return false;
	}
	wxString cTag = wxString::Format(wxS("@[%04X]!"),aStart);
	if ((cFlag=m8085.ConnectRAM(aStart))) {
		this->PrintInfoMessage(wxS("6264 RAM added ")+cTag);
		this->UpdateMemoryPanel();
	}
	else this->PrintErrorMessage(wxS("Cannot add 6264 RAM ")+cTag);
	return cFlag;
}
//------------------------------------------------------------------------------
bool my1Form::ConnectPPI(int aStart) {
	bool cFlag = false;
	if (aStart<0) return cFlag;
	if (aStart%PPI8255_SIZE!=0) {
		wxString cTest = wxS("8255 PPI start address should be");
		cTest += wxString::Format(wxS("multiple of 0x%02X!"),PPI8255_SIZE);
		wxMessageBox(cTest,wxS("Anomaly Detected!"),
			wxOK|wxICON_EXCLAMATION,this);
		return false;
	}
	wxString cTag = wxString::Format(wxS("@[%02X]!"),aStart);
	if ((cFlag=m8085.ConnectPPI(aStart))) {
		this->PrintInfoMessage(wxS("8255 PPI added ")+cTag);
		this->ResetDevicePopupMenu();
	}
	else this->PrintErrorMessage(wxS("Cannot add 8255 PPI ")+cTag);
	return cFlag;
}
//------------------------------------------------------------------------------
bool my1Form::LoadSystem(const wxString& aFilename) {
	bool cFlag = true;
	wxFileInputStream cRead(aFilename);
	wxFileConfig cSystem(cRead);
	wxString cVal, cKey=wxS("/System/my1sim85key");
	if (!cSystem.Read(cKey,&cVal)||cVal!=wxS("my1sim85chk")) {
		wxMessageBox(wxString::Format("File: '%s'",aFilename),
			wxS("[SYSTEM LOAD ERROR]"),wxOK|wxICON_ERROR);
		return false;
	}
	this->PrintInfoMessage(wxS("\nRemoving current system..."));
	// rebuild system here!
	this->SystemDisconnect();
	this->PrintInfoMessage(wxS("\nLoading new system..."));
	// look for memory instances
	long cValue, cCount;
	cKey = wxS("/System/CountM");
	cCount = cSystem.ReadLong(cKey,0);
	this->PrintInfoMessage(wxString::Format("Memory Count: '%ld'",cCount));
	for (int cLoop=0;cLoop<cCount;cLoop++) {
		cSystem.SetPath(wxString::Format(wxS("/Memory%d"),cLoop));
		cKey = wxS("Info");
		cFlag &= cSystem.Read(cKey,&cVal);
		cVal = cVal.Mid(cVal.Find(wxS("Start:")));
		cVal = cVal.AfterFirst(':');
		cVal = cVal.BeforeFirst(';');
		cVal.ToLong(&cValue,16);
		cKey = wxS("Type");
		cFlag &= cSystem.Read(cKey,&cVal);
		if (cVal==wxS("RAM")) this->ConnectRAM(cValue);
		else this->ConnectROM(cValue);
		cSystem.SetPath(wxS("/")); // just in case
	}
	// look for device instances
	cKey = wxS("/System/CountD");
	cCount = cSystem.ReadLong(cKey,0);
	this->PrintInfoMessage(wxString::Format("Device Count: '%ld'",cCount));
	for (int cLoop=0;cLoop<cCount;cLoop++) {
		cSystem.SetPath(wxString::Format(wxS("/Device%d"),cLoop));
		cKey = wxS("Type");
		cFlag &= cSystem.Read(cKey,&cVal);
		cKey = wxS("Info");
		cFlag &= cSystem.Read(cKey,&cVal);
		cVal = cVal.Mid(cVal.Find(wxS("Start:")));
		cVal = cVal.AfterFirst(':');
		cVal = cVal.BeforeFirst(';');
		cVal.ToLong(&cValue,16);
		this->ConnectPPI(cValue);
		cSystem.SetPath(wxS("/")); // just in case
	}
	// look for control instances
	cKey = wxS("/System/CountC");
	cCount = cSystem.ReadLong(cKey,0);
	this->PrintInfoMessage(wxString::Format("PanelD Count: '%ld'",cCount));
	for (int cLoop=0;cLoop<cCount;cLoop++) {
		wxString cName; bool cTest;
		my1DEVPanel* pPanel = 0x0;
		cSystem.SetPath(wxString::Format(wxS("/Control%d"),cLoop));
		cKey = wxS("Type");
		cFlag &= cSystem.Read(cKey,&cVal);
		cKey = wxS("Name");
		cFlag &= cSystem.Read(cKey,&cName);
		cKey = wxS("Flag");
		cFlag &= cSystem.Read(cKey,&cTest);
		// create the panel
		if (cVal==wxS("7SEG"))
			pPanel = (my1DEVPanel*) this->CreateDevice7SegPanel(cName);
		else if (cVal==wxS("KPAD"))
			pPanel = (my1DEVPanel*) this->CreateDeviceKPadPanel(cName);
		else if (cVal==wxS("LED"))
			pPanel = (my1DEVPanel*) this->CreateDeviceLEDPanel(cName,cTest);
		else if (cVal==wxS("SWI"))
			pPanel = (my1DEVPanel*) this->CreateDeviceSWIPanel(cName);
		else if (cVal==wxS("BUT"))
			pPanel = (my1DEVPanel*) this->CreateDeviceBUTPanel(cName);
		if (!pPanel) continue; // very unlikely!
		// link the bits
		int cBitCount = 0;
		wxWindowList& cBitList = pPanel->GetChildren();
		wxWindowList::Node *pBitNode = cBitList.GetFirst();
		while (pBitNode) {
			wxWindow *pBitCheck = (wxWindow*) pBitNode->GetData();
			if (pBitCheck->IsKindOf(wxCLASSINFO(my1BITCtrl))) {
				my1BITCtrl *pCtrl = (my1BITCtrl*) pBitCheck;
				cKey = wxString::Format("Bit%d",cBitCount++);
				cFlag &= cSystem.Read(cKey,&cVal);
				wxString cChk = cVal;
				// start convert to num
				cFlag &= cChk.BeforeFirst(':').ToLong(&cValue);
				pCtrl->Link().mDevice = cValue;
				cChk = cChk.AfterFirst(':');
				cFlag &= cChk.BeforeFirst(':').ToLong(&cValue);
				pCtrl->Link().mDevicePort = cValue;
				cChk = cChk.AfterFirst(':');
				cFlag &= cChk.BeforeFirst(':').ToLong(&cValue);
				pCtrl->Link().mDeviceBit = cValue;
				cChk = cChk.AfterFirst(':');
				cFlag &= cChk.BeforeFirst(':').ToLong(&cValue);
				pCtrl->Link().mDeviceAddr = cValue;
				cChk = cChk.AfterFirst(':');
				// use default active level if not specified (empty)
				if (!cChk.IsEmpty()) {
					long cActive;
					cFlag &= cChk.ToLong(&cActive);
					// active level!
					pCtrl->ActiveLevel(cActive);
				}
				// make the link!
				if (!pCtrl->IsDummy()&&(cValue>=0||pCtrl->Link().mDevice<0)) {
					my1dpin_t* pBit = this->GetDeviceBit(pCtrl->Link(),true);
					if (pBit) { // should get the device!
						dpin_unlink(pBit); // just in case?
						pCtrl->LinkThis((my1dpin_t*)pCtrl->Link().mPointer);
					}
				}
			}
			pBitNode = pBitNode->GetNext();
		}
		cSystem.SetPath(wxS("/")); // just in case
	}
	// load saved layout
	cSystem.SetPath(wxS("/System"));
	{
		wxString cVal, cKey = wxS("Layout");
		cFlag &= cSystem.Read(cKey,&cVal);
		cVal.Replace(wxS(":"),wxS("="));
		cVal.Replace(wxS("__"),wxS(" "));
		mMainUI.LoadPerspective(cVal);
		// check procTool status?
		wxAuiPaneInfo& cPaneProc = mMainUI.GetPane(wxS("procTool"));
		if (cPaneProc.IsOk()) {
			wxWindow *cTarget = mNoteBook->GetCurrentPage();
			if (cTarget&&cTarget->IsKindOf(wxCLASSINFO(my1CodeEdit)))
				cPaneProc.Show(true);
			else cPaneProc.Show(false);
			mMainUI.Update();
		}
		cSystem.SetPath(wxS("/"));
	}
	// DEBUG
	//this->PrintInfoMessage(wxString::Format("Layout: '%s'",cVal));
	//this->PrintInfoMessage(wxString::Format("Flag: '%s'",cFlag?"true":"false"));
	if (cFlag) {
		wxMessageBox(wxString::Format("System Loaded!"),
			wxS("[SUCCESS]"),wxOK|wxICON_INFORMATION);
		this->PrintInfoMessage(wxString::Format("System Ready.\n"));
	}
	return cFlag;
}
//------------------------------------------------------------------------------
bool my1Form::SaveSystem(const wxString& aFilename) {
	bool cFlag = true;
	int cLoop;
	wxFileName cName(aFilename);
	if (!cName.FileExists()) {
		wxFileOutputStream cTest(aFilename);
		if (!cTest.IsOk()) {
			wxMessageBox(wxString::Format("File: '%s'",aFilename),
				wxS("[CREATE ERROR]"),wxOK|wxICON_INFORMATION);
			return false;
		}
	}
	wxFileInputStream *pFile = new wxFileInputStream(aFilename);
	wxFileConfig cSystem(*pFile);
	// delete previous system, if applicable
	cSystem.DeleteAll();
	// throw in savefile id
	cSystem.SetPath(wxS("/System"));
	{
		wxString cKey = wxS("my1sim85key");
		wxString cVal = wxS("my1sim85chk");
		cFlag &= cSystem.Write(cKey,cVal);
		cSystem.SetPath(wxS("/"));
	}
	// save memory
	cLoop = 0;
	my1memory_t *pmem;
	my1list_t *list = m8085.MemoryList();
	list_scan_prep(list);
	while (list_scan_item(list)) {
		pmem = (my1memory_t*)list->curr->data;
		wxString cKey, cVal;
		int cValue;
		wxString cPath = wxString::Format("/Memory%d",cLoop++);
		cSystem.SetPath(cPath);
		cKey = wxS("Type");
		if (memd_is_ro(pmem)) cVal = wxS("ROM");
		else cVal = wxS("RAM");
		cFlag &= cSystem.Write(cKey,cVal);
		cKey = wxS("Info");
		cVal = wxS("Name:");
		if (memd_is_ro(pmem)) cVal += wxS("2764;");
		else cVal += wxS("6264;");
		cValue = memd_is_ro(pmem)?1:0;
		cVal += wxString::Format(wxS("ReadOnly:%d;"),cValue);
		cVal += wxString::Format(wxS("Start:%04X;"),pmem->addr.offs);
		cVal += wxString::Format(wxS("Size:%04X;"),pmem->addr.size);
		cFlag &= cSystem.Write(cKey,cVal);
		cSystem.SetPath(wxS("/"));
	}
	// save memory count
	cSystem.SetPath(wxS("/System"));
	{
		wxString cKey = wxS("CountM");
		long cVal = cLoop;
		cFlag &= cSystem.Write(cKey,cVal);
		cSystem.SetPath(wxS("/"));
	}
	// save device
	cLoop = 0;
	my1device_t *pdev;
	list = m8085.DeviceList();
	list_scan_prep(list);
	while (list_scan_item(list)) {
		pdev = (my1device_t*)list->curr->data;
		wxString cKey, cVal;
		wxString cPath = wxString::Format("/Device%d",cLoop++);
		cSystem.SetPath(cPath);
		cKey = wxS("Type");
		cVal = wxS("PPI");
		cFlag &= cSystem.Write(cKey,cVal);
		cKey = wxS("Info");
		cVal = wxS("Name:8255;");
		cVal += wxString::Format(wxS("Start:%02X;"),pdev->addr.offs);
		cVal += wxString::Format(wxS("Size:%02X;"),pdev->addr.size);
		cFlag &= cSystem.Write(cKey,cVal);
		cSystem.SetPath(wxS("/"));
	}
	// save memory count
	cSystem.SetPath(wxS("/System"));
	{
		wxString cKey = wxS("CountD");
		long cVal = cLoop;
		cFlag &= cSystem.Write(cKey,cVal);
		cSystem.SetPath(wxS("/"));
	}
	// save all controls??
	cLoop = 0;
	wxWindowList& cList = this->GetChildren();
	wxWindowList::Node *pNode = cList.GetFirst();
	while (pNode) {
		wxWindow *pTarget = (wxWindow*) pNode->GetData();
		if (pTarget->IsKindOf(wxCLASSINFO(my1DEVPanel))) {
			wxString cKey, cVal;
			wxString cPath = wxString::Format("/Control%d",cLoop++);
			cSystem.SetPath(cPath);
			wxAuiPaneInfo& cPane = mMainUI.GetPane(pTarget);
			if (!cPane.IsOk()) continue;
			cVal = mMainUI.SavePaneInfo(cPane);
			wxString cCheck = cVal.Mid(cVal.First(wxS("name=")));
			cCheck = cCheck.BeforeFirst(';');
			cCheck = cCheck.AfterFirst('=');
			cFlag &= cSystem.Write(wxS("Name"),cCheck);
			// get in form devXXX[X]YY
			cCheck = cCheck.Mid(3,cCheck.Length()-5);
			cFlag &= cSystem.Write(wxS("Type"),cCheck);
			// write flag
			my1DEVPanel* pPanel = (my1DEVPanel*) pTarget;
			cFlag &= cSystem.Write(wxS("Flag"),pPanel->Flag());
			// save bit information!
			int cBitCount = 0;
			wxWindowList& cBitList = pTarget->GetChildren();
			wxWindowList::Node *pBitNode = cBitList.GetFirst();
			while (pBitNode) {
				wxWindow *pBitCheck = (wxWindow*) pBitNode->GetData();
				if (pBitCheck->IsKindOf(wxCLASSINFO(my1BITCtrl))) {
					my1BITCtrl *pCtrl = (my1BITCtrl*) pBitCheck;
					cKey = wxString::Format("Bit%d",cBitCount++);
					cVal = wxString::Format("%d:%d:%d:%d:%d",
						pCtrl->Link().mDevice, pCtrl->Link().mDevicePort,
						pCtrl->Link().mDeviceBit, pCtrl->Link().mDeviceAddr,
						pCtrl->ActiveLevel());
					cFlag &= cSystem.Write(cKey,cVal);
				}
				pBitNode = pBitNode->GetNext();
			}
			cSystem.SetPath(wxS("/"));
		}
		pNode = pNode->GetNext();
	}
	// save control count
	cSystem.SetPath(wxS("/System"));
	{
		wxString cKey = wxS("CountC");
		long cVal = cLoop;
		cFlag &= cSystem.Write(cKey,cVal);
		cSystem.SetPath(wxS("/"));
	}
	// save layout
	cSystem.SetPath(wxS("/System"));
	{
		wxString cKey = wxS("Layout");
		wxString cVal = mMainUI.SavePerspective();
		cVal.Replace(wxS("="),wxS(":"));
		cVal.Replace(wxS(" "),wxS("__"));
		cFlag &= cSystem.Write(cKey,cVal);
		cSystem.SetPath(wxS("/"));
	}
	delete pFile;
	pFile = 0x0;
	// only if no errors
	if (cFlag) {
		wxFileOutputStream cFile(aFilename);
		cSystem.Save(cFile);
	}
	return cFlag;
}
//------------------------------------------------------------------------------
bool my1Form::LoadConfig(const wxString& aFilename) {
	bool cFlag = true;
	wxFileName cName(aFilename);
	if (!cName.FileExists()) return false;
	wxFileInputStream cRead(aFilename);
	wxFileConfig cSystem(cRead);
	wxString cVal, cKey;
	cSystem.SetPath(wxS("/Window")); // just in case
	cKey = wxS("chkID");
	if (!cSystem.Read(cKey,&cVal)||cVal!=wxS("my1ID"))
		return false;
	long tmp1, tmp2;
	tmp1 = cSystem.ReadLong(wxS("posX"),0);
	tmp2 = cSystem.ReadLong(wxS("posY"),0);
	if (!tmp1||!tmp2) return false;
	wxPoint here(tmp1,tmp2);
	this->SetPosition(here);
	tmp1 = cSystem.ReadLong(wxS("sizeX"),0);
	tmp2 = cSystem.ReadLong(wxS("sizeY"),0);
	if (!tmp1||!tmp2) return false;
	wxSize curr(tmp1,tmp2);
	this->SetSize(curr);
	return cFlag;
}
//------------------------------------------------------------------------------
bool my1Form::SaveConfig(const wxString& aFilename) {
	bool cFlag = true;
	wxFileName cName(aFilename);
	if (!cName.FileExists()) {
		wxFileOutputStream cTest(aFilename);
		if (!cTest.IsOk()) {
			wxMessageBox(wxString::Format("File: '%s'",aFilename),
				wxS("[CREATE ERROR]"),wxOK|wxICON_INFORMATION);
			return false;
		}
	}
	wxFileInputStream *pFile = new wxFileInputStream(aFilename);
	wxFileConfig cConfig(*pFile);
	wxString cKey, cVal;
	wxSize curr;
	wxPoint here;
	cConfig.SetPath(wxS("/Window"));
	curr = this->GetSize();
	cKey = wxS("chkID");
	cVal = wxS("my1ID");
	cFlag &= cConfig.Write(cKey,cVal);
	cKey = wxS("sizeX");
	cFlag &= cConfig.Write(cKey,curr.GetWidth());
	cKey = wxS("sizeY");
	cFlag &= cConfig.Write(cKey,curr.GetHeight());
	here = this->GetPosition();
	cKey = wxS("posX");
	cFlag &= cConfig.Write(cKey,here.x);
	cKey = wxS("posY");
	cFlag &= cConfig.Write(cKey,here.y);
	cConfig.SetPath(wxS("/"));
	delete pFile;
	pFile = 0x0;
	// only if no errors
	if (cFlag) {
		wxFileOutputStream cFile(aFilename);
		cConfig.Save(cFile);
	}
	return cFlag;
}
//------------------------------------------------------------------------------
void my1Form::SimUpdateREG(void* sobj) {
	// update register view
	wxString cFormat;
	my1reg85_t *preg = (my1reg85_t*) sobj;
	my1sys85_t *psys = (my1sys85_t*) preg->core;
	my1Form* pForm = (my1Form*)psys->pobj;
	int test, temp;
	if (preg->stat&REG85_STAT_PCNT) {
		cFormat = wxString::Format("%04X",preg->pcnt);
		pForm->mPCNTLink.Panel()->SetText(cFormat);
	}
	else if (preg->stat&REG85_STAT_SPTR) {
		cFormat = wxString::Format("%04X",preg->sptr);
		pForm->mSPTRLink.Panel()->SetText(cFormat);
	}
	else if (reg85_chk_wr(preg)) {
		test = reg85_get_last(preg);
		if (test&REG85_STAT_PAIR) {
			temp = test&~REG85_STAT_PAIR;
			if (temp==I8085_RP_BC) {
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_B]);
				pForm->mReg8Link[MY1SIMUI_REG8_B].Panel()->SetText(cFormat);
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_C]);
				pForm->mReg8Link[MY1SIMUI_REG8_C].Panel()->SetText(cFormat);
			}
			else if (temp==I8085_RP_DE) {
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_D]);
				pForm->mReg8Link[MY1SIMUI_REG8_D].Panel()->SetText(cFormat);
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_E]);
				pForm->mReg8Link[MY1SIMUI_REG8_E].Panel()->SetText(cFormat);
			}
			else if (temp==I8085_RP_HL) {
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_H]);
				pForm->mReg8Link[MY1SIMUI_REG8_H].Panel()->SetText(cFormat);
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_L]);
				pForm->mReg8Link[MY1SIMUI_REG8_L].Panel()->SetText(cFormat);
			}
			else if (temp==(I8085_RP_SP|REG85_STAT_PICK_PSW)) {
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_A]);
				pForm->mReg8Link[MY1SIMUI_REG8_A].Panel()->SetText(cFormat);
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_F]);
				pForm->mReg8Link[MY1SIMUI_REG8_F].Panel()->SetText(cFormat);
				// update flag bits?
				pForm->SimUpdateFLAG();
			}
			else if (temp==I8085_RP_SP) {
				cFormat = wxString::Format("%04X",preg->sptr);
				pForm->mSPTRLink.Panel()->SetText(cFormat);
			}
		}
		else {
			if (test==I8085_REG_B) {
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_B]);
				pForm->mReg8Link[MY1SIMUI_REG8_B].Panel()->SetText(cFormat);
			}
			else if (test==I8085_REG_C) {
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_C]);
				pForm->mReg8Link[MY1SIMUI_REG8_C].Panel()->SetText(cFormat);
			}
			else if (test==I8085_REG_D) {
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_D]);
				pForm->mReg8Link[MY1SIMUI_REG8_D].Panel()->SetText(cFormat);
			}
			else if (test==I8085_REG_E) {
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_E]);
				pForm->mReg8Link[MY1SIMUI_REG8_E].Panel()->SetText(cFormat);
			}
			else if (test==I8085_REG_H) {
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_H]);
				pForm->mReg8Link[MY1SIMUI_REG8_H].Panel()->SetText(cFormat);
			}
			else if (test==I8085_REG_L) {
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_L]);
				pForm->mReg8Link[MY1SIMUI_REG8_L].Panel()->SetText(cFormat);
			}
			else if (test==I8085_REG_A) {
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_A]);
				pForm->mReg8Link[MY1SIMUI_REG8_A].Panel()->SetText(cFormat);
			}
			else if (test==I8085_REG_F) {
				// we do not 'write' to flag???
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_F]);
				pForm->mReg8Link[MY1SIMUI_REG8_F].Panel()->SetText(cFormat);
				pForm->SimUpdateFLAG();
			}
			if (reg85_chk_flag(preg)) {
				cFormat = wxString::Format("%02X",preg->regs[I8085_REG_F]);
				pForm->mReg8Link[MY1SIMUI_REG8_F].Panel()->SetText(cFormat);
				pForm->SimUpdateFLAG();
			}
		}
	}
	reg85_clr_rdwr(preg);
}
//------------------------------------------------------------------------------
void my1Form::SimUpdateMEM(void* sobj) {
	// update memory view
	my1memory_t *pmem;
	word32_t temp;
	pmem = (my1memory_t*)sobj;
	if (!(pmem->addr.stat&ADDR_STAT_WRPROC)) return;
	temp = pmem->addr.last;
	//printf("## [%04X] <= %02X\n",temp+pmem->addr.offs,pmem->data[temp]);
	wxGrid *pGrid = (wxGrid*) FindWindowById(MY1ID_MEMGRID);
	int cGridWidth = pGrid->GetNumberCols();
	int cAddress = temp+pmem->addr.offs;
	int cCol = cAddress%cGridWidth;
	int cRow = cAddress/cGridWidth;
	int cData = pmem->data[temp];
	pGrid->SetCellValue(cRow,cCol,wxString::Format(wxS("%02X"),cData));
	// find mini viewers
	wxWindow* pParent = pGrid->GetGrandParent(); // get RegsPanel
	my1Form* pForm =  (my1Form*) pParent->GetParent(); // get the form!
	my1MiniViewer* pViewer = pForm->mFirstViewer;
	while (pViewer) {
		if (pViewer->IsSelected(cAddress)) {
			int cIndex = cAddress - pViewer->mStart;
			cGridWidth = pViewer->pGrid->GetNumberCols();
			cCol = cIndex%cGridWidth;
			cRow = cIndex/cGridWidth;
			pViewer->pGrid->SetCellValue(cRow,cCol,
				wxString::Format(wxS("%02X"),cData));
		}
		pViewer = pViewer->mNext;
	}
}
//------------------------------------------------------------------------------
#include "wxform_panels.cpp"
#include "wxform_toolbars.cpp"
#include "wxform_handlers.cpp"
#include "wxform_devpanels.cpp"
#include "wxform_prints.cpp"
//------------------------------------------------------------------------------
