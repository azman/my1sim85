//------------------------------------------------------------------------------
#ifndef __MY1BITSELECT_HPP__
#define __MY1BITSELECT_HPP__
//------------------------------------------------------------------------------
extern "C" {
#include "my1sys85.h"
}
//------------------------------------------------------------------------------
struct my1BitSelect : iolink_t {
	int mDevice;
	int mDevicePort;
	int mDeviceBit;
	int mDeviceAddr;
	void* mPointer;
	my1device_t* mDev;
	my1BitSelect():mDevice(0),mDevicePort(0),mDeviceBit(0),
			mDeviceAddr(-1),mPointer(0x0),mDev(0x0){
		iolink_init(this);
	}
	my1BitSelect(int anIndex) { this->UseIndex(anIndex); }
	my1BitSelect(int anID, void* aPointer) { this->UseSystem(anID,aPointer); }
	my1BitSelect(my1Sim85& psim, word32_t addr) {
		this->PickSelect(psim,addr);
	}
	word32_t PickSelect(my1Sim85& psim, word32_t addr) {
		if (psim.PrepLink(this,addr)) {
			if ((addr&SYS85_PORTMASK)==SYS85_INTRPORT) {
				mDev = 0x0;
				mDevice = -1;
				mDevicePort = -1;
				mDeviceAddr = -1;
			}
			else {
				mDev = (my1device_t*)psim.DeviceList()->curr->data;
				mDevice = psim.DeviceList()->step;
				mDevicePort = this->pick;
				mDeviceAddr = mDev->addr.offs;
			}
			mDeviceBit = this->pidx;
			if (this->pbit) mPointer = (void*)this->pbit;
			else mPointer = (void*)this->port;
		}
		return this->stat;
	}
	void UseIndex(int anIndex) {
		mDeviceBit = anIndex%DPORT_PCNT;
		anIndex = anIndex/DPORT_PCNT;
		mDevicePort = anIndex%(PPI8255_PCNT);
		mDevice = anIndex/(PPI8255_PCNT);
		mDeviceAddr = -1;
		mPointer = 0x0;
	}
	void UseSystem(int anID,void* aPointer) {
		mDevice = -1;
		mDevicePort = -1;
		mDeviceBit = anID;
		mDeviceAddr = -1;
		mPointer = aPointer;
	}
	int GetIndex(void) {
		int cIndex = mDevice*(PPI8255_PCNT)*DPORT_PCNT;
		cIndex += mDevicePort*DPORT_PCNT;
		cIndex += mDeviceBit;
		return cIndex;
	}
	void operator=(my1BitSelect& aSelect) {
		*((iolink_t*)this) = *((iolink_t*)&aSelect);
		mDevice = aSelect.mDevice;
		mDevicePort = aSelect.mDevicePort;
		mDeviceBit = aSelect.mDeviceBit;
		mDeviceAddr = aSelect.mDeviceAddr;
		mPointer = aSelect.mPointer;
	}
};
//------------------------------------------------------------------------------
#endif // __MY1BITSELECT_HPP__
//------------------------------------------------------------------------------
