//------------------------------------------------------------------------------
// wxform_panels.cpp
// - method implementations for creating panels in wxform
// - to be included directly in wxform
//------------------------------------------------------------------------------
wxPanel* my1Form::CreateInitPanel(wxWindow *parent) {
	wxPanel *cPanelX = new wxPanel(mNoteBook);
	wxFont cFont(PANEL_FONT_SIZE,wxFONTFAMILY_SWISS,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	cPanelX->SetFont(cFont);
	wxStaticText *tLabel = new wxStaticText(cPanelX,wxID_ANY,wxS(MY1APP_TITLE));
	wxFont tFont(TITLE_FONT_SIZE,wxFONTFAMILY_SWISS,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	tLabel->SetFont(tFont);
	wxStaticText *pLabel = new wxStaticText(cPanelX,wxID_ANY,
		wxS("8085 Microprocessor System Development"));
	wxFont pFont(SIMS_FONT_SIZE,wxFONTFAMILY_SWISS,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	pLabel->SetFont(pFont);
	wxString cAuth = wxS("by ");
	cAuth += wxS(MY1APP_AUTHMAIL);
	wxStaticText *eLabel = new wxStaticText(cPanelX,wxID_ANY,cAuth);
	wxFont eFont(EMAIL_FONT_SIZE,wxFONTFAMILY_SWISS,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	eLabel->SetFont(eFont);
	wxBoxSizer *aBoxSizer = new wxBoxSizer(wxVERTICAL);
	aBoxSizer->AddStretchSpacer();
	aBoxSizer->Add(tLabel,1,wxALIGN_CENTER);
	aBoxSizer->Add(pLabel,1,wxALIGN_CENTER);
	aBoxSizer->Add(eLabel,0,wxALIGN_RIGHT);
	cPanelX->SetSizerAndFit(aBoxSizer);
	return cPanelX;
}
//------------------------------------------------------------------------------
wxPanel* my1Form::CreateMainPanel(void) {
	wxPanel *cPanel = new wxPanel(this);
	wxFont cFont(PANEL_FONT_SIZE,wxFONTFAMILY_SWISS,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	cPanel->SetFont(cFont);
	// start sidebox content - build panel!
	my1Panel *cBuildHead = new my1Panel(cPanel,wxID_ANY,-1,
		wxS("Build Menu"),-1,-1,wxTAB_TRAVERSAL|wxBORDER_RAISED);
	cBuildHead->SetTextColor(*wxBLUE);
	cBuildHead->SetBackgroundColour(wxColor(0xAA,0xAA,0xAA));
	wxButton *cButtonRST = new wxButton(cPanel, MY1ID_BUILDRST, wxS("Reset"),
		wxDefaultPosition, wxDefaultSize);
	wxButton *cButtonDEF = new wxButton(cPanel, MY1ID_BUILDDEF, wxS("Default"),
		wxDefaultPosition, wxDefaultSize);
	wxButton *cButtonNFO = new wxButton(cPanel, MY1ID_BUILDNFO, wxS("Current"),
		wxDefaultPosition, wxDefaultSize);
	wxButton *cButtonROM = new wxButton(cPanel, MY1ID_BUILDROM, wxS("Add ROM"),
		wxDefaultPosition, wxDefaultSize);
	wxButton *cButtonRAM = new wxButton(cPanel, MY1ID_BUILDRAM, wxS("Add RAM"),
		wxDefaultPosition, wxDefaultSize);
	wxButton *cButtonPPI = new wxButton(cPanel, MY1ID_BUILDPPI, wxS("Add PPI"),
		wxDefaultPosition, wxDefaultSize);
	wxBoxSizer *sBoxSizer = new wxBoxSizer(wxVERTICAL);
	sBoxSizer->Add(cBuildHead, 1, wxEXPAND);
	sBoxSizer->Add(cButtonRST, 1, wxEXPAND);
	sBoxSizer->Add(cButtonDEF, 1, wxEXPAND);
	sBoxSizer->Add(cButtonNFO, 1, wxEXPAND);
	sBoxSizer->Add(cButtonROM, 1, wxEXPAND);
	sBoxSizer->Add(cButtonRAM, 1, wxEXPAND);
	sBoxSizer->Add(cButtonPPI, 1, wxEXPAND);
	cPanel->SetSizerAndFit(sBoxSizer);
	if (!mMainPanel) mMainPanel = cPanel;
	return cPanel;
}
//------------------------------------------------------------------------------
wxPanel* my1Form::CreateRegsPanel(void) {
	wxPanel *cPanel = new wxPanel(this);
	//wxFont cFont(PANEL_FONT_SIZE,wxFONTFAMILY_SWISS,
	//	wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	//cPanel->SetFont(cFont);
	m8085.Regs()->doupdate = &this->SimUpdateREG;
	// vertical layout
	wxBoxSizer *pBoxSizer = new wxBoxSizer(wxVERTICAL);
	// header panel - general purpose registers
	my1Panel *cHeader = new my1Panel(cPanel,wxID_ANY,-1,
		wxS("8-bit Registers"),REGS_PANEL_WIDTH,REGS_HEADER_HEIGHT,
		wxTAB_TRAVERSAL|wxBORDER_SUNKEN);
	pBoxSizer->Add(cHeader,0,wxEXPAND);
	// fill - general purpose registers
	for (int cLoop=0;cLoop<8;cLoop++) { // 8-bit regs
		wxBoxSizer *cBoxSizer = new wxBoxSizer(wxHORIZONTAL);
		int cRegID = cLoop;
		if (cLoop==7) cRegID = -1;
		else if (cLoop==6) cRegID = 4;
		else if (cLoop==5) cRegID = 10;
		else if (cLoop==4) cRegID = 6;
		wxString cRegName = wxString::Format(wxS("%c"),(char)cRegID+'B');
		my1Panel *cLabel = new my1Panel(cPanel,wxID_ANY,-1,cRegName,
			-1,-1,wxTAB_TRAVERSAL|wxBORDER_SUNKEN);
		wxString cRegValue = wxString::Format("%02X",m8085.Regs()->regs[cLoop]);
		my1Panel *cValue = new my1Panel(cPanel,wxID_ANY,
			cLoop,cRegValue,-1,-1,wxTAB_TRAVERSAL|wxBORDER_SUNKEN);
		cValue->SetTextColor(*wxBLUE);
		cValue->SetBackgroundColour(*wxWHITE);
		mReg8Link[cLoop].SetLink((void*)cValue);
		// add to row sizer
		cBoxSizer->AddSpacer(INFO_REG_SPACER);
		cBoxSizer->Add(cLabel,1,wxEXPAND);
		cBoxSizer->AddSpacer(INFO_REG_SPACER);
		cBoxSizer->Add(cValue,1,wxEXPAND);
		cBoxSizer->AddSpacer(INFO_REG_SPACER);
		// add to main sizer
		pBoxSizer->AddSpacer(INFO_REG_SPACER);
		pBoxSizer->Add(cBoxSizer,0,wxEXPAND);
	}
	pBoxSizer->AddSpacer(INFO_REG_SPACER);
	// header panel - system registers
	cHeader = new my1Panel(cPanel,wxID_ANY,-1,
		wxS("System Registers"),REGS_PANEL_WIDTH,REGS_HEADER_HEIGHT,
		wxTAB_TRAVERSAL|wxBORDER_SUNKEN);
	pBoxSizer->Add(cHeader,0,wxEXPAND);
	// program counter
	{
		wxBoxSizer *cBoxSizer = new wxBoxSizer(wxHORIZONTAL);
		wxString cRegName = wxS("PC");
		my1Panel *cLabel = new my1Panel(cPanel,wxID_ANY,-1,cRegName,
			-1,-1,wxTAB_TRAVERSAL|wxBORDER_SUNKEN);
		wxString cRegValue = wxString::Format("%04X",m8085.Regs()->pcnt);
		my1Panel *cValue = new my1Panel(cPanel,wxID_ANY,-1,cRegValue,
			-1,-1,wxTAB_TRAVERSAL|wxBORDER_SUNKEN);
		cValue->SetTextColor(*wxBLUE);
		cValue->SetBackgroundColour(*wxWHITE);
		mPCNTLink.SetLink((void*)cValue);
		// add to row sizer
		cBoxSizer->AddSpacer(INFO_REG_SPACER);
		cBoxSizer->Add(cLabel,1,wxEXPAND);
		cBoxSizer->AddSpacer(INFO_REG_SPACER);
		cBoxSizer->Add(cValue,1,wxEXPAND);
		cBoxSizer->AddSpacer(INFO_REG_SPACER);
		// add to main sizer
		pBoxSizer->AddSpacer(INFO_REG_SPACER);
		pBoxSizer->Add(cBoxSizer,0,wxEXPAND);
	}
	// stack pointer
	{
		wxBoxSizer *cBoxSizer = new wxBoxSizer(wxHORIZONTAL);
		wxString cRegName = wxS("SP");
		my1Panel *cLabel = new my1Panel(cPanel,wxID_ANY,-1,cRegName,
			-1,-1,wxTAB_TRAVERSAL|wxBORDER_SUNKEN);
		wxString cRegValue = wxString::Format("%04X",m8085.Regs()->sptr);
		my1Panel *cValue = new my1Panel(cPanel,wxID_ANY,-1,cRegValue,
			-1,-1,wxTAB_TRAVERSAL|wxBORDER_SUNKEN);
		cValue->SetTextColor(*wxBLUE);
		cValue->SetBackgroundColour(*wxWHITE);
		mSPTRLink.SetLink((void*)cValue);
		// add to row sizer
		cBoxSizer->AddSpacer(INFO_REG_SPACER);
		cBoxSizer->Add(cLabel,1,wxEXPAND);
		cBoxSizer->AddSpacer(INFO_REG_SPACER);
		cBoxSizer->Add(cValue,1,wxEXPAND);
		cBoxSizer->AddSpacer(INFO_REG_SPACER);
		// add to main sizer
		pBoxSizer->AddSpacer(INFO_REG_SPACER);
		pBoxSizer->Add(cBoxSizer,0,wxEXPAND);
	}
	pBoxSizer->AddSpacer(INFO_REG_SPACER);
	// header panel - flag bits
	cHeader = new my1Panel(cPanel,wxID_ANY,-1,
		wxS("Flag Bits"),REGS_PANEL_WIDTH,REGS_HEADER_HEIGHT,
		wxTAB_TRAVERSAL|wxBORDER_SUNKEN);
	pBoxSizer->Add(cHeader,0,wxEXPAND);
	pBoxSizer->AddSpacer(INFO_REG_SPACER);
	// flag labels
	{
		wxBoxSizer *cBoxSizer = new wxBoxSizer(wxHORIZONTAL);
		char cFlagHD[] = "SZXAXPXC";
		for (int cLoop=0;cLoop<8;cLoop++) { // flag header
			wxString cFlagName = cFlagHD[cLoop];
			my1Panel *cLabel = new my1Panel(cPanel,wxID_ANY,-1,cFlagName,
				-1,-1,wxTAB_TRAVERSAL|wxBORDER_SUNKEN);
			// add to row sizer
			cBoxSizer->Add(cLabel,1,wxEXPAND);
		}
		// add to main sizer
		pBoxSizer->Add(cBoxSizer,0,wxEXPAND);
	}
	// flag bits
	{
		wxBoxSizer *cBoxSizer = new wxBoxSizer(wxHORIZONTAL);
		int cFlagID = 0x80;
		for (int cLoop=0;cLoop<8;cLoop++,cFlagID>>=1) { // flag value
			bool cGoWhite = false;
			wxString cFlagValue = wxS("X");
			if (cFlagID&I8085_FLAG_BITS) {
				cFlagValue = wxString::Format(wxS("%01X"),
						(reg85_flag(m8085.Regs())&cFlagID)?1:0);
				cGoWhite = true;
			}
			my1Panel *cValue = new my1Panel(cPanel,wxID_ANY,-1,cFlagValue,
				-1,-1,wxTAB_TRAVERSAL|wxBORDER_SUNKEN);
			if (cGoWhite) {
				cValue->SetTextColor(*wxBLUE);
				cValue->SetBackgroundColour(*wxWHITE);
				mFlagLink[7-cLoop].SetLink((void*)cValue);
			}
			// add to row sizer
			cBoxSizer->Add(cValue,1,wxEXPAND);
		}
		// add to main sizer
		pBoxSizer->Add(cBoxSizer,0,wxEXPAND);
	}
	pBoxSizer->AddSpacer(INFO_REG_SPACER);
	// assign to main panel
	cPanel->SetSizerAndFit(pBoxSizer);
	return cPanel;
}
//------------------------------------------------------------------------------
wxPanel* my1Form::CreateMemsPanel(void) {
	wxPanel *cPanel = new wxPanel(this);
	//wxFont cFont(PANEL_FONT_SIZE,wxFONTFAMILY_SWISS,
	//	wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	//cPanel->SetFont(cFont);
	// vertical layout
	wxBoxSizer *pBoxSizer = new wxBoxSizer(wxVERTICAL);
	// header panel - memory space
	my1Panel *cHeader = new my1Panel(cPanel,wxID_ANY,-1,
		wxS("Memory Map"),REGS_PANEL_WIDTH,REGS_HEADER_HEIGHT,
		wxTAB_TRAVERSAL|wxBORDER_SUNKEN);
	pBoxSizer->Add(cHeader,0,wxEXPAND);
	pBoxSizer->AddSpacer(INFO_REG_SPACER);
	wxPanel *pPanelM = CreateMemoryGridPanel(cPanel,0x0000,
		MEM_VIEW_WIDTH,MEM_VIEW_HEIGHT,&mMemoryGrid);
	// add to main sizer
	pBoxSizer->Add(pPanelM,1,wxEXPAND);
	// assign to main panel
	cPanel->SetSizerAndFit(pBoxSizer);
	return cPanel;
}
//------------------------------------------------------------------------------
wxPanel* my1Form::CreateIntrPanel(void) {
	wxPanel *cPanel = new wxPanel(this);
	wxFont cFont(SIMS_FONT_SIZE,wxFONTFAMILY_SWISS,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	cPanel->SetFont(cFont);
	cPanel->SetLabel(wxS("INTPANEL"));
	wxBoxSizer *pBoxSizer = new wxBoxSizer(wxHORIZONTAL);
	int cInterruptID = I8085_PIN_TRAP;
	for (int cLoop=0;cLoop<I8085_PIN_COUNT;cLoop++) {
		wxString cLabel, cType;
		switch (cInterruptID) {
			case I8085_PIN_TRAP:
				cType = wxS("Trap");
				cLabel = wxString::Format(wxS("TRAP [0x%04X]"),I8085_ISR_TRP);
				break;
			case I8085_PIN_I7P5:
				cType = wxS("I7.5");
				cLabel = wxString::Format(wxS("I7.5 [0x%04X]"),I8085_ISR_7P5);
				break;
			case I8085_PIN_I6P5:
				cType = wxS("I6.5");
				cLabel = wxString::Format(wxS("I6.5 [0x%04X]"),I8085_ISR_6P5);
				break;
			case I8085_PIN_I5P5:
				cType = wxS("I5.5");
				cLabel = wxString::Format(wxS("I5.5 [0x%04X]"),I8085_ISR_5P5);
				break;
		}
		my1INTCtrl* pCtrl = new my1INTCtrl(cPanel,wxID_ANY,
			REGS_PANEL_WIDTH/I8085_PIN_COUNT,REGS_HEADER_HEIGHT*4/5,cType);
		pCtrl->SetLabel(cLabel);
		// get interrupt index & link, anID should be >=0 && <I8085_PIN_COUNT
		word32_t temp;
		temp = sys85_intr_addr(cInterruptID);
		my1BitSelect cLink(m8085,temp);
		pCtrl->LinkCheck(cLink);
		pBoxSizer->Add((wxWindow*)pCtrl,0,wxALIGN_CENTER);
		cInterruptID++;
	}
	cPanel->SetSizerAndFit(pBoxSizer);
	return cPanel;
}
//------------------------------------------------------------------------------
wxPanel* my1Form::CreateSimsPanel(void) {
	wxPanel *cPanel = new wxPanel(this);
	wxFont cFont(SIMS_FONT_SIZE,wxFONTFAMILY_SWISS,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	cPanel->SetFont(cFont);
	wxButton *cButtonStep = new wxButton(cPanel, MY1ID_SIMSSTEP, wxS("Step"),
		wxDefaultPosition, wxDefaultSize);
	wxButton *cButtonExec = new wxButton(cPanel, MY1ID_SIMSEXEC, wxS("Run"),
		wxDefaultPosition, wxDefaultSize);
	wxButton *cButtonRset = new wxButton(cPanel, MY1ID_SIMRESET, wxS("Reset"),
		wxDefaultPosition, wxDefaultSize);
	wxButton *cButtonBRKP = new wxButton(cPanel, MY1ID_SIMSBRKP, wxS("Break"),
		wxDefaultPosition, wxDefaultSize);
	wxButton *cButtonInfo = new wxButton(cPanel, MY1ID_SIMSINFO, wxS("Info"),
		wxDefaultPosition, wxDefaultSize);
	wxButton *cButtonPrev = new wxButton(cPanel, MY1ID_SIMSPREV, wxS("Prev"),
		wxDefaultPosition, wxDefaultSize);
	wxButton *cButtonMini = new wxButton(cPanel, MY1ID_SIMSMIMV, wxS("miniMV"),
		wxDefaultPosition, wxDefaultSize);
	wxButton *cButtonExit = new wxButton(cPanel, MY1ID_SIMSEXIT, wxS("Exit"),
		wxDefaultPosition, wxDefaultSize);
	wxBoxSizer *pBoxSizer = new wxBoxSizer(wxVERTICAL);
	pBoxSizer->Add(cButtonStep, 1, wxEXPAND);
	pBoxSizer->Add(cButtonExec, 1, wxEXPAND);
	pBoxSizer->Add(cButtonRset, 1, wxEXPAND);
	pBoxSizer->Add(cButtonBRKP, 1, wxEXPAND);
	pBoxSizer->Add(cButtonInfo, 1, wxEXPAND);
	pBoxSizer->Add(cButtonPrev, 1, wxEXPAND);
	pBoxSizer->Add(cButtonMini, 1, wxEXPAND);
	pBoxSizer->Add(cButtonExit, 1, wxEXPAND);
	cPanel->SetSizerAndFit(pBoxSizer);
	return cPanel;
}
//------------------------------------------------------------------------------
wxPanel* my1Form::CreateConsPanel(void) {
	wxPanel *cPanel = new wxPanel(this);
	wxTextCtrl *cConsole = new wxTextCtrl(cPanel, wxID_ANY,
		wxS(""), wxDefaultPosition, wxDefaultSize,
		wxTE_MULTILINE|wxTE_READONLY|wxTE_RICH, wxDefaultValidator);
	wxPanel *cComsPanel = new wxPanel(cPanel);
	wxTextCtrl *cCommandText = new wxTextCtrl(cComsPanel, MY1ID_CONSCOMM,
		wxS(""), wxDefaultPosition, wxDefaultSize,wxTE_PROCESS_ENTER);
	cCommandText->Connect(MY1ID_CONSCOMM,wxEVT_KEY_DOWN,
		WX_KEH(my1Form::OnCheckConsole),NULL,this);
	wxButton *cButton = new wxButton(cComsPanel, MY1ID_CONSEXEC,
		wxS("Execute"));
	wxBoxSizer *dBoxSizer = new wxBoxSizer(wxHORIZONTAL);
	dBoxSizer->Add(cCommandText, 1, wxEXPAND);
	dBoxSizer->Add(cButton, 0);
	cComsPanel->SetSizer(dBoxSizer);
	dBoxSizer->Fit(cComsPanel);
	dBoxSizer->SetSizeHints(cComsPanel);
	wxBoxSizer *eBoxSizer = new wxBoxSizer(wxVERTICAL);
	eBoxSizer->Add(cConsole, 1, wxEXPAND);
	eBoxSizer->Add(cComsPanel, 0, wxEXPAND);
	cPanel->SetSizerAndFit(eBoxSizer);
	wxFont cFont(CONS_FONT_SIZE,wxFONTFAMILY_TELETYPE,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL,
		false,wxEmptyString,wxFONTENCODING_ISO8859_1);
	cConsole->SetFont(cFont);
	cConsole->AppendText(wxString::Format(wxS("Welcome to %s\n\n"),
		MY1APP_TITLE));
	// 'remember' main console
	if (!mConsole) mConsole = cConsole;
	if (!mCommand) mCommand = cCommandText;
	if (!mComsPanel) mComsPanel = cComsPanel;
	if (mShowSystem) mComsPanel->Show();
	else mComsPanel->Hide();
	return cPanel;
}
//------------------------------------------------------------------------------
wxPanel* my1Form::CreateMemoryGridPanel(wxWindow* aParent, int aStart,
		int aWidth, int aHeight, wxGrid** ppGrid) {
	wxPanel *cPanel = new wxPanel(aParent);
	wxSizer *pBoxSizer = new wxBoxSizer(wxVERTICAL);
	wxGrid *pGrid = new wxGrid(cPanel, MY1ID_MEMGRID);
	pGrid->CreateGrid(aHeight,aWidth);
	//wxFont cFont(GRID_FONT_SIZE,wxFONTFAMILY_SWISS,
	//	wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	//pGrid->SetFont(cFont);
	//pGrid->SetLabelFont(cFont);
	//pGrid->UseNativeColHeader();
	pGrid->SetRowLabelAlignment(wxALIGN_CENTRE,wxALIGN_CENTRE);
	pGrid->SetColLabelAlignment(wxALIGN_CENTRE,wxALIGN_CENTRE);
	pGrid->SetDefaultCellAlignment(wxALIGN_CENTRE,wxALIGN_CENTRE);
	for (int cRow=0;cRow<aHeight;cRow++)
		pGrid->SetRowLabelValue(cRow,
			wxString::Format(wxS("%04X"),aStart+cRow*aWidth));
	for (int cCol=0;cCol<aWidth;cCol++)
		pGrid->SetColLabelValue(cCol,wxString::Format(wxS("%02X"),cCol));
	word32_t addr=aStart;
	byte08_t temp;
	for (int cRow=0;cRow<aHeight;cRow++) {
		for (int cCol=0;cCol<aWidth;cCol++) {
			if (m8085.MemoryRead(addr++,temp)) {
				pGrid->SetCellValue(cRow,cCol,
					wxString::Format(wxS("%02X"),temp));
			}
			else pGrid->SetCellValue(cRow,cCol,wxS("--"));
		}
	}
	pGrid->DisableCellEditControl();
	pGrid->EnableEditing(false);
	pGrid->SetRowLabelSize(wxGRID_AUTOSIZE);
	pGrid->AutoSize();
	for (int cRow=0;cRow<aHeight;cRow++)
		pGrid->DisableRowResize(cRow);
	for (int cCol=0;cCol<aWidth;cCol++)
		pGrid->DisableColResize(cCol);
	pBoxSizer->Add(pGrid,1,wxEXPAND);
	cPanel->SetSizerAndFit(pBoxSizer);
	*ppGrid = pGrid;
	return cPanel;
}
//------------------------------------------------------------------------------
void my1Form::BuildMainPanels(void) {
	// reg panel
	mMainUI.AddPane(CreateRegsPanel(), wxAuiPaneInfo().
		Name(wxS("regsPanel")).Caption(wxS("Registers")).
		DefaultPane().Left().Layer(AUI_EXTER_LAYER).Show(mShowSystem).
		Dockable(false).LeftDockable(true).
		MinSize(wxSize(REGS_PANEL_WIDTH,0)));
	// mem panel
	mMainUI.AddPane(CreateMemsPanel(), wxAuiPaneInfo().
		Name(wxS("memsPanel")).Caption(wxS("Memory")).
		DefaultPane().Right().Layer(AUI_EXTER_LAYER).Show(mShowSystem).
		Dockable(false).RightDockable(true).
		MinSize(wxSize(REGS_PANEL_WIDTH,0)));
	// interrupt panel
	mMainUI.AddPane(CreateIntrPanel(), wxAuiPaneInfo().
		Name(wxS("intrPanel")).Caption(wxS("Interrupts")).
		DefaultPane().Top().Show(mShowSystem).
		Dockable(false).TopDockable(true));
	// system panel
	mMainUI.AddPane(CreateMainPanel(), wxAuiPaneInfo().
		Name(wxS("systPanel")).Caption(wxS("System")).
		DefaultPane().Left().Layer(AUI_EXTER_LAYER).Show(mShowSystem).
		Dockable(false).LeftDockable(true));
	// sim panel
	mMainUI.AddPane(CreateSimsPanel(), wxAuiPaneInfo().
		Name(wxS("simsPanel")).Caption(wxS("Simulation")).
		DefaultPane().Left().Layer(AUI_INNER_LAYER).Show(mShowSystem).
		Dockable(false).LeftDockable(true).CloseButton(false));
	// tty panel
	mMainUI.AddPane(mTermCon, wxAuiPaneInfo().MaximizeButton(true).
		Name(wxS("termPanel")).Caption(wxS("Terminal Panel")).
		DefaultPane().Layer(AUI_OUTER_LAYER).
		Dockable(false).BottomDockable(true).Bottom().Position(BOT_TERM_POS).
		MinSize(wxSize(0,CONS_PANEL_HEIGHT)));
	// log panel
	mMainUI.AddPane(CreateConsPanel(), wxAuiPaneInfo().MaximizeButton(true).
		Name(wxS("consPanel")).Caption(wxS("Console Panel")).
		DefaultPane().Layer(AUI_OUTER_LAYER).
		Dockable(false).BottomDockable(true).Bottom().Position(BOT_CONS_POS).
		MinSize(wxSize(0,CONS_PANEL_HEIGHT)));
}
//------------------------------------------------------------------------------
