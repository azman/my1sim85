//------------------------------------------------------------------------------
// wxform.hpp
// - header for main wx-based form
//------------------------------------------------------------------------------
#ifndef __MY1FORM_HPP__
#define __MY1FORM_HPP__
//------------------------------------------------------------------------------
#include "wx/wx.h"
#include "wx/arrstr.h" // command history list
#include "wx/grid.h" // miniviewer needs this
#include "wx/aui/aui.h" // duh!
#include "wxpref.hpp"
#include "wxterm.hpp"
#include "wxmain.hpp"
#include "my1sim85.hpp"
#include "my1sim85_ui.hpp"
#include "my1bitselect.hpp"
//------------------------------------------------------------------------------
#define MACRO_WXBMP(bmp) wxBitmap(bmp##_xpm)
#define MACRO_WXICO(bmp) wxIcon(bmp##_xpm)
//------------------------------------------------------------------------------
#define MY1APP_TITLE "MY1 SIM85"
#ifndef MY1APP_PROGNAME
#define MY1APP_PROGNAME "my1sim85"
#endif
#ifndef MY1APP_PROGVERS
#define MY1APP_PROGVERS "build"
#endif
#ifndef MY1APP_AUTHMAIL
#define MY1APP_AUTHMAIL "azman@my1matrix.org"
#endif
#ifndef MY1APP_AUTHOR
#define MY1APP_AUTHOR "Azman M. Yusof <" MY1APP_AUTHMAIL ">"
#endif
#define PRINT_BPL_COUNT 0x10
#define CMD_HISTORY_COUNT 10
#define MY1APP_PROGCONF MY1APP_PROGNAME".conf"
//------------------------------------------------------------------------------
#define MY1ID_MAIN_OFFSET (wxID_HIGHEST+1)
#define MY1ID_DSEL_OFFSET (MY1ID_MAIN_OFFSET+500)
#define MY1ID_CPOT_OFFSET (MY1ID_DSEL_OFFSET+80)
#define MY1ID_CBIT_OFFSET (MY1ID_DSEL_OFFSET+280)
#define MY1ID_8085_OFFSET (MY1ID_DSEL_OFFSET+1600)
//------------------------------------------------------------------------------
enum {
	MY1ID_EXIT = MY1ID_MAIN_OFFSET,
	MY1ID_MAIN,
	MY1ID_MAIN_TERM,
	MY1ID_NEW,
	MY1ID_LOAD,
	MY1ID_SAVE,
	MY1ID_SAVEAS,
	MY1ID_SYSTEM,
	MY1ID_VIEW_SYSTPANE,
	MY1ID_VIEW_REGSPANE,
	MY1ID_VIEW_MEMSPANE,
	MY1ID_VIEW_INTRPANE,
	MY1ID_VIEW_CONSPANE,
	MY1ID_VIEW_TERMPANE,
	MY1ID_ASSEMBLE,
	MY1ID_SIMULATE,
	MY1ID_GENERATE,
	MY1ID_OPTIONS,
	MY1ID_ABOUT,
	MY1ID_WHATSNEW,
	MY1ID_README,
	MY1ID_FILETOOL,
	MY1ID_EDITTOOL,
	MY1ID_PROCTOOL,
	MY1ID_DEVCTOOL,
	MY1ID_STAT_TIMER,
	MY1ID_SIMX_TIMER,
	MY1ID_CONSCOMM,
	MY1ID_CONSEXEC,
	MY1ID_SIMSEXEC,
	MY1ID_SIMSSTEP,
	MY1ID_SIMSINFO,
	MY1ID_SIMSPREV,
	MY1ID_SIMRESET,
	MY1ID_SIMSMIMV,
	MY1ID_SIMSBRKP,
	MY1ID_SIMSEXIT,
	MY1ID_BUILDRST,
	MY1ID_BUILDDEF,
	MY1ID_BUILDNFO,
	MY1ID_BUILDROM,
	MY1ID_BUILDRAM,
	MY1ID_BUILDPPI,
	MY1ID_BUILDLOD,
	MY1ID_BUILDSAV,
	MY1ID_CREATE_MINIMV,
	MY1ID_CREATE_DV7SEG,
	MY1ID_CREATE_DVKPAD,
	MY1ID_CREATE_DEVLED,
	MY1ID_CREATE_DEVSWI,
	MY1ID_CREATE_DEVBUT,
	MY1ID_CREATE_DEVLVD,
	MY1ID_TOGGLE_ACTLVL,
	MY1ID_CHANGE_LABEL,
	MY1ID_CHANGE_COLOR,
	MY1ID_MEMGRID,
	MY1ID_DUMMY
};
//------------------------------------------------------------------------------
#define MY1SIMUI_REG8_B 0
#define MY1SIMUI_REG8_C 1
#define MY1SIMUI_REG8_D 2
#define MY1SIMUI_REG8_E 3
#define MY1SIMUI_REG8_H 4
#define MY1SIMUI_REG8_L 5
#define MY1SIMUI_REG8_F 6
#define MY1SIMUI_REG8_A 7
#define MY1SIMUI_FLAG_S 7
#define MY1SIMUI_FLAG_Z 6
#define MY1SIMUI_FLAG_A 4
#define MY1SIMUI_FLAG_P 2
#define MY1SIMUI_FLAG_C 0
//------------------------------------------------------------------------------
#include "wxform_minimv.hpp"
//------------------------------------------------------------------------------
class my1DEVPanel;
//------------------------------------------------------------------------------
class my1Form : public wxFrame {
private:
	friend class my1CodeEdit;
	bool mSimulationMode, mSimulationRunning, mSimulationStepping;
	bool mShowSystem;
	my1Sim85 m8085;
	my1SimUI mPCNTLink;
	my1SimUI mSPTRLink;
	my1SimUI mReg8Link[I8085_REG_COUNT];
	my1SimUI mFlagLink[I8085_BIT_COUNT];
	my1MiniViewer *mFirstViewer;
	my1App* myApp;
	wxArrayString mCmdHistory;
	int mCmdHistIndex;
	wxAuiManager mMainUI;
	my1Options mOptions;
	wxTimer *mDisplayTimer;
	wxTimer *mSimExecTimer;
	wxAuiNotebook *mNoteBook;
	wxAuiToolBar *mFileTool, *mEditTool, *mProcTool;
	wxTextCtrl *mConsole;
	wxTextCtrl *mCommand;
	wxPanel *mComsPanel;
	my1Term *mTermCon;
	wxMenu *mDevicePopupMenu;
	wxMenu *mDevicePortMenu;
	wxGrid *mMemoryGrid;
	wxStreamToTextRedirector *mRedirector;
	wxString mThisPath, mConfPath;
	wxPanel *mPortPanel;
	wxPanel *mMainPanel;
	wxWindowList mDevPanels;
public:
	my1Form(const wxString& title, const my1App* p_app);
	~my1Form();
	void SimulationMode(bool aGo=true);
	bool GetUniqueName(wxString&);
	bool LinkPanelToPort(wxPanel*,int);
	void OpenEdit(wxString&);
	void SaveEdit(wxWindow*, bool aSaveAs=false);
	void ShowStatus(wxString&);
	void SetupWorkPathConfig(void);
	void BuildMenu(void);
	void UpdateMenuState(bool);
	// messages
	void PrintMessage(const wxString&,bool aNewline=false);
	void PrintTaggedMessage(const wxString&,const wxString&,
		const wxColor& aTagColor=wxNullColour);
	void PrintInfoMessage(const wxString&, char* ptag=0x0);
	void PrintDebugMessage(const wxString&, char* ptag=0x0);
	void PrintErrorMessage(const wxString&);
	void PrintMemoryContent(word32_t, int aSize=PRINT_BPL_COUNT);
	void PrintPeripheralInfo(void);
	void PrintHelp(void);
	void PrintUnknownCommand(const wxString&);
	void PrintUnknownParameter(const wxString&,const wxString&);
	void PrintCodexInfo(my1inst_t* aCodex=0x0);
	// system ui
	int GetBuildAddress(const wxString&);
	my1dpin_t* GetDeviceBit(my1BitSelect&,bool useAddress=false);
	void UpdateDeviceBit(bool unLink=false);
	wxMenu* GetDevicePopupMenu(void);
	void ResetDevicePopupMenu(bool unLink=false);
	wxMenu* GetDevicePortMenu(void);
	void UpdateMemoryPanel(void);
	void SimUpdateFLAG(void);
	bool RemoveControls(void);
	// 'wrapper' functions
	bool SystemDefault(void);
	bool SystemDisconnect(void);
	bool ConnectROM(int aStart=SYS85_DEFBUILD_ROMADDR);
	bool ConnectRAM(int aStart=SYS85_DEFBUILD_RAMADDR);
	bool ConnectPPI(int aStart=SYS85_DEFBUILD_PPIADDR);
	// system load/save facilities
	bool LoadSystem(const wxString&);
	bool SaveSystem(const wxString&);
	bool LoadConfig(const wxString&);
	bool SaveConfig(const wxString&);
	// event handlers
	void OnFormClose(wxCloseEvent &event);
	void OnQuit(wxCommandEvent &event);
	void OnNew(wxCommandEvent &event);
	void OnLoad(wxCommandEvent &event);
	void OnSave(wxCommandEvent &event);
	void OnAbout(wxCommandEvent &event);
	void OnWhatsNew(wxCommandEvent &event);
	void OnReadMe(wxCommandEvent &event);
	void OnMenuHighlight(wxMenuEvent&);
	void OnAssemble(wxCommandEvent &event);
	void OnSimulate(wxCommandEvent &event);
	void OnGenerate(wxCommandEvent &event);
	void OnSysLoad(wxCommandEvent &event);
	void OnSysSave(wxCommandEvent &event);
	void OnCheckFont(wxKeyEvent &event);
	void OnCheckConsole(wxKeyEvent &event);
	void OnExecuteConsole(wxCommandEvent &event);
	void OnSimulationPick(wxCommandEvent &event);
	void OnSimulationInfo(wxCommandEvent &event);
	void OnBuildSelect(wxCommandEvent &event);
	void OnClosePane(wxAuiManagerEvent &event);
	void OnShowSystem(wxCommandEvent &event);
	void OnShowPanel(wxCommandEvent &event);
	void OnCheckOptions(wxCommandEvent &event);
	void OnStatusTimer(wxTimerEvent &event);
	void OnSimExeTimer(wxTimerEvent &event);
	void OnPageChanging(wxAuiNotebookEvent &event);
	void OnPageChanged(wxAuiNotebookEvent &event);
	void OnPageClosing(wxAuiNotebookEvent &event);
	void OnBITPanelClick(wxMouseEvent &event);
	void OnBITPortClick(wxCommandEvent &event);
	void OnMiniMVChanging(wxGridEvent& event);
	void AssignHandlers(void);
protected:
	// tool bars
	wxAuiToolBar* CreateFileToolBar(void);
	wxAuiToolBar* CreateEditToolBar(void);
	wxAuiToolBar* CreateViewToolBar(void);
	wxAuiToolBar* CreateProcToolBar(void);
	wxAuiToolBar* CreateDevcToolBar(void);
	void UpdateProcTool(bool);
	void BuildToolBars(void);
	// main panels
	wxPanel* CreateInitPanel(wxWindow*);
	wxPanel* CreateMainPanel(void);
	wxPanel* CreateRegsPanel(void);
	wxPanel* CreateMemsPanel(void);
	wxPanel* CreateIntrPanel(void);
	wxPanel* CreateConsPanel(void);
	wxPanel* CreateSimsPanel(void);
	wxPanel* CreateMemoryGridPanel(wxWindow*,int,int,int,wxGrid**);
	void BuildMainPanels(void);
	// virtual device panels
	wxPanel* CreateMemoryMiniPanel(int anAddress=-1);
	my1DEVPanel* CreateDevice7SegPanel(const wxString& aName=wxEmptyString);
	my1DEVPanel* CreateDeviceKPadPanel(const wxString& aName=wxEmptyString);
	my1DEVPanel* CreateDeviceLEDPanel(const wxString& aName=wxEmptyString,
		bool aVertical=false);
	my1DEVPanel* CreateDeviceSWIPanel(const wxString& aName=wxEmptyString);
	my1DEVPanel* CreateDeviceBUTPanel(const wxString& aName=wxEmptyString);
public:
	// static methods
	static void SimUpdateREG(void*);
	static void SimUpdateMEM(void*);
};
//------------------------------------------------------------------------------
#endif
//------------------------------------------------------------------------------
