//------------------------------------------------------------------------------
// wxform_minimv.hpp
// - mini memory viewer for wxform
//------------------------------------------------------------------------------
#ifndef __MY1FORM_MINIMV_HPP__
#define __MY1FORM_MINIMV_HPP__
//------------------------------------------------------------------------------
struct my1MiniViewer {
	int mStart, mSize;
	my1memory_t* pMemory;
	wxGrid* pGrid;
	my1MiniViewer* mNext;
	bool IsSelected(int anAddress) {
		if (anAddress>=this->mStart&&anAddress<this->mStart+this->mSize)
			return true;
		return false;
	}
};
//------------------------------------------------------------------------------
#endif /** __MY1FORM_MINIMV_HPP__ */
//------------------------------------------------------------------------------
