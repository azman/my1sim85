//------------------------------------------------------------------------------
// wxform_prints.cpp
// - method implementations for message prints in wxform
// - to be included directly in wxform
//------------------------------------------------------------------------------
void my1Form::PrintMessage(const wxString& aMessage, bool aNewline) {
	mConsole->AppendText(aMessage);
	if (aNewline) mConsole->AppendText("\n");
}
//------------------------------------------------------------------------------
void my1Form::PrintTaggedMessage(const wxString& aTag, const wxString& aMessage,
		const wxColor& aTagColor) {
	long cPosB, cPosE;
	wxTextAttr cTextAttr;
	wxColor cSaveColor;
	wxString cTag;
	cPosB = mConsole->GetInsertionPoint();
	mConsole->GetStyle(cPosB,cTextAttr);
	cTag = wxS("[") + aTag + wxS("] ");
	this->PrintMessage(cTag);
	if (aTagColor!=wxNullColour) {
		cSaveColor = cTextAttr.GetTextColour();
		cPosE = mConsole->GetInsertionPoint();
		cTextAttr.SetTextColour(aTagColor);
		mConsole->SetStyle(cPosB,cPosE-1,cTextAttr);
	}
	mConsole->ShowPosition(mConsole->GetInsertionPoint());
	mConsole->Refresh();
	mConsole->Update();
	this->PrintMessage(aMessage,true);
}
//------------------------------------------------------------------------------
void my1Form::PrintInfoMessage(const wxString& aMessage, char* ptag) {
	wxString cTag;
	if (!ptag) cTag = wxString("INFO");
	else cTag = wxString(ptag);
	this->PrintTaggedMessage(cTag,aMessage,*wxBLUE);
}
//------------------------------------------------------------------------------
void my1Form::PrintDebugMessage(const wxString& aMessage, char* ptag) {
	wxString cTag;
	if (!ptag) cTag = wxString("DEBUG");
	else cTag = wxString(ptag);
	this->PrintTaggedMessage(cTag,aMessage,*wxGREEN);
}
//------------------------------------------------------------------------------
void my1Form::PrintErrorMessage(const wxString& aMessage) {
	mConsole->AppendText("\n"); // always on a new line
	this->PrintTaggedMessage(wxS("ERROR"),aMessage,*wxRED);
}
//------------------------------------------------------------------------------
void my1Form::PrintMemoryContent(word32_t anAddress, int aSize) {
	word32_t cAddress = anAddress;
	if (cAddress%PRINT_BPL_COUNT!=0)
		cAddress -= (cAddress%PRINT_BPL_COUNT);
	if (aSize%PRINT_BPL_COUNT!=0)
		aSize += (aSize%PRINT_BPL_COUNT);
	byte08_t cData;
	int cCount = 0;
	// print header!
	this->PrintMessage(wxS("\n--------"));
	for (int cLoop=0;cLoop<PRINT_BPL_COUNT;cLoop++)
		this->PrintMessage(wxS("-----"));
	this->PrintMessage(wxS("\n|      |"));
	for (int cLoop=0;cLoop<PRINT_BPL_COUNT;cLoop++) {
		this->PrintMessage(wxS(" "));
		this->PrintMessage(wxString::Format(wxS("%02X"),cLoop));
		this->PrintMessage(wxS(" |"));
	}
	// print table!
	while (cCount<aSize&&cAddress<I8085_MAX_MSIZE) {
		if (!m8085.MemoryRead(cAddress,cData)) {
			this->PrintMessage(wxS("\n"));
			this->PrintErrorMessage(wxS("Cannot read from address 0x")+
				wxString::Format(wxS("%04X!"),cAddress));
			break;
		}
		if (cCount%PRINT_BPL_COUNT==0) {
			this->PrintMessage(wxS("\n--------"));
			for(int cLoop=0;cLoop<PRINT_BPL_COUNT;cLoop++)
				this->PrintMessage(wxS("-----"));
			this->PrintMessage(wxS("\n| "));
			this->PrintMessage(wxString::Format(wxS("%04X"),cAddress));
			this->PrintMessage(wxS(" |"));
		}
		this->PrintMessage(wxS(" "));
		this->PrintMessage(wxString::Format(wxS("%02X"),cData));
		this->PrintMessage(wxS(" |"));
		cCount++; cAddress++;
	}
	this->PrintMessage(wxS("\n--------"));
	for(int cLoop=0;cLoop<PRINT_BPL_COUNT;cLoop++)
		this->PrintMessage(wxS("-----"));
	this->PrintMessage(wxS("\n"));
}
//------------------------------------------------------------------------------
void my1Form::PrintPeripheralInfo(void) {
	my1list_t* list;
	my1adev_t* pdev;
	my1cstr_t buff;
	cstr_init(&buff);
	cstr_resize(&buff,32);
	list = m8085.MemoryList();
	this->PrintMessage(wxS("\nMemory Count: "));
	this->PrintMessage(wxString::Format(wxS("%d"),list->size));
	this->PrintMessage(wxS("\n"));
	list_scan_prep(list);
	while (list_scan_item(list)) {
		pdev = (my1adev_t*)list->curr->data;
		this->PrintMessage(wxS("(Memory) Name: "));
		switch (pdev->uuid) {
			case ROM2764_UUID: cstr_setstr(&buff,(char*)"2764"); break;
			case RAM6264_UUID: cstr_setstr(&buff,(char*)"6264"); break;
			default: cstr_setstr(&buff,(char*)"????"); break;
		}
		this->PrintMessage(buff.buff);
		this->PrintMessage(wxS(", "));
		this->PrintMessage(wxS("Read-Only: "));
		this->PrintMessage(memd_is_ro(pdev)?wxS("YES"):wxS("NO "));
		this->PrintMessage(wxS(", "));
		this->PrintMessage(wxS("Start: 0x"));
		this->PrintMessage(wxString::Format(wxS("%04X"),pdev->addr.offs));
		this->PrintMessage(wxS(", "));
		this->PrintMessage(wxS("Size: 0x"));
		this->PrintMessage(wxString::Format(wxS("%04X"),pdev->addr.size));
		this->PrintMessage(wxS("\n"));
	}
	list = m8085.DeviceList();
	this->PrintMessage(wxS("Device Count: "));
	this->PrintMessage(wxString::Format(wxS("%d"),list->size));
	this->PrintMessage(wxS("\n"));
	list_scan_prep(list);
	while (list_scan_item(list)) {
		pdev = (my1adev_t*)list->curr->data;
		this->PrintMessage(wxS("(Device) Name: "));
		switch (pdev->uuid) {
			case PPI8255_UUID: cstr_setstr(&buff,(char*)"8255"); break;
			default: cstr_setstr(&buff,(char*)"????"); break;
		}
		this->PrintMessage(buff.buff);
		this->PrintMessage(wxS(", "));
		this->PrintMessage(wxS("Start: 0x"));
		this->PrintMessage(wxString::Format(wxS("%04X"),pdev->addr.offs));
		this->PrintMessage(wxS(", "));
		this->PrintMessage(wxS("Size: 0x"));
		this->PrintMessage(wxString::Format(wxS("%04X"),pdev->addr.size));
		this->PrintMessage(wxS("\n"));
	}
	cstr_free(&buff);
}
//------------------------------------------------------------------------------
void my1Form::PrintHelp(void) {
	mConsole->AppendText(wxS("\nAvailable command(s):\n"));
	mConsole->AppendText(wxS("- show [system|mem=?|minimv=?]\n"));
	mConsole->AppendText(wxS("  > system (print system info)\n"));
	mConsole->AppendText(wxS("  > info (print codex info)\n"));
	mConsole->AppendText(wxS("  > prev (print previous codex info)\n"));
	mConsole->AppendText(wxS("  > mem=? (show memory @ given addr)\n"));
	mConsole->AppendText(wxS("  > minimv=? (show minimv @ given addr)\n"));
	mConsole->AppendText(wxS("- build [default|reset|rom=?|ram=?|ppi=?]\n"));
	mConsole->AppendText(wxS("  > default (build default system)\n"));
	mConsole->AppendText(wxS("  > reset (reset system build)\n"));
	mConsole->AppendText(wxS("  > rom=? (add 2764 ROM @given addr)\n"));
	mConsole->AppendText(wxS("  > ram=? (add 6264 RAM @given addr)\n"));
	mConsole->AppendText(wxS("  > ppi=? (add 8255 PPI @given addr)\n"));
	mConsole->AppendText(wxS("- data @addr16[data8]\n"));
	mConsole->AppendText(wxS("  > data @addr16 (show data @addr16)\n"));
	mConsole->AppendText(wxS("  > data @addr16=data8 (save data8 @addr16)\n"));
	mConsole->AppendText(wxS("  **addr16 in hex notation\n"));
	mConsole->AppendText(wxS("  **data8 in decimal (0x- prefix for hex)\n"));
	mConsole->AppendText(wxS("- clear\n"));
	mConsole->AppendText(wxS("  > clear this console\n"));
	mConsole->AppendText(wxS("- help\n"));
	mConsole->AppendText(wxS("  > show this text\n"));
}
//------------------------------------------------------------------------------
void my1Form::PrintUnknownCommand(const wxString& aCommand) {
	mConsole->AppendText(wxS("\nUnknown command '"));
	mConsole->AppendText(aCommand);
	mConsole->AppendText(wxS("'\n"));
}
//------------------------------------------------------------------------------
void my1Form::PrintUnknownParameter(const wxString& aParam,
		const wxString& aCommand) {
	mConsole->AppendText(wxS("\nUnknown parameter '"));
	mConsole->AppendText(aParam);
	mConsole->AppendText(wxS("' for ["));
	mConsole->AppendText(aCommand);
	mConsole->AppendText(wxS("]\n"));
}
//------------------------------------------------------------------------------
void my1Form::PrintCodexInfo(my1inst_t* aCodex) {
	wxString info;
	if (!aCodex) {
		if (!m8085.Curr()) return;
		aCodex = m8085.Curr();
	}
	info = "[Codex] ";
	info += wxString::Format("Addr:%04X, ",aCodex->addr);
	info += wxString::Format("Line:%d, ",aCodex->line);
	info += wxS("Data: ");
	for (int cLoop=0;cLoop<aCodex->size;cLoop++)
		info += wxString::Format("%02X, ",aCodex->data[cLoop]);
	info += wxS("T-States: ");
	if (aCodex==m8085.Curr())
		info += wxString::Format("%d, ",m8085.Tstates(0));
	else if (aCodex==m8085.Next())
		info += wxS("Waiting execution! ");
	else info += wxS("??? ");
	info += wxString::Format("Total T-states:%d",m8085.Tstates());
	this->PrintInfoMessage(info);
}
//------------------------------------------------------------------------------
