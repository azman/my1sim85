//------------------------------------------------------------------------------
// wxpaneldev.cpp
// - implementation for wx-based panel control for virtual device
//------------------------------------------------------------------------------
#include "wxpaneldev.hpp"
//------------------------------------------------------------------------------
wxIMPLEMENT_DYNAMIC_CLASS(my1DEVPanel, wxWindow);
//------------------------------------------------------------------------------
#define WX_SEH wxSizeEventHandler
//------------------------------------------------------------------------------
my1DEVPanel::my1DEVPanel(wxWindow* parent, wxWindowID id, int aCheck,
		int aWidth, int aHeight, long style)
		: my1Panel(parent, id, aCheck, wxEmptyString, aWidth, aHeight, style) {
	delete mText; mText = 0x0;
	myForm = (my1Form*) parent;
	mFlag = false;
	this->Disconnect(wxEVT_SIZE, WX_SEH(my1Panel::OnResize));
}
//------------------------------------------------------------------------------
my1DEVPanel::my1DEVPanel() : my1Panel(0x0, wxID_ANY) {
	//this->Hide();
}
//------------------------------------------------------------------------------
my1DEVPanel::~my1DEVPanel() {
	myForm->ResetDevicePopupMenu();
}
//------------------------------------------------------------------------------
bool my1DEVPanel::Flag(void) {
	return mFlag;
}
//------------------------------------------------------------------------------
void my1DEVPanel::Flag(bool aFlag) {
	mFlag = aFlag;
}
//------------------------------------------------------------------------------
