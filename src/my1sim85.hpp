//------------------------------------------------------------------------------
#ifndef __MY1SIM85_HPP__
#define __MY1SIM85_HPP__
//------------------------------------------------------------------------------
extern "C" {
#include "my1sys85.h"
}
//------------------------------------------------------------------------------
#define SIM85_STAT_CODE_OK 0x00000001
#define SIM85_STAT_CORE_OK 0x00000002
#define SIM85_STAT_INIT_OK 0x00000004
#define SIM85_STAT_NO_CODE 0x00010000
#define SIM85_STAT_XX_CODE 0x00020000
#define SIM85_STAT_INVALID (SIM85_STAT_NO_CODE|SIM85_STAT_XX_CODE)
//------------------------------------------------------------------------------
#define SIM85_FLAG_ECODE2MEM 0x00000001
#define SIM85_FLAG_ECODEFIND 0x00000002
#define SIM85_FLAG_ESTEPEXEC 0x00000004
//------------------------------------------------------------------------------
class my1Sim85 { // : public my1Sim8085
protected:
	my1sys85_t mSyst;
	my1inst_t *mNext, *mCurr;
	my1memory_t* mLast;
	void* mCodeLink;
	word32_t mStat, mFlag;
	word32_t mCmem; // used by Code2MEM
	word32_t mChex; // used by Code2HEX
public:
	my1Sim85();
	virtual ~my1Sim85();
	void SystemObject(void*);
	void UpdateHandler(regstask_t);
	bool Ready(void);
	bool Built(void);
	bool Halted(void);
	bool Interrupted(void);
	bool ZeroCode(void);
	int GetStartAddress(void);
	void SetStartAddress(int);
	void* GetCodeLink(void);
	void SetCodeLink(void*);
	word32_t Stat(word32_t mask=((word32_t)~0));
	word32_t Flag(word32_t mask=((word32_t)~0));
	word32_t C2mem(void);
	word32_t C2hex(void);
	word32_t CodeSize(void);
	word32_t Tstates(int full=1);
	my1inst_t* Next(void);
	my1inst_t* Curr(void);
	my1inst_t* Reset(word32_t addr=0x10000,int cold=0);
protected:
	// code management
	word32_t Code2MEM(void);
	word32_t Code2HEX(char*);
public:
	// simulation functions
	bool Step(void);
	bool Exec(int aStep=1);
	// build function
	bool DisconnectALL(void);
	bool ConnectROM(int aStart);
	bool ConnectRAM(int aStart);
	bool ConnectPPI(int aStart);
	// high-level sim interface
	bool Assemble(const char*,const char*);
	bool Generate(const char*); // hex file!
	bool Simulate(int aStep=1);
	// for external access
	my1list_t* MemoryList(void);
	my1list_t* DeviceList(void);
	my1dpin_t* Pin(int); // my1dpin_t=my1BitIO
	my1memory_t* Memory(word32_t anAddr);
	my1memory_t* LastMemory(void);
	my1device_t* Device(word32_t anAddr, int* pIndex);
	my1device_t* DevicePick(int anIndex);
	bool PrepLink(iolink_t* link, word32_t anAddr);
	bool MemoryRead(word32_t anAddr, byte08_t& rData);
	bool MemoryWrite(word32_t anAddr, byte08_t rData);
	my1reg85_t* Regs(void);
	int GetCodexLine(void);
	int GetNextLine(void);
};
//------------------------------------------------------------------------------
#endif
//------------------------------------------------------------------------------
