//------------------------------------------------------------------------------
// wxform_toolbars.cpp
// - method implementations for creating toolbars in wxform
// - to be included directly in wxform
//------------------------------------------------------------------------------
wxAuiToolBar* my1Form::CreateFileToolBar(void) {
	wxBitmap mIconExit = MACRO_WXBMP(exit);
	wxBitmap mIconNewd = MACRO_WXBMP(newd);
	wxBitmap mIconLoad = MACRO_WXBMP(open);
	wxBitmap mIconSave = MACRO_WXBMP(save);
	wxAuiToolBar* fileTool = new wxAuiToolBar(this, MY1ID_FILETOOL,
		wxDefaultPosition, wxDefaultSize, wxAUI_TB_DEFAULT_STYLE);
	fileTool->SetToolBitmapSize(wxSize(16,16));
	fileTool->AddTool(MY1ID_EXIT, wxS("Exit"), mIconExit, wxS("Exit"));
	fileTool->AddSeparator();
	fileTool->AddTool(MY1ID_NEW, wxS("Clear"), mIconNewd, wxS("New"));
	fileTool->AddTool(MY1ID_LOAD, wxS("Open"), mIconLoad, wxS("Open"));
	fileTool->AddTool(MY1ID_SAVE, wxS("Save"), mIconSave, wxS("Save"));
	fileTool->Realize();
	if(!mFileTool) mFileTool = fileTool;
	return fileTool;
}
//------------------------------------------------------------------------------
wxAuiToolBar* my1Form::CreateEditToolBar(void) {
	wxAuiToolBar* editTool = new wxAuiToolBar(this, MY1ID_EDITTOOL,
		wxDefaultPosition, wxDefaultSize, wxAUI_TB_DEFAULT_STYLE);
	editTool->SetToolBitmapSize(wxSize(16,16));
	editTool->AddTool(MY1ID_SYSTEM, wxS("System"), MACRO_WXBMP(build),
		wxS("System"), wxITEM_CHECK);
	editTool->AddSeparator();
	editTool->AddTool(MY1ID_VIEW_CONSPANE, wxS("Show Console"),
		MACRO_WXBMP(cons),wxS("Console"));
	editTool->AddTool(MY1ID_VIEW_TERMPANE, wxS("Show Terminal"),
		MACRO_WXBMP(term),wxS("Terminal"));
	editTool->AddSeparator();
	editTool->AddTool(MY1ID_OPTIONS, wxS("Options"), MACRO_WXBMP(option),
		wxS("Options"));
	editTool->Realize();
	if(!mEditTool) mEditTool = editTool;
	return editTool;
}
//------------------------------------------------------------------------------
wxAuiToolBar* my1Form::CreateProcToolBar(void) {
	wxBitmap mIconAssemble = MACRO_WXBMP(binary);
	wxBitmap mIconSimulate = MACRO_WXBMP(simx);
	wxBitmap mIconGenerate = MACRO_WXBMP(hexgen);
	wxAuiToolBar* procTool = new wxAuiToolBar(this, MY1ID_PROCTOOL,
		wxDefaultPosition, wxDefaultSize, wxAUI_TB_DEFAULT_STYLE);
	procTool->SetToolBitmapSize(wxSize(16,16));
	procTool->AddTool(MY1ID_ASSEMBLE, wxS("Assemble"),
		mIconAssemble, wxS("Assemble"));
	procTool->AddTool(MY1ID_GENERATE, wxS("Generate"),
		mIconGenerate, wxS("Generate"));
	procTool->AddTool(MY1ID_SIMULATE, wxS("Simulate"),
		mIconSimulate, wxS("Simulate"));
	procTool->Realize();
	if(!mProcTool) mProcTool = procTool;
	return procTool;
}
//------------------------------------------------------------------------------
wxAuiToolBar* my1Form::CreateDevcToolBar(void) {
	wxBitmap mIconDEVLED = MACRO_WXBMP(devled);
	wxBitmap mIconDEVSWI = MACRO_WXBMP(devswi);
	wxBitmap mIconDEVBUT = MACRO_WXBMP(devbut);
	wxBitmap mIconDV7SEG = MACRO_WXBMP(dv7seg);
	wxBitmap mIconDVKPAD = MACRO_WXBMP(dvkpad);
	wxBitmap mIconMiniMV = MACRO_WXBMP(target);
	wxAuiToolBar* devcTool = new wxAuiToolBar(this, MY1ID_DEVCTOOL,
		wxDefaultPosition, wxDefaultSize, wxAUI_TB_DEFAULT_STYLE);
	devcTool->SetToolBitmapSize(wxSize(16,16));
	devcTool->AddTool(MY1ID_CREATE_DEVLED, wxS("LED"),
		mIconDEVLED, wxS("LED"));
	devcTool->AddTool(MY1ID_CREATE_DEVSWI, wxS("Switch"),
		mIconDEVSWI, wxS("Switch"));
	devcTool->AddTool(MY1ID_CREATE_DEVBUT, wxS("Button"),
		mIconDEVBUT, wxS("Button"));
	devcTool->AddTool(MY1ID_CREATE_DV7SEG, wxS("7-segment"),
		mIconDV7SEG, wxS("7-segment"));
	devcTool->AddTool(MY1ID_CREATE_DVKPAD, wxS("Keypad"),
		mIconDVKPAD, wxS("Keypad"));
	devcTool->AddSeparator();
	devcTool->AddTool(MY1ID_CREATE_MINIMV, wxS("MiniMV"),
		mIconMiniMV, wxS("Create Mini MemViewer"));
	devcTool->Realize();
	return devcTool;
}
//------------------------------------------------------------------------------
void my1Form::UpdateProcTool(bool cEditMode) {
	mProcTool->EnableTool(MY1ID_ASSEMBLE,cEditMode);
	mProcTool->EnableTool(MY1ID_GENERATE,cEditMode);
	mProcTool->EnableTool(MY1ID_SIMULATE,cEditMode&&mShowSystem);
	if (mEditTool->GetToolToggled(MY1ID_SYSTEM)!=mShowSystem)
		mEditTool->ToggleTool(MY1ID_SYSTEM,mShowSystem);
}
//------------------------------------------------------------------------------
void my1Form::BuildToolBars(void) {
	// tool bar - file
	mMainUI.AddPane(CreateFileToolBar(), wxAuiPaneInfo().
		Name(wxS("fileTool")).ToolbarPane().Top().Row(0).
		Position(TOOL_FILE_POS).Floatable(false).BottomDockable(false).
		LeftDockable(false).RightDockable(false));
	// tool bar - edit
	mMainUI.AddPane(CreateEditToolBar(), wxAuiPaneInfo().
		Name(wxS("editTool")).ToolbarPane().Top().Row(0).
		Position(TOOL_EDIT_POS).Floatable(false).BottomDockable(false).
		LeftDockable(false).RightDockable(false));
	// tool bar - proc
	mMainUI.AddPane(CreateProcToolBar(), wxAuiPaneInfo().
		Name(wxS("procTool")).ToolbarPane().Top().Row(0).
		Position(TOOL_PROC_POS).Floatable(false).BottomDockable(false).
		LeftDockable(false).RightDockable(false));
	// tool bar - device
	mMainUI.AddPane(CreateDevcToolBar(), wxAuiPaneInfo().
		Name(wxS("devcTool")).ToolbarPane().Top().Row(0).
		Position(TOOL_DEVC_POS).Floatable(false).BottomDockable(false).
		LeftDockable(false).RightDockable(false));
}
//------------------------------------------------------------------------------
