//------------------------------------------------------------------------------
#ifndef __MY1SIM85_UI_HPP__
#define __MY1SIM85_UI_HPP__
//------------------------------------------------------------------------------
#include "wxpanel.hpp"
//------------------------------------------------------------------------------
struct my1SimUI {
	void *mLink;
	my1SimUI() {
		mLink = 0x0;
	}
	my1Panel* Panel(void) {
		return (my1Panel*)mLink;
	}
	void* GetLink(void) {
		return mLink;
	}
	void SetLink(void* aLink) {
		mLink = aLink;
	}
	void Unlink(void) {
		SetLink(0x0);
	}
};
//------------------------------------------------------------------------------
#endif // __MY1SIM85_UI_HPP__
//------------------------------------------------------------------------------
