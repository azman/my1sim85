//------------------------------------------------------------------------------
// wxpaneldev.hpp
// - header for wx-based panel control for virtual device
//------------------------------------------------------------------------------
#ifndef __MY1PANELDEV_HPP__
#define __MY1PANELDEV_HPP__
//------------------------------------------------------------------------------
#include "wxpanel.hpp"
#include "wxform.hpp"
//------------------------------------------------------------------------------
class my1DEVPanel : public my1Panel {
	wxDECLARE_DYNAMIC_CLASS(my1DEVPanel);
protected:
	my1Form *myForm;
	bool mFlag;
	// redefine access
	const wxString& GetText(void);
	void SetText(const wxString&);
public:
	my1DEVPanel(wxWindow*,wxWindowID id=wxID_ANY,int aCheck=-1,
		int aWidth=-1,int aHeight=-1,long style=wxTAB_TRAVERSAL);
	my1DEVPanel();
	~my1DEVPanel();
	bool Flag(void);
	void Flag(bool);
};
//------------------------------------------------------------------------------
#endif
//------------------------------------------------------------------------------
