//------------------------------------------------------------------------------
// wxform_devpanels.cpp
// - method implementations for creating device panels in wxform
// - to be included directly in wxform
//------------------------------------------------------------------------------
wxPanel* my1Form::CreateMemoryMiniPanel(int cAddress) {
	if (cAddress<0) {
		cAddress = this->GetBuildAddress(wxS("Start Address for miniMV"));
		if (cAddress<0) return 0x0;
	}
	if (cAddress%8!=0) {
		cAddress = cAddress-cAddress%8;
		wxString cStatus = wxS("[miniMV] Address must be in multiples of 8!") +
			wxString::Format(wxS(" Using [0x%04X]"),cAddress);
		this->PrintInfoMessage(cStatus);
	}
	my1memory_t* pMemory = (my1memory_t*) m8085.Memory((word32_t)cAddress);
	if (!pMemory) {
		wxString cStatus = wxS("[miniMV] Creation Error!");
		cStatus += wxS(" No memory object at address ") +
			wxString::Format(wxS("0x%04X!"),cAddress);
		this->PrintErrorMessage(cStatus);
		return 0x0;
	}
	wxString cPanelName = wxS("miniMV") +
		wxString::Format(wxS("%04X"),cAddress);
	wxAuiPaneInfo& cPane = mMainUI.GetPane(cPanelName);
	if (cPane.IsOk()) {
		cPane.Show();
		mMainUI.Update();
		return 0x0;
	}
	my1MiniViewer *pViewer = new my1MiniViewer;
	wxGrid* pGrid = 0x0;
	wxPanel* cPanel = CreateMemoryGridPanel(this,
		cAddress,MEM_MINIVIEW_WIDTH,MEM_MINIVIEW_HEIGHT,&pGrid);
	pGrid->EnableCellEditControl();
	pGrid->EnableEditing(true);
	pGrid->Connect(wxID_ANY,wxEVT_GRID_CELL_CHANGING,
		wxGridEventHandler(my1Form::OnMiniMVChanging),NULL,this);
	// update grid?
	word32_t cStart, cCheck;
	cStart = cAddress-pMemory->addr.offs;
	cCheck = pMemory->addr.size;
	for (int cRow=0;cRow<MEM_MINIVIEW_HEIGHT;cRow++) {
		for (int cCol=0;cCol<MEM_MINIVIEW_WIDTH;cCol++) {
			if (cStart<cCheck) {
				pGrid->SetCellValue(cRow,cCol,
					wxString::Format(wxS("%02X"),pMemory->data[cStart]));
			}
			else {
				pGrid->SetCellValue(cRow,cCol,wxS("--"));
				pGrid->SetReadOnly(cRow,cCol,true);
			}
			cStart++;
		}
	}
	wxPoint cPoint = this->GetScreenPosition();
	mMainUI.AddPane(cPanel, wxAuiPaneInfo().Name(cPanelName).
		Caption(cPanelName).DefaultPane().Float().DestroyOnClose().
		Dockable(false).RightDockable(true).
		FloatingPosition(cPoint.x+FLOAT_INIT_X,cPoint.y+FLOAT_INIT_Y));
	mMainUI.Update();
	pViewer->mStart = cAddress;
	pViewer->mSize = MEM_MINIVIEW_HEIGHT*MEM_VIEW_WIDTH;
	pViewer->pMemory = pMemory;
	pViewer->pGrid = pGrid;
	// get insert location based on start address
	my1MiniViewer *pTemp = mFirstViewer, *pPrev = 0x0;
	while(pTemp) {
		if (cAddress<pTemp->mStart) break;
		pPrev = pTemp;
		pTemp = pTemp->mNext;
	}
	// now, insert!
	if (!pPrev) mFirstViewer = pViewer;
	else pPrev->mNext = pViewer;
	pViewer->mNext = pTemp;
	return cPanel;
}
//------------------------------------------------------------------------------
my1DEVPanel* my1Form::CreateDevice7SegPanel(const wxString& aName) {
	// create unique panel name
	wxString cPanelName=wxS("dev7SEG");
	wxString cPanelCaption=wxS("7segment");
	if (aName!=wxEmptyString) cPanelName = aName;
	else if (!this->GetUniqueName(cPanelName)) return 0x0;
	// create 7-segment panel
	my1DEVPanel *cPanel = new my1DEVPanel(this);
	wxFont cFont(SIMS_FONT_SIZE,wxFONTFAMILY_SWISS,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	cPanel->SetFont(cFont);
	wxBoxSizer *pBoxSizer = new wxBoxSizer(wxHORIZONTAL);
	my1LED7Seg *cTemp;
	wxGBPosition cPosGB;
	wxString cLabel;
	wxGridBagSizer *pGridBagSizer = new wxGridBagSizer(); // vgap,hgap
	// this is 'msb' - for panel port linking (inserted below!)
	my1LEDCtrl* cTest = new my1LEDCtrl(cPanel, wxID_ANY,
		true, DOT_SIZE, DOT_SIZE); // dot
	cLabel = wxS("dot"); cTest->SetLabel(cLabel);
	cPosGB.SetRow(4); cPosGB.SetCol(3);
	pGridBagSizer->Add((wxWindow*)cTest,cPosGB);
	// bit-6 => 'g'
	cTemp = new my1LED7Seg(cPanel, wxID_ANY, false); // mid horiz
	cLabel = wxS("g"); cTemp->SetLabel(cLabel);
	cPosGB.SetRow(2); cPosGB.SetCol(1);
	pGridBagSizer->Add((wxWindow*)cTemp,cPosGB);
	// bit-5 => 'f'
	cTemp = new my1LED7Seg(cPanel, wxID_ANY, true); // top-left vert
	cLabel = wxS("f"); cTemp->SetLabel(cLabel);
	cPosGB.SetRow(1); cPosGB.SetCol(0);
	pGridBagSizer->Add((wxWindow*)cTemp,cPosGB);
	// bit-4 => 'e'
	cTemp = new my1LED7Seg(cPanel, wxID_ANY, true); // bot-left vert
	cLabel = wxS("e"); cTemp->SetLabel(cLabel);
	cPosGB.SetRow(3); cPosGB.SetCol(0);
	pGridBagSizer->Add((wxWindow*)cTemp,cPosGB);
	// bit-3 => 'd'
	cTemp = new my1LED7Seg(cPanel, wxID_ANY, false); // bot horiz
	cLabel = wxS("d"); cTemp->SetLabel(cLabel);
	cPosGB.SetRow(4); cPosGB.SetCol(1);
	pGridBagSizer->Add((wxWindow*)cTemp,cPosGB);
	// bit-2 => 'c'
	cTemp = new my1LED7Seg(cPanel, wxID_ANY, true); // bot-right vert
	cLabel = wxS("c"); cTemp->SetLabel(cLabel);
	cPosGB.SetRow(3); cPosGB.SetCol(2);
	pGridBagSizer->Add((wxWindow*)cTemp,cPosGB);
	// bit-1 => 'b'
	cTemp = new my1LED7Seg(cPanel, wxID_ANY, true); // top-right vert
	cLabel = wxS("b"); cTemp->SetLabel(cLabel);
	cPosGB.SetRow(1); cPosGB.SetCol(2);
	pGridBagSizer->Add((wxWindow*)cTemp,cPosGB);
	// bit-0 => 'a'
	cTemp = new my1LED7Seg(cPanel, wxID_ANY, false); // top horiz
	cLabel = wxS("a"); cTemp->SetLabel(cLabel);
	cPosGB.SetRow(0); cPosGB.SetCol(1);
	pGridBagSizer->Add((wxWindow*)cTemp,cPosGB);
	// add this!
	pBoxSizer->AddSpacer(SEG7_NUM_SPACER);
	pBoxSizer->Add(pGridBagSizer, 0, wxALIGN_CENTER);
	cPanel->SetSizerAndFit(pBoxSizer);
	// pass to aui manager
	mMainUI.AddPane(cPanel,wxAuiPaneInfo().Name(cPanelName).
		Caption(cPanelCaption).DefaultPane().Fixed().Position(DEV_INIT_POS).
		Bottom().Dockable(true).DestroyOnClose());
	mMainUI.Update();
	// save in main list
	mDevPanels.Append(cPanel);
	this->PrintInfoMessage(wxString::Format("Panel '%s' created!",
		cPanelName.Mid(0,cPanelName.Length()-2)));
	// port selector menu
	cPanel->Connect(cPanel->GetId(),wxEVT_RIGHT_DOWN,
		WX_MEH(my1Form::OnBITPanelClick),NULL,this);
	cPanel->Connect(cPanel->GetId(),wxEVT_LEFT_DCLICK,
		WX_MEH(my1Form::OnBITPanelClick),NULL,this);
	// return pointer to panel
	return cPanel;
}
//------------------------------------------------------------------------------
my1DEVPanel* my1Form::CreateDeviceKPadPanel(const wxString& aName) {
	// create unique panel name
	wxString cPanelName=wxS("devKPAD");
	wxString cPanelCaption=wxS("Keypad");
	if (aName!=wxEmptyString) cPanelName = aName;
	else if (!this->GetUniqueName(cPanelName)) return 0x0;
	// create keypad panel
	my1DEVPanel *cPanel = new my1DEVPanel(this);
	wxFont cFont(KPAD_FONT_SIZE,wxFONTFAMILY_SWISS,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	cPanel->SetFont(cFont);
	wxBoxSizer *pBoxSizer = new wxBoxSizer(wxHORIZONTAL);
	wxString cLabel;
	wxGBPosition cPosGB;
	// need to create the bitctrls first! wxCLASSINFO macro is not so smart!
	wxGridBagSizer *pGridBagSizer = new wxGridBagSizer(); // vgap,hgap
	// 3 dummy controls (for port assignment)
	my1ENCkPad *pData = new my1ENCkPad(cPanel, wxID_ANY,true);
	pData = new my1ENCkPad(cPanel, wxID_ANY,true);
	pData = new my1ENCkPad(cPanel, wxID_ANY,true);
	// data accessible signal
	pData = new my1ENCkPad(cPanel, wxID_ANY);
	cLabel = wxS("DA"); pData->SetLabel(cLabel);
	cPosGB.SetRow(0); cPosGB.SetCol(0);
	pGridBagSizer->Add(pData,cPosGB);
	// encoder output d3
	pData = new my1ENCkPad(cPanel, wxID_ANY);
	cLabel = wxS("D3"); pData->SetLabel(cLabel);
	cPosGB.SetRow(1); cPosGB.SetCol(0);
	pGridBagSizer->Add(pData,cPosGB);
	// encoder output d2
	pData = new my1ENCkPad(cPanel, wxID_ANY);
	cLabel = wxS("D2"); pData->SetLabel(cLabel);
	cPosGB.SetRow(2); cPosGB.SetCol(0);
	pGridBagSizer->Add(pData,cPosGB);
	// encoder output d1
	pData = new my1ENCkPad(cPanel, wxID_ANY);
	cLabel = wxS("D1"); pData->SetLabel(cLabel);
	cPosGB.SetRow(3); cPosGB.SetCol(0);
	pGridBagSizer->Add(pData,cPosGB);
	// encoder output d0
	pData = new my1ENCkPad(cPanel, wxID_ANY);
	cLabel = wxS("D0"); pData->SetLabel(cLabel);
	cPosGB.SetRow(4); cPosGB.SetCol(0);
	pGridBagSizer->Add(pData,cPosGB);
	// create new grid
	wxGridBagSizer *qGridBagSizer = new wxGridBagSizer(); // vgap,hgap
	int cSize = (KEY_SIZE_PANEL*5)/4;
	for (int cRow=0,cIndex=0;cRow<4;cRow++) {
		for (int cCol=0;cCol<4;cCol++) {
			if (cRow==3&&cCol==0) { cLabel = wxS("*"); cIndex = -1; }
			else if (cRow==3&&cCol==2) { cLabel = wxS("#"); cIndex = 15; }
			else if (cCol==3)
				cLabel = wxString::Format(wxS("%c"),(char)(cIndex/4)+'A');
			else cLabel = wxString::Format(wxS("%d"),++cIndex);
			my1KEYCtrl *pCtrl = new my1KEYCtrl(cPanel,wxID_ANY,cSize,cSize,
				(cRow*4+cCol),cLabel);
			cPosGB.SetRow(cRow); cPosGB.SetCol(cCol);
			qGridBagSizer->Add(pCtrl,cPosGB);
		}
	}
	// add to main sizer
	pBoxSizer->Add(qGridBagSizer,0,wxALIGN_CENTER);
	pBoxSizer->AddSpacer(5);
	pBoxSizer->Add(pGridBagSizer,0,wxALIGN_CENTER);
	pBoxSizer->AddSpacer(5);
	// assign sizer to main panel
	cPanel->SetSizerAndFit(pBoxSizer);
	// pass to aui manager
	mMainUI.AddPane(cPanel,wxAuiPaneInfo().Name(cPanelName).
		Caption(cPanelCaption).DefaultPane().Fixed().Position(DEV_INIT_POS).
		Bottom().Dockable(true).DestroyOnClose());
	mMainUI.Update();
	// save in main list
	mDevPanels.Append(cPanel);
	this->PrintInfoMessage(wxString::Format("Panel '%s' created!",
		cPanelName.Mid(0,cPanelName.Length()-2)));
	// panel doesn't look nice at first, refreshing view
	cPanel->SendSizeEvent();
	// port selector menu
	cPanel->Connect(cPanel->GetId(),wxEVT_RIGHT_DOWN,
		WX_MEH(my1Form::OnBITPanelClick),NULL,this);
	cPanel->Connect(cPanel->GetId(),wxEVT_LEFT_DCLICK,
		WX_MEH(my1Form::OnBITPanelClick),NULL,this);
	// return pointer to panel
	return cPanel;
}
//------------------------------------------------------------------------------
my1DEVPanel* my1Form::CreateDeviceLEDPanel(const wxString& aName,
		bool aVertical) {
	// create unique panel name
	wxString cPanelName=wxS("devLED");
	wxString cPanelCaption=wxS("LED");
	if (aName!=wxEmptyString) cPanelName = aName;
	else if (!this->GetUniqueName(cPanelName)) return 0x0;
	// create the panel
	my1DEVPanel *cPanel = new my1DEVPanel(this);
	wxFont cFont(SIMS_FONT_SIZE,wxFONTFAMILY_SWISS,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	cPanel->SetFont(cFont);
	cPanel->Flag(aVertical);
	int cOrient = aVertical ? wxVERTICAL : wxHORIZONTAL;
	wxBoxSizer *pBoxSizer = new wxBoxSizer(cOrient);
	pBoxSizer->AddSpacer(DEVC_POP_SPACER);
	for (int cLoop=0;cLoop<DATASIZE;cLoop++) {
		my1LEDCtrl* pCtrl = new my1LEDCtrl(cPanel,wxID_ANY);
		pBoxSizer->Add((wxWindow*)pCtrl,0,wxALIGN_TOP);
	}
	pBoxSizer->AddSpacer(DEVC_POP_SPACER);
	cPanel->SetSizerAndFit(pBoxSizer);
	// pass to aui manager
	mMainUI.AddPane(cPanel,wxAuiPaneInfo().Name(cPanelName).
		Caption(cPanelCaption).DefaultPane().Fixed().Position(DEV_INIT_POS).
		Top().Dockable(true).DestroyOnClose());
	if (aVertical) {
		wxAuiPaneInfo &cPane = mMainUI.GetPane(cPanelName);
		if (cPane.IsOk()) cPane.Left();
	}
	mMainUI.Update();
	// save in main list
	mDevPanels.Append(cPanel);
	this->PrintInfoMessage(wxString::Format("Panel '%s' created!",
		cPanelName.Mid(0,cPanelName.Length()-2)));
	// port selector menu
	cPanel->Connect(cPanel->GetId(),wxEVT_RIGHT_DOWN,
		WX_MEH(my1Form::OnBITPanelClick),NULL,this);
	cPanel->Connect(cPanel->GetId(),wxEVT_LEFT_DCLICK,
		WX_MEH(my1Form::OnBITPanelClick),NULL,this);
	// return pointer to panel
	return cPanel;
}
//------------------------------------------------------------------------------
my1DEVPanel* my1Form::CreateDeviceSWIPanel(const wxString& aName) {
	// create unique panel name
	wxString cPanelName=wxS("devSWI");
	wxString cPanelCaption=wxS("Switch");
	if (aName!=wxEmptyString) cPanelName = aName;
	else if (!this->GetUniqueName(cPanelName)) return 0x0;
	// create the panel
	my1DEVPanel *cPanel = new my1DEVPanel(this);
	wxFont cFont(SIMS_FONT_SIZE,wxFONTFAMILY_SWISS,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	cPanel->SetFont(cFont);
	wxBoxSizer *pBoxSizer = new wxBoxSizer(wxHORIZONTAL);
	pBoxSizer->AddSpacer(DEVC_POP_SPACER);
	for (int cLoop=0;cLoop<DATASIZE;cLoop++) {
		my1SWICtrl* pCtrl = new my1SWICtrl(cPanel,wxID_ANY);
		pBoxSizer->Add((wxWindow*)pCtrl,0,wxALIGN_TOP);
	}
	pBoxSizer->AddSpacer(DEVC_POP_SPACER);
	cPanel->SetSizerAndFit(pBoxSizer);
	// pass to aui manager
	mMainUI.AddPane(cPanel,wxAuiPaneInfo().Name(cPanelName).
		Caption(cPanelCaption).DefaultPane().Fixed().Position(DEV_INIT_POS).
		Top().Dockable(true).DestroyOnClose());
	mMainUI.Update();
	// save in main list
	mDevPanels.Append(cPanel);
	this->PrintInfoMessage(wxString::Format("Panel '%s' created!",
		cPanelName.Mid(0,cPanelName.Length()-2)));
	// port selector menu
	cPanel->Connect(cPanel->GetId(),wxEVT_RIGHT_DOWN,
		WX_MEH(my1Form::OnBITPanelClick),NULL,this);
	cPanel->Connect(cPanel->GetId(),wxEVT_LEFT_DCLICK,
		WX_MEH(my1Form::OnBITPanelClick),NULL,this);
	// return pointer to panel
	return cPanel;
}
//------------------------------------------------------------------------------
my1DEVPanel* my1Form::CreateDeviceBUTPanel(const wxString& aName) {
	// create unique panel name
	wxString cPanelName=wxS("devBUT");
	wxString cPanelCaption=wxS("Button");
	if (aName!=wxEmptyString) cPanelName = aName;
	else if (!this->GetUniqueName(cPanelName)) return 0x0;
	// create the panel
	my1DEVPanel *cPanel = new my1DEVPanel(this);
	wxFont cFont(SIMS_FONT_SIZE,wxFONTFAMILY_SWISS,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
	cPanel->SetFont(cFont);
	wxBoxSizer *pBoxSizer = new wxBoxSizer(wxHORIZONTAL);
	pBoxSizer->AddSpacer(DEVC_POP_SPACER);
	for (int cLoop=0;cLoop<DATASIZE;cLoop++) {
		my1BUTCtrl* pCtrl = new my1BUTCtrl(cPanel,wxID_ANY);
		pBoxSizer->Add((wxWindow*)pCtrl,0,wxALIGN_TOP);
	}
	pBoxSizer->AddSpacer(DEVC_POP_SPACER);
	cPanel->SetSizerAndFit(pBoxSizer);
	// pass to aui manager
	mMainUI.AddPane(cPanel,wxAuiPaneInfo().Name(cPanelName).
		Caption(cPanelCaption).DefaultPane().Fixed().Position(DEV_INIT_POS).
		Top().Dockable(true).DestroyOnClose());
	mMainUI.Update();
	// save in main list
	mDevPanels.Append(cPanel);
	this->PrintInfoMessage(wxString::Format("Panel '%s' created!",
		cPanelName.Mid(0,cPanelName.Length()-2)));
	// port selector menu
	cPanel->Connect(cPanel->GetId(),wxEVT_RIGHT_DOWN,
		WX_MEH(my1Form::OnBITPanelClick),NULL,this);
	cPanel->Connect(cPanel->GetId(),wxEVT_LEFT_DCLICK,
		WX_MEH(my1Form::OnBITPanelClick),NULL,this);
	// return pointer to panel
	return cPanel;
}
//------------------------------------------------------------------------------
