//------------------------------------------------------------------------------
#include "my1sim85.hpp"
//------------------------------------------------------------------------------
/* only needed by Assemble */
#include <iostream>
//------------------------------------------------------------------------------
#ifndef MY1APP_PROGNAME
#define PROGNAME "my1sim85"
#else
#define PROGNAME MY1APP_PROGNAME
#endif
#ifndef MY1APP_AUTHMAIL
#define PROGAUTH "azman@my1matrix.org"
#else
#define PROGAUTH MY1APP_AUTHMAIL
#endif
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
my1Sim85::my1Sim85() {
	sys85_init(&mSyst);
	mSyst.core.regs.umsk = 0; // show all regs
	// stop when no valid code
	sys85_do_exit_xxcode(&mSyst);
	sys85_do_exit_nocode(&mSyst);
	// build default system
	sys85_build_default(&mSyst);
	sys85_boot(&mSyst,1); // cold boot
	// states
	mNext = 0x0; mCurr = 0x0;
	mStat = 0; mFlag = 0;
	mLast = 0x0;
	mCodeLink = 0x0;
}
//------------------------------------------------------------------------------
my1Sim85::~my1Sim85() {
	this->DisconnectALL();
	sys85_free(&mSyst);
}
//------------------------------------------------------------------------------
void my1Sim85::SystemObject(void* anObject) {
	mSyst.pobj = (void*)anObject;
}
//------------------------------------------------------------------------------
void my1Sim85::UpdateHandler(regstask_t aTask) {
	mSyst.core.regs.doupdate = (regstask_t)aTask;
}
//------------------------------------------------------------------------------
bool my1Sim85::Ready(void) {
	return code8085_valid(CODE85(&mSyst))?true:false;
}
//------------------------------------------------------------------------------
bool my1Sim85::Built(void) {
	return sys85_mems(&mSyst); /* at least have memory */
}
//------------------------------------------------------------------------------
bool my1Sim85::Halted(void) {
	return sys85_halted(&mSyst)?true:false;
}
//------------------------------------------------------------------------------
bool my1Sim85::Interrupted(void) {
	return core8085_is_intr(CORE85(&mSyst))?true:false;
}
//------------------------------------------------------------------------------
bool my1Sim85::ZeroCode(void) {
	return sys85_codesize(&mSyst)?false:true;
}
//------------------------------------------------------------------------------
int my1Sim85::GetStartAddress(void) {
	return mSyst.init;
}
//------------------------------------------------------------------------------
void my1Sim85::SetStartAddress(int anAddr) {
	if (anAddr<0||anAddr>=I8085_MAX_MSIZE)
		anAddr = 0;
	mSyst.init = anAddr;
}
//------------------------------------------------------------------------------
void* my1Sim85::GetCodeLink(void) {
	return mCodeLink;
}
//------------------------------------------------------------------------------
void my1Sim85::SetCodeLink(void* aCodeLink) {
	mCodeLink = aCodeLink;
}
//------------------------------------------------------------------------------
word32_t my1Sim85::Stat(word32_t mask) {
	return mStat&mask;
}
//------------------------------------------------------------------------------
word32_t my1Sim85::Flag(word32_t mask) {
	return mFlag&mask;
}
//------------------------------------------------------------------------------
word32_t my1Sim85::C2mem(void) {
	return mCmem;
}
//------------------------------------------------------------------------------
word32_t my1Sim85::C2hex(void) {
	return mChex;
}
//------------------------------------------------------------------------------
word32_t my1Sim85::CodeSize(void) {
	return mSyst.code.bcnt;
}
//------------------------------------------------------------------------------
word32_t my1Sim85::Tstates(int full) {
	return full?mSyst.core.step:mSyst.core.curr;
}
//------------------------------------------------------------------------------
my1inst_t* my1Sim85::Next(void) {
	return mNext;
}
//------------------------------------------------------------------------------
my1inst_t* my1Sim85::Curr(void) {
	return mCurr;
}
//------------------------------------------------------------------------------
my1inst_t* my1Sim85::Reset(word32_t addr, int cold) {
	if ((addr&0xffff)==addr)
		mSyst.init = addr;
	sys85_boot(&mSyst,cold);
	sys85_bootaddr(&mSyst,mSyst.init);
	mNext = sys85_find_code(&mSyst,mSyst.init);
	mCurr = 0x0;
	mStat &= ~SIM85_STAT_INIT_OK;
	mStat &= ~SIM85_STAT_INVALID;
	return mNext;
}
//------------------------------------------------------------------------------
word32_t my1Sim85::Code2MEM(void) {
	mCmem = sys85_load_code(&mSyst,CODE85(&mSyst));
	return mCmem;
}
//------------------------------------------------------------------------------
word32_t my1Sim85::Code2HEX(char* aFilename) {
	mChex = sys85_make_hex(&mSyst,aFilename);
	return mChex;
}
//------------------------------------------------------------------------------
bool my1Sim85::Step(void) {
	my1inst_t* temp;
	temp = mCurr;
	sys85_step(&mSyst);
	mCurr = mSyst.curr;
	mNext = sys85_find_code(&mSyst,mSyst.core.regs.pcnt);
	if (sys85_halted(&mSyst)&&mCurr==mNext)
		mCurr = temp;
	if (sys85_exited_nocode(&mSyst)) {
		mStat |= SIM85_STAT_NO_CODE;
		return false;
	}
	if (sys85_exited_xxcode(&mSyst)) {
		mStat |= SIM85_STAT_XX_CODE;
		return false;
	}
	return true;
}
//------------------------------------------------------------------------------
bool my1Sim85::Exec(int aStep) {
	bool cFlag = true;
	bool cFlow = true;
	while (cFlag&&cFlow) {
		cFlag = this->Step();
		if (aStep<0) cFlow = !sys85_exited(&mSyst);
		else if (--aStep==0) cFlow = false;
	}
	return cFlag;
}
//------------------------------------------------------------------------------
bool my1Sim85::DisconnectALL(void) {
	int loop;
	core8085_redo(CORE85(&mSyst));
	for (loop=0;loop<4;loop++) {
		dpin_unlink(&mSyst.core.pins[loop]);
	}
	return true;
}
//------------------------------------------------------------------------------
bool my1Sim85::ConnectROM(int aStart) {
	my1memory_t* pmem = core8085_insert_rom2764(CORE85(&mSyst),aStart);
	return pmem ? true : false;
}
//------------------------------------------------------------------------------
bool my1Sim85::ConnectRAM(int aStart) {
	my1memory_t* pmem = core8085_insert_ram6264(CORE85(&mSyst),aStart);
	return pmem ? true : false;
}
//------------------------------------------------------------------------------
bool my1Sim85::ConnectPPI(int aStart) {
	my1device_t* pdev = core8085_insert_ppi8255(CORE85(&mSyst),aStart);
	return pdev ? true : false;
}
//------------------------------------------------------------------------------
bool my1Sim85::Assemble(const char* srcName, const char* lstName) {
	my1code8085_t code;
	int stat;
	mStat &= ~SIM85_STAT_CODE_OK;
	// prepare assembler/loader
	code8085_redo(CODE85(&mSyst));
	code8085_init(&code);
	if (lstName) code.list = (char*)lstName;
	// print tool info
	std::cout << "\n" << PROGNAME << " - 8085 Assembler\n";
	std::cout << "  => by " << PROGAUTH << "\n\n";
	// do it!
	stat = code8085_load_asm(&code,(char*)srcName);
	if (stat) {
		// show error if there is any...
		code.errs.curr = 0x0;
		while (list_scan_item(&code.errs)) {
			my1cstr_t* pstr = (my1cstr_t*)code.errs.curr->data;
			std::cout << "** " <<  pstr->buff << std::endl;
		}
		std::cout << std::endl;
	}
	std::cout << PROGNAME << " - Done! [" << code.lcnt << " line(s): ";
	if (!code.ecnt) std::cout << "OK";
	else std::cout << "Found " << code.ecnt << " error(s)";
	std::cout << "]\n\n";
	if (code.list) {
		if (code.flag&SW85_FLAG_LIST)
			std::cout << "## LST file written! (" << code.list << ")";
		else
			std::cout << "** Failed to create " << code.list << " ?!";
		std::cout << "]\n\n";
	}
	if (!stat) {
		// 'keep' code if valid
		if (code8085_load_from(CODE85(&mSyst),&code))
			std::cout << "** Failed to keep code " << srcName << " ?!\n\n";
		if (code8085_valid(CODE85(&mSyst))) // Ready()
			mStat |= SIM85_STAT_CODE_OK;
	}
	// release stuff
	code8085_free(&code);
#ifdef MY1DEBUG
	this->PrintCodexInfo();
#endif
	return this->Ready();
}
//------------------------------------------------------------------------------
bool my1Sim85::Generate(const char* tgtName) {
	return this->Code2HEX((char*)tgtName) ? false : true;
}
//------------------------------------------------------------------------------
bool my1Sim85::Simulate(int aStep) {
	mFlag = 0;
	if (!this->Built()||!this->Ready()) return false;
	if (!(mStat&SIM85_STAT_INIT_OK)||aStep<1) {
		if (this->Code2MEM()) {
			mFlag |= SIM85_FLAG_ECODE2MEM;
			return false;
		}
		if (!this->Reset()) {
			mFlag |= SIM85_FLAG_ECODEFIND;
			return false;
		}
		mStat |= SIM85_STAT_INIT_OK;
	}
	else {
		if(!this->Exec(aStep)) {
			mFlag |= SIM85_FLAG_ESTEPEXEC;
			return false;
		}
	}
#ifdef MY1DEBUG
	this->PrintCodexInfo();
#endif
	return true;
}
//------------------------------------------------------------------------------
my1list_t* my1Sim85::MemoryList(void) {
	return &mSyst.core.mmap.devs;
}
//------------------------------------------------------------------------------
my1list_t* my1Sim85::DeviceList(void) {
	return &mSyst.core.pmap.devs;
}
//------------------------------------------------------------------------------
my1dpin_t* my1Sim85::Pin(int aPick) {
	my1dpin_t* pBit;
	if (aPick<0||aPick>=I8085_PIN_COUNT)
		pBit = 0x0;
	else pBit = &mSyst.core.pins[aPick];
	return pBit;
}
//------------------------------------------------------------------------------
my1memory_t* my1Sim85::Memory(word32_t anAddr) {
	mLast =  sys85_memory(&mSyst,anAddr);
	return mLast;
}
//------------------------------------------------------------------------------
my1memory_t* my1Sim85::LastMemory(void) {
	return mLast;
}
//------------------------------------------------------------------------------
my1device_t* my1Sim85::Device(word32_t anAddr, int* pIndex) {
	my1device_t* pDev;
	pDev = sys85_device(&mSyst,anAddr);
	if (pIndex)
		*pIndex = (pDev) ? mSyst.core.pmap.devs.step : -1;
	return pDev;
}
//------------------------------------------------------------------------------
my1device_t* my1Sim85::DevicePick(int anIndex) {
	my1list_t* list;
	list = &mSyst.core.pmap.devs;
	list_scan_prep(list);
	while (list_scan_item(list)) {
		if (list->step == anIndex)
			return (my1device_t*)list->curr->data;
	}
	return 0x0;
}
//------------------------------------------------------------------------------
bool my1Sim85::PrepLink(iolink_t* link, word32_t anAddr) {
	return sys85_link_prep(&mSyst,link,anAddr)?false:true;
}
//------------------------------------------------------------------------------
bool my1Sim85::MemoryRead(word32_t anAddr, byte08_t& rData) {
	if (sys85_mmap_getb(&mSyst,anAddr,&rData))
		mLast = mSyst.core.mmap.last;
	else mLast = 0x0;
	return mLast?true:false;
}
//------------------------------------------------------------------------------
bool my1Sim85::MemoryWrite(word32_t anAddr, byte08_t rData) {
	if (sys85_mmap_putb(&mSyst,anAddr,rData))
		mLast = mSyst.core.mmap.last;
	else mLast = 0x0;
	return mLast?true:false;
}
//------------------------------------------------------------------------------
my1reg85_t* my1Sim85::Regs(void) {
	return &mSyst.core.regs;
}
//------------------------------------------------------------------------------
int my1Sim85::GetCodexLine(void) {
	return mCurr ? mCurr->line : 0;
}
//------------------------------------------------------------------------------
int my1Sim85::GetNextLine(void) {
	return mNext ? mNext->line : 0;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
