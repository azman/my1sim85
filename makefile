# makefile for my1sim85 - 8085 system simulation and development tool

PROJECT = my1sim85
GUISPRO = $(PROJECT)
GUISOBJ = my1sys85.o
# my1code8085.o my1core8085.o
# my1sim85_base.o my1sim85_devs.o my1sim85_up85.o
GUISOBJ += my1sim85.o my1uart.o wxterm.o
GUISOBJ += wxbit.o wxled.o wxswitch.o wxcode.o wxpanel.o wxpaneldev.o
GUISOBJ += wxpref.o wxform.o wxmain.o
# components of wxform
FORMSRC = src/wxform_panels.cpp src/wxform_toolbars.cpp
FORMSRC += src/wxform_handlers.cpp src/wxform_devpanels.cpp
FORMSRC += src/wxform_prints.cpp

EXTPATH = ../my1asm85/src
EX1PATH = ../my1emu85/src
EX2PATH = ../my1codelib/src
EX3PATH = ../my1termw/src
PACKDIR = $(PROJECT)-$(shell cat VERSION)
PACKDAT = README TODO CHANGELOG VERSION asm sys
PLATBIN ?= $(shell uname -m)
VERSION = -DMY1APP_PROGVERS=\"$(shell date +%Y%m%d)\"

DELETE = rm -rf
COPY = cp -R
ARCHIVE = tar cjf
ARCHEXT = .tar.bz2
CONVERT = convert

CFLAGS += -Wall -I$(EXTPATH) -I$(EX1PATH) -I$(EX2PATH) -I$(EX3PATH)
LFLAGS +=
OFLAGS +=
LOCAL_FLAGS =
WX_LIBS = stc,aui,html,adv,core,xml,base
WX_OPTS = --libs $(WX_LIBS)

ifeq ($(DO_MINGW),YES)
	GUISPRO = $(PROJECT).exe
	GUISOBJ += wxmain.res
	PLATBIN = mingw
	ARCHIVE = zip -r
	ARCHEXT = .zip
	# cross-compiler settings
	XTOOL_CHK ?= /home/share/tool/xtool-mingw
	XTOOL_DIR = $(XTOOL_CHK)
	ifeq ("$(wildcard $(XTOOL_DIR)/bin/*-gcc)","")
		XTOOL_PREFIX ?= x86_64-w64-mingw32-
		BUILD_TOOL = $(shell which $(XTOOL_PREFIX)gcc 2>/dev/null)
		ifneq ("$(BUILD_TOOL)","")
			TOOL_PATH_BIN = $(shell dirname $(BUILD_TOOL))
			XTOOL_DIR = $(shell dirname $(TOOL_PATH_BIN))
		else
			XTOOL_DIR = /usr
		endif
	endif
	CROSS_COMPILE = $(XTOOL_DIR)/bin/$(XTOOL_PREFIX)
	# below is to remove console at runtime
	LFLAGS += -Wl,-subsystem,windows
	# extra switches
	CFLAGS += --static
	CFLAGS += -I$(XTOOL_DIR)/include -DDO_MINGW -DWIN32_LEAN_AND_MEAN
	LFLAGS += -L$(XTOOL_DIR)/lib
	# wx-config may be at different location!
	WXCFG_BIN = $(XTOOL_CHK)/bin/wx-config
	ifeq ("$(wildcard $(WXCFG_BIN))","")
		FIND_WXCFG = $(shell which wx-config 2>/dev/null)
		ifneq ("$(FIND_WXCFG)","")
			WXCFG_BIN = $(FIND_WXCFG)
			XTOOL_CHK = $(shell dirname $(WXCFG_BIN))
		endif
	endif
	WX_OPTW = $(WX_OPTS) --debug=no
	# -mthreads is not playing nice with others - has to go!
	WX_LIBFLAGS = $(shell $(WXCFG_BIN) $(WX_OPTW) | sed 's/-mthreads//g')
	WX_CXXFLAGS = $(shell $(WXCFG_BIN) --cxxflags | sed 's/-mthreads//g')
	# include for resource compilation!
	WX_VERS = $(shell $(WXCFG_BIN) --version)
	WX_MAJORVERS = wx-$(shell echo $(WX_VERS) | sed 's/^\([0-9]\.[0-9]\).*/\1/')
	WINDRES_FLAG = --include-dir $(XTOOL_CHK)/include
	WINDRES_FLAG += --include-dir $(XTOOL_CHK)/include/$(WX_MAJORVERS)
else
	BUILD_TOOL=gcc
	WXCFG_BIN=wx-config
	WX_LIBFLAGS = $(shell $(WXCFG_BIN) $(WX_OPTS))
	WX_CXXFLAGS = $(shell $(WXCFG_BIN) --cxxflags)
	WX_VERS = $(shell $(WXCFG_BIN) --version)

my1sys85.o: CFLAGS += -DMY1RAND_USE_RANDOM

endif
PACKDAT += $(GUISPRO)

CC = $(CROSS_COMPILE)gcc
CPP = $(CROSS_COMPILE)g++
RES = $(CROSS_COMPILE)windres

.PHONY: all gui new debug find pack version release clean sweep

debug: LOCAL_FLAGS += -DMY1DEBUG
pack: ARCNAME = $(PACKDIR)-$(PLATBIN)-$(shell date +%Y%m%d%H%M)$(ARCHEXT)
version: VERSION = -DMY1APP_PROGVERS=\"$(shell cat VERSION)\"
release: WX_LIBFLAGS = $(shell $(WXCFG_BIN) $(WX_OPTS) --debug=no)
release: WX_CXXFLAGS = $(shell $(WXCFG_BIN) --cxxflags --debug=no)

all: gui

gui: $(GUISPRO)

new: clean all

debug: new

find:
	@echo "Tool:{$(BUILD_TOOL)}"
	@echo "  CC:{$(CC)}"
	@echo "Path:{$(XTOOL_DIR)}"
	@echo "Wx-v:{$(WX_VERS)}"
	@echo "Conf:{$(WXCFG_BIN)}"

pack: release
	mkdir -pv $(PACKDIR)
	$(COPY) $(PACKDAT) $(PACKDIR)/
	$(DELETE) $(ARCNAME)
	$(ARCHIVE) $(ARCNAME) $(PACKDIR)

version: new

release: version
	strip --strip-unneeded $(GUISPRO)

$(GUISPRO): $(GUISOBJ)
	$(CPP) -o $@ $+ $(LFLAGS) $(OFLAGS) $(WX_LIBFLAGS)

wxform.o: src/wxform.cpp src/wxform.hpp $(FORMSRC)
	$(CPP) $(CFLAGS) $(VERSION) $(WX_CXXFLAGS) $(LOCAL_FLAGS) -c $<

wx%.o: src/wx%.cpp src/wx%.hpp
	$(CPP) $(CFLAGS) $(VERSION) $(WX_CXXFLAGS) $(LOCAL_FLAGS) -c $<

wx%.o: src/wx%.cpp
	$(CPP) $(CFLAGS) $(VERSION) $(WX_CXXFLAGS) $(LOCAL_FLAGS) -c $<

%.o: src/%.c src/%.h
	$(CC) $(CFLAGS) $(LOCAL_FLAGS) -c $<

%.o: src/%.c
	$(CC) $(CFLAGS) $(LOCAL_FLAGS) -c $<

%.o: src/%.cpp src/%.hpp
	$(CPP) $(CFLAGS) $(LOCAL_FLAGS) -c $<

%.o: src/%.cpp
	$(CPP) $(CFLAGS) $(LOCAL_FLAGS) -c $<

%.ico: res/%.xpm
	$(CONVERT) $< $@

%.res: src/%.rc apps.ico
	$(RES) --include-dir res $(WINDRES_FLAG) -O COFF $< -o $@

%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	$(CC) $(CFLAGS) -c $<

%.o: $(EX1PATH)/%.c $(EX1PATH)/%.h
	$(CC) $(CFLAGS) -c $<

%.o: $(EX2PATH)/%.c $(EX2PATH)/%.h
	$(CC) $(CFLAGS) -c $<

wx%.o: $(EX3PATH)/wx%.cpp $(EX3PATH)/wx%.hpp
	$(CC) $(CFLAGS) $(WX_CXXFLAGS) -c $<

clean:
	-$(DELETE) $(PROJECT) $(PROJECT)-* *.exe *.bz2 *.zip *.o *.ico *.res

sweep: clean
	-$(DELETE) asm/*.lst asm/*.hex
