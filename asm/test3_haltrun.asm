	org 0000h
	jmp start
	org 0024h
	jmp do_trap
	org 002ch
	jmp do_back
	org 0034h
	jmp do_back
	org 003ch
	jmp do_back
	org 0040h
start:
	lxi sp,4000h
	xra a
	hlt
	mvi a, 83h
	out 83h
reset:
	mvi b, 8 ; loop count
	mvi c, 01h ; init pattern
loop:
	mov a, c
	out 80h
	call delay
	rlc
	mov c, a
	dcr b
	jnz loop
	hlt
	jmp reset
; basic delay routine
delay:
	push psw
	push b
	lxi b, 128
delay_loop:
	dcx b
	mov a, b
	ora c
	jnz delay_loop
	pop b
	pop psw
	ret
; interrupt service routines (trap)
do_trap:
	push psw
	mvi a,0fh ; bsr mode!
	out 83h
	call delay
	mvi a,0eh
	out 83h
	call delay
	pop psw
	ei
	ret
do_back:
	ei
	ret
